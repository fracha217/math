\chapter{Series}

References \autocite{nagy-vector-valued-calculus}.

\section{Absolute Convergence}

\begin{proposition}
\label{prop:norm-module-convergent-series-characterization}
Let $M$ be a normed topological module.
Let $(x_i)$ be a sequence of elements of $M$.
The following are equivalent:
\begin{enumerate}
\item $\sum_{i=0}^\infty x_i$ converges
\item for every $\epsilon \gt 0$, there exists an $N \in \N$ such that for every pair of natural numbers $m\geq n \geq N$, $\norm{\sum_{i=n}^m x_i} \lt \epsilon$.
\end{enumerate}
\end{proposition}
\begin{proof}
Omitted.
\end{proof}

\begin{proposition}
\label{prop:absolute-convergence-characterization}
Let $M$ be a normed topological module.
Let $\sum x_i$ be series of elements of $M$.
The following are equivalent:
\begin{enumerate}
\item the series of real numbers $\sum_{i=0}^\infty \norm{x_i}$ converges (to an element of $\R$)
\item there is an $M \in \R$ such that $\sum_{i=0}^n \norm{x_i} \lt M$ for all $n \in \N$.
\end{enumerate}
\end{proposition}
\begin{proof}
Omitted.
\end{proof}

\begin{exposition}
A series $\sum x_i$ in a normed topological module $M$ is said to be a \addindex{absolutely convergent series}, or converges absolutely, if any of the conditions in \cref{prop:absolute-convergence-characterization} hold.

If in addition $\sum x_i$ converges to $x \in M$, we say $\sum x_i$ \addindex{converges absolutely} to $x$.
\end{exposition}

\begin{exposition}
A family $(x_i)$ of elements in a normed topological module $M$ with norm $\norm{\blank}\colon M \to R_+$.
We say $(x_i)$ is \addindex{absolutely summable} if $(\norm{x_i})$ is a summable family of elements of $\R$.
\end{exposition}

\begin{proposition}
\label{prop:absolutely-summable-summable}
Let $M$ be a complete normed module.
Let $(x_i)_{i \in I}$ be an absolutely summable family of elements of $M$.
Then $(x_i)$ is summable.
\end{proposition}
\begin{proof}
Let $\epsilon \gt 0$.
By \cref{prop:series-cauchy-criteria} there is finite subset $I_\epsilon \subset I$ such that $\sum_{j \in J} \norm{x_j} \lt \epsilon$ for every finite subset $J \subset I \setminus I_\epsilon$.
Then for every finite subset $J \subset  I \setminus I_\epsilon$, we have $\norm{\sum_{j \in J} x_j} \leq \sum_{j \in J} \norm{x_j} \lt \epsilon$.

Since $M$ is complete, the result then follows from \cref{prop:series-cauchy-criteria}.
\end{proof}

\begin{proposition}
\label{prop:absolutely-convergent-series-convergent}
Let $M$ be a complete normed module.
Let $\sum_{i=0}^\infty x_i$ be a series in $M$.
Consider the following:
\begin{enumerate}
\item $\sum_{i=0}^\infty x_i$ is absolutely convergent
\item $(x_i)$ is summable
\item for every bijection $\sigma\colon \N \to \N$, the series $\sum_{i=0}^\infty x_{\sigma(i)}$ converges.
\end{enumerate}
Then \proofitem{1} implies \proofitem{2}, and \proofitem{2} implies \proofitem{3}.
Furthermore, if \proofitem{1} holds, then $\norm{\sum x_i} \leq \sum \norm{x_i}$, and if \proofitem{2} holds, then $\sum_{i=0}^\infty x_{\sigma(i)} = \sum_{i \in \N} x_i$ for all bijections $\sigma\colon \N \to \N$.
\end{proposition}
\begin{proof}
Assume \proofitem{1} holds.

We have $\norm{\sum^m_{i=n} x_i} \leq \sum^m_{i=n} \norm{x_i}$ for every $m$ and $n$.
Hence $\sum x_i$ converges by \cref{prop:norm-module-convergent-series-characterization}.
For every $n \in \N$, we have $\norm{\sum_{i=0}^n x_i} \leq \sum_{i=0}^n \norm{x_i}$, so follows $\norm{\sum x_i} \leq \sum \norm{x_i}$.

Because $M$ is complete, to show \proofitem{2} holds, it suffices to show that the net $(\sum_{i \in S} x_i)_S$, where $S$ ranges over all finite subsets of $\N$, is a Cauchy net.

Let $\epsilon \gt 0$.
Since $\sum_{i=1}^\infty \norm{x_i}$ converges, we can find an $N \in \N$ such that for every pair of natural numbers $m \geq n \geq N$, $\sum_{i=n}^m \norm{x_i} \lt \epsilon/2$ (\cref{prop:norm-module-convergent-series-characterization})
Then for all finite sets $S$ and $T$, we have
\[
\norm{\sum_{i \in S \setminus \{1,\dotsc,N-1\}} x_i - \sum_{j \in T \setminus \{1,\dotsc,N-1\}} x_j} \leq \sum_{i \in S\setminus \{1,\dotsc,N-1\}} \norm{x_i} + \sum_{j \in T\setminus \{1,\dotsc,N-1\}} \norm{x_j} \lt \epsilon\nsp{.}
\]
It follows that for all finite sets $S$ and $T$ containing $\{1,\dotsc,N\}$, we have $\norm{\sum_{i \in S} x_i - \sum_{j \in T} x_j} \lt \epsilon$
Thus $(\sum_{i \in S} x_i)_S$ is a Cauchy net.

Now assume \proofitem{2} holds.

Fix a bijection $\sigma\colon \N \to \N$.
For every $N \in \N$, the partial sums $\sum_{i=0}^n x_{\sigma(i)}$ for $n \geq N$ are contained in the set
\[
\left\{\sum_{i \in S}x_i:\text{$S$ is finite and $\sigma(i) \in S$ for all $i \leq N$}\right\}
\]
so the the partial sums $(\sum_{i=0}^n x_{\sigma(i)})$ are a subnet of the net defining $\sum_{i \in \N} x_i$.
Hence $\sum_{i=0}^n x_{\sigma(i)} = \sum_{i \in \N} x_i$ (\cref{prop:net-cluster-convergence-properties}).
Thus \proofitem{3} holds.
\end{proof}

\begin{proposition}
Let $M$ be a complete normed module over a complete normed algebra $A$.
The following hold:
\begin{enumerate}
\item if $\sum_{i=0}^\infty x_i$ is absolutely convergent, then $\sum ax_i$ is absolutely convergent
\item if  $\sum_{i=0}^\infty x_i$ and $\sum_{i=0}^\infty y_i$ are absolutely convergent, then $\sum x_i + y_i$ is absolutely convergent and $\sum x_i + y_i = \sum x_i + \sum y_i$.
\end{enumerate}
\end{proposition}
\begin{proof}
Omitted.
\end{proof}

\begin{proposition}
\label{prop:absolutely-convergent-series-product-convergent}
Let $A$ be an associative normed prealgebra.
Let $(a_i)$ and $(b_i)$ be sequences in $A$.
Suppose $\sum a_i$ converges absolutely to $a$ and $\sum b_i$ converges to $b$.
Set $c_i \eqdef \sum^i_{k=0} a_kb_{i-k}$ for each $i$.
The following hold:
\begin{enumerate}
\item $\sum c_i$ converges to $ab$
\item if $\sum b_i$ converges absolutely, so does $\sum c_i$.
\end{enumerate}
\end{proposition}
\begin{proof}
\proofitem{1}.
Let $a'_n$, $b'_n$ and $c'_n$ be the $n$-th partial sums for $\sum a_i$, $\sum b_i$, and $\sum c_i$, respectively.
Set $\beta_n \eqdef b'_n - b$.
Then
\[
\begin{aligned}
c'_n & = a_0b_0 + (a_0b_1 + a_1b_0) + \dotsb + (a_0b_n + \dotsb + a_nb_0) \\
     & = a_0b'_n + a_1b'_{n-1} + \dotsb + a_nb'_0                         \\
     & = a_0(b + \beta_n) + a_1(b + \beta_{n-1})+ \dotsb + a_n(b+\beta_0) \\
     & = a'_nb + a_0\beta_n + \dotsb + a_n\beta_0\nsp{.}
\end{aligned}
\]
Set $d_n \eqdef a_0\beta_n + \dotsb + a_n\beta_0$.
Since $a'_nb \to ab$, it suffices to show that $d_n \to 0$.

Put $\alpha \eqdef \sum\norm{a_i}$.
Let $\epsilon \gt 0$.
We can choose $N \in \N$ such that $\norm{\beta_i} \lt \epsilon$ for $i \geq N$ (since $\beta_n \to 0$.
Then for each $i \geq N$ we have
\[
\begin{aligned}
\norm{d_i} & \leq \norm{\beta_0a_i + \dotsb + \beta_Na_{i-N}} + \norm{\beta_{N+1}a_{n-N-1}} + \dotsb + \norm{\beta_na_0} \\
           & \leq \norm{\beta_0a_i + \dotsb + \beta_Na_{i-N}} + \epsilon(\norm{a_{n-N-1}} + \dotsb + \norm{a_0})         \\
           & \leq \norm{\beta_0a_i + \dotsb + \beta_Na_{i-N}} + \epsilon\alpha\nsp{.}
\end{aligned}
\]
Since $a_i \to 0$, it follows $\limsup \abs{d_n} \leq \epsilon \alpha$.
Since $\epsilon$ was arbitrary, we conclude $\limsup \norm{d_n} = 0$, so that $\norm{d_n} \to 0$ and thus $d_n \to 0$.

\proofitem{2}.
Because $\sum a_i$ and $\sum b_i$ converge absolutely, there is an $a$ and an $b$ such that $\sum \norm{a_i} \lt a$ and $\sum \norm{b_i} \lt b$.

Then
\[
\begin{aligned}
\sum^n_{i=0} \norm{c_i} & = \sum_{i=0}^n \norm{\sum_{k=0}^i a_kb_{i-k}}                                                         \\
                        & \leq \sum_{i=0}^n \sum_{k=0}^i \norm{a_kb_{i-k}}                                                      \\
                        & = \norm{a_0b_0} + (\norm{a_0b_1} + \norm{a_1b_0}) + \dotsb + (\norm{a_0b_n} + \dotsb + \norm{a_nb_0}) \\
                        & \leq \sum_{i=0}^n \norm{a_i}\sum_{k=0}^{n-i}\norm{b_k}                                                \\
                        & \leq \sum_{i=0}^n \norm{a_i}b                                                                         \\
                        & \leq ab\nsp{.}
\end{aligned}
\]
Since $n$ was arbitrary, the result follows.
\end{proof}

\begin{proposition}[Comparison test]
\label{prop:comparison-test}
Let $M$ be a normed module.
Let $(x_i)$ be a sequence in $M$, and suppose there is a sequence $(a_i)$ in $\R$ and an $j \in \N$ such $\norm{x_i} \leq a_i$ for all $i \geq j$.
Furthermore suppose that $\sum a_i$ converges.
Then $\sum x_i$ converges absolutely.
\end{proposition}
\begin{proof}
Fix $\epsilon \gt 0$.
By \cref{prop:norm-module-convergent-series-characterization}, we can find $N \geq j$ such that $\sum_{i=n}^m a_i \lt \epsilon$ for all $m \geq n \geq N$.
Then
\[
\norm{\sum_{i=n}^m x_i} \leq \sum_{i=n}^m \norm{x_i} \leq \sum_{i=n}^m a_i \lt \epsilon
\]
for every $m \geq n \geq N$.
Since $\epsilon$ was arbitrary, by \cref{prop:norm-module-convergent-series-characterization}, $\sum \norm{x_i}$ is convergent (since $\R$ is complete), so $\sum x_i$ is absolutely convergent.
\end{proof}

\begin{proposition}[Root test]
\label{prop:root-test}
Let $M$ be a normed module.
Let $(x_i)$ be a sequence in $M$.
The following hold:
\begin{enumerate}
\item if $\limsup \norm{x_i}^{1/i} \lt 1$, then $\sum x_i$ converges absolutely
\item if $\limsup \norm{x_i}^{1/i} \gt 1$, then $\sum x_i$ diverges.
\end{enumerate}
\end{proposition}
\begin{proof}
\proofitem{1}.
We can find $c \lt 1$ such that $\limsup \norm{x_n}^{1/i} \lt c \lt 1$.
There is an $N$ such that $n \geq N$ implies $\norm{x_n}^{1/n} \lt c$.
Then $\norm{x_n} \lt c^n$ for all $n \geq N$, so the result follows from \cref{prop:comparison-test,prop:geometric-series-sum}.

\proofitem{2}.
The conditions imply that $\norm{x_i} \gt 1^i =1$ for infinitely many $i$, so $(x_i)$ does not converge to $0$, and hence $\sum x_i$ cannot converge.
\end{proof}

\begin{lemma}
\label{lem:ratio-root-test-comparison}
Let $(a_i)$ be a sequence in $\R_+$.
Then
\[
\liminf \frac{a_{i+1}}{a_i} \leq \liminf a_i^{1/i} \leq \limsup a_i^{1/i} \leq \limsup \frac{a_{i+1}}{a_i}\nsp{.}
\]
\end{lemma}
\begin{proof}
The middle inequality is clear.

We prove the third (rightmost) inequality, the proof of other inequality is similar.

Fix $R \gt \limsup \frac{a_{i+1}}{a_i}$.
Then there is $N \in \N$ such that for all $i \geq N$, we have $\frac{a_{i+1}}{a_i} \leq R$.
Thus for all $k \in \N$,
\[
a_{N+k} \leq R^ka_N = R^{N+k}\frac{a_N}{R^N}
\]
so that
\[
a_{N+k}^{1/N+k} \leq R \left(\frac{a_N}{R_N}\right)^{N+k}
\]
for all $k \in \N$.
It follows that
\[
\limsup a_i^{1/i} = \limsup a_{N+k}^{1/(N+k)} \leq R\limsup \left(\frac{a_N}{R_N}\right)^{N+k} = R
\]
(see \cref{lem:nth-root-limit}).

Since $R$ was arbitrary, we see that $\limsup a_i^{1/i} \leq \limsup \frac{a_{i+1}}{a_i}$.
\end{proof}

\begin{proposition}[Ratio test]
\label{prop:ratio-test}
Let $A$ be an normed module.
Let $(x_i)$ be a sequence of nonzero elements of $M$.
The following hold:
\begin{enumerate}
\item if $\limsup \frac{\norm{x_{i+1}}}{\norm{x_i}} \lt 1$, then $\sum x_i$ converges absolutely
\item if $\liminf \frac{\norm{x_{i+1}}}{\norm{x_i}} \gt 1$, then $\sum x_i$ diverges.
\end{enumerate}
\end{proposition}
\begin{proof}
This follows from \cref{lem:ratio-root-test-comparison,prop:root-test}.
\end{proof}

\begin{proposition}[Weierstrass M-test]
\label{prop:weierstrass-m-test}
Let $E$ be a normed module.
Let $X$ be a set.
Let $(f_i\colon X \to E)$ be a sequence of maps.
Suppose there are $a_i \in \R$ such that $\norm{f_i(x)} \leq a_i$ for all $i$ and $x$, and such that $\sum a_i$ converges (in $\R$).
Then the partial sums $(\sum_{i=0}^k f_i)$ are uniformly Cauchy.
\end{proposition}
\begin{proof}
Let $\epsilon \gt 0$.
Since $\sum a_i$ converges, there is an $N$ such that for every $m \geq n \geq N$, $\norm{\sum^m_{i=n} a_i} \lt \epsilon$ (\cref{prop:norm-module-convergent-series-characterization}).
Then for every $m \geq n \geq N$ and $x \in X$,
\[
\norm{\sum_{i=m}^n f_i(x)} \leq \sum_{i=m}^n\norm{f_i(x)}\leq \sum_{i=m}^n a_i \lt \epsilon
\]
so we see that the partial sums are uniformly Cauchy.
\end{proof}

\section{Analytic Functions}

\begin{proposition}
\label{prop:radius-of-convergence-formula}
It $(a_i \in \R_+)$ is a sequence, then
\[
\frac{1}{\limsup a_i^{1/i}} = \sup\left\{r \in \R_{\geq 0}:z \in \K,\sum a_ir^i \lt \infty \right\} = \sup\left\{r \in \R_{\geq 0}:a_ir^i \to 0\right\}\nsp{.}
\]
\end{proposition}
\begin{proof}
By \cref{prop:root-test},
\[
\sum a_ir^i \lt \infty \iff \limsup a_i^{1/i}r \lt 1 \iff r \lt \frac{1}{\limsup a_i^{1/i}}\nsp{,}
\]
giving the first equality.
Let $R$ be this common value.

Let $R'$ be the third value.
Assume $a_ir^i \to 0$.
Fix $\rho \lt r$.
There exists $M \lt \infty$ such that $a_ir^n \leq M$ for all $n \in \N$.
Then since $\rho\lt r$,
\[
\sum a_i\rho^i = \sum r^i a_i\left(\frac{\rho}{r}\right)^i \leq  M\sum \left(\frac{\rho}{r}\right)^i \lt \infty\nsp{.}
\]
Hence $\rho \leq R$.
As $\rho$ was arbitrary, $r \leq R$.
This shows that $R' \leq R$.
On the other hand, $\sum a_ir^i \lt \infty$ implies $a_ir^i \to 0$, so it follows $R \leq R'$.
\end{proof}

\begin{exposition}
Let $M$ be a normed module.
Let $f = \sum m_ix^i \in M\lbb x \rbb$.
The \addindex{radius of convergence} of $f$ is the quantity
\[
\frac{1}{\limsup \norm{m_i}^{1/i}} = \sup\left\{R \in \R_{\geq 0}:z \in \K,\sum \norm{m_i}R^i \lt \infty \right\} = \sup\left\{r \in \R_{\geq 0}:\norm{m_i}r^i \to 0\right\}
\]
(cf. \cref{prop:radius-of-convergence-formula}).
\end{exposition}

\begin{theorem}[Cauchy--Hadamard theorem]
\label{thm:cauchy-hadamard-theorem}
Let $\K$ be a valued field.
Let $A$ be a normed algebra over $\K$.
Let $M$ be a normed module over $A$.
Let $f = \sum m_ix^i \in M\lbb x \rbb$.
Let $R$ be the radius of convergence of $f$.
The following hold:
\begin{enumerate}
\item for all $z \in A$ such that $\norm{z}_\sigma \lt R$, $\sum m_iz^i$ converges absolutely
\item if the norm on $A$ is multiplicative, then $\norm{z}_\sigma \gt R$ implies $\sum m_iz^i$ does not converge
\item if the norm on $A$ is multiplicative, then
      \[
      R = \sup\{\norm{z}_\sigma:\text{$\sum m_iz^i$ converges}\}\nsp{.}
      \]
\end{enumerate}
\end{theorem}
\begin{proof}
\proofitem{1}.
Suppose $\norm{z}_\sigma \lt R$.
We have
\[
\begin{aligned}
\limsup \norm{m_iz^i}^{1/i} & \leq \limsup (\norm{m_i}\norm{z^i})^{1/i}                \\
                            & = \limsup \norm{m_i}^{1/i}\norm{z^i}^{1/i}               \\
                            & \leq (\limsup \norm{m_i}^{1/i})(\limsup\norm{z^i}^{1/i}) \\
                            & = R^{-1} \norm{z}_\sigma                                 \\
                            & \lt 1
\end{aligned}
\]
so $\sum a_iz^i$ converges absolutely by \cref{prop:root-test}.

\proofitem{2}.
In this case we have
\[
\limsup \norm{m_iz^i}^{1/i} = \limsup \norm{z}\norm{m_i}^{1/i} = \norm{z}\limsup \norm{m_i}^{1/i} \gt 1\nsp{,}
\]
so the result follows from \cref{prop:root-test}.

\proofitem{3}.
Let $M$ be the supremum.
By \proofitem{1}, $R \leq M$.
If $R \lt M$, there is some $z \in A$ such that $R \lt \norm{z}_\sigma \lt M$ and $\sum m_iz^i$ converges.
This contradicts \proofitem{2}, so we conclude $R = M$.
\end{proof}

\begin{lemma}
\label{lem:radius-of-convergence-uniformly-cauchy}
Let $\K$ be a valued field.
Let $A$ be a normed algebra over $\K$.
Let $M$ be a normed module over $A$.
Let $f = \sum m_ix^i \in M\lbb x\rbb$.
Let $R$ be the radius of convergence of $f$.
Let $r \lt R$, and let $D \eqdef \{z \in A:\norm{z}_\sigma \leq r\}$.
Then the partial sums $(z \mapsto \sum_{i=0}^j a_iz^i)$ are uniformly Cauchy on $D$.
\end{lemma}
\begin{proof}
We can find a $j$ such that for each $n \geq j$, we have $\norm{m_n}^{1/n} \leq \limsup \norm{m_i}^{1/i} + \frac{R-r}{2Rr}$.
Then for all $n \geq j$, for all $z \in D$ we have
\[
\begin{aligned}
\norm{m_nz^n} & \leq \norm{m_n}\norm{z^n}_\sigma                                               \\
              & = \norm{m_n}\norm{z}_\sigma^n                                                & \\
              & \leq \norm{m_n}r^n                                                             \\
              & = (\norm{m_n}^{1/n}r)^n                                                        \\
              & \leq \left(\left(\limsup \norm{m_i}^{1/i} + \frac{R-r}{2Rr}\right)r\right)^n   \\
              & = \left(\frac{r}{R} + \frac{R-r}{2R}\right)^n                                  \\
              & = \left(\frac{R+r}{2R}\right)^n
\end{aligned}
\]
Since $\frac{R+r}{2R} \lt 1$, $\sum_{n = j}^{\infty} \left(\frac{R+r}{2R}\right)^n$ converges, so the result follows from \cref{prop:weierstrass-m-test}.
\end{proof}

\begin{proposition}
\label{prop:complete-valued-module-analytic-continuous}
Let $\K$ be a valued field.
Let $A$ be a normed algebra over $\K$.
Let $M$ be a complete normed module over $A$.
Let $f = \sum a_ix^i \in M\lbb x\rbb$.
Let $R$ be the radius of convergence of $f$.
The following hold:
\begin{enumerate}
\item for every $r \lt R$, the partial sums of $z \mapsto \sum_{k=0}^\infty a_kz^k$ converge uniformly to a unique continuous map $\{z \in A:\norm{z}_\sigma \leq r\}\to \K$
\item there is a unique continuous map $\{z \in A:\norm{x}_\sigma \lt R\} \to \K$ defined by $z \mapsto \sum_{i=0}^\infty m_iz^i$
\end{enumerate}
\end{proposition}
\begin{proof}
\proofitem{1}.
By \cref{lem:radius-of-convergence-uniformly-cauchy} the partial sums $(z \mapsto \sum_{i=0}^j a_iz^i)$ are uniformly Cauchy, and they converge uniquely to a map because $M$ is complete and Hausdorff.
Since the maps $z \mapsto a_iz^i$ are continuous for each $i$, the limit map $K \to M$ is also continuous (\cref{prop:uniform-convergence-continuous-maps}).

\proofitem{2}.
For each $r$, the set $\{z \in A:\norm{z} \lt r\}$ is open because $\norm{\blank}_\sigma$ is upper semicontinuous.
Since
\[
\{z \in A:\norm{z}_\sigma \lt R\} = \bigcup_{r \lt R} \{z \in A:\norm{z} \lt r\}\nsp{.}
\]
the result follows from \cref{prop:continuous-map-sheaf} and \proofitem{1}.
\end{proof}

\begin{proposition}
Let $\K$ be a valued field.
The following hold:
\begin{enumerate}
\item if $\sum a_ix^i \in \K\lbb x\rbb$ and $\sum a'_ix^i \in \K\lbb x\rbb$ have radii of covergence $R$ and $R'$, respectively, then the radius of convergence of $\sum a_ix^i +\sum a'_ix^i$ is at least $\min(R,R')$
\item if $\sum a_ix^i \in \K\lbb x\rbb$ and $\sum a'_ix^i \in \K\lbb x\rbb$ have radii of covergence $R$ and $R'$, respectively, then the radius of convergence of $(\sum a_ix^i)(\sum a'_ix^i)$ is at least $\min(R,R')$
\item if $\sum a_ix^i \in \K\lbb x\rbb$ is a unit with radius of convergence $R > 0$, then $(\sum a_ix^i)^{-1}$ has a nonzero radius of convergence.
\end{enumerate}
\end{proposition}
\begin{proof}
\proofitem{1}.
Omitted.

\proofitem{2}.
Omitted.

\proofitem{3}.
We can assume $a_0 = 1$, since $a_0$ is a unit in $\K$ (\cref{prop:formal-power-series-unit-characterization}).

Choose $r \gt 0$ such that $r \lt \min(1,R)$.
Then $\sum \abs{a_i}r^i \lt \infty$.
There is an $N$ such that $\sum_{i \gt N} \abs{a_i}r^k \leq 1/2$.
Put $M \eqdef \max(\abs{a_0},\dotsc,\abs{a_N})$.
Then
\[
\sum_{i \geq 1} \abs{a_i}r^i \leq M \sum_{1 \leq i \leq N} r^i + \sum_{i \gt N}\abs{a_i}r^k \leq \frac{Mr}{1-r} + \frac{1}{2}\nsp{.}
\]
It follows that there is an $r \lt R$ such that $r \gt 0$ and $\sum_{i \geq 1}\abs{a_i}r^i \lt 1$ (consider $r' \lt \min(r,\frac{1}{2M + 1})$).

Let $\sum b_ix^i$ be the (multiplicative) inverse of $\sum a_ix^i$.
We show by induction that $\abs{b_n} \leq r^{-n}$ for all $n$.
This is clear for $n = 0$.
For $n \gt 0$, by the inductive hypothesis we have
\[
\abs{b_n} = \abs{\sum_{k=1}^n a_kb_{n-k}} \leq \sum_{k=1}^n \abs{a_k}\abs{b_{n-k}} \leq \sum_{k=1}^n \abs{a_k}r^{k-n}
\]
and hence $\abs{b_n}r^n \leq \sum_{k=1}^n \abs{a_k}r^{k} \lt 1$, which completes the induction.

From this it follows that for all $t \lt r$, $\sum \abs{b_i}t^k \lt \infty$, and the result follows.
\end{proof}

\begin{proposition}
\label{prop:hasse-taylor-expansion}
Let $\K$ be a complete valued field.
For every $k \in \N$ and $f = \sum a_ix^i \in \K\lbb x\rbb$ define
\[
f^{(k)} \eqdef  \sum_{i=0}^\infty \binom{i+k}{k}a_{i+k}x^i\nsp{.}
\]
Suppose $f$ has radius of convergence $R$.
The following hold:
\begin{enumerate}
\item for all $k$, $f^{(k)}$ has radius of convergence $R$
\item for every $z_0 \in \K$ such that $\abs{z_0} \lt R$, the series $\sum_k f^{(k)}(z_0)y^k \in \K\lbb y\rbb$ (which makes sense by \proofitem{1}) has radius of convergence at least $R-\abs{z_0}$ and
      \[
      f(z) = \sum_k f^{(k)}(z_0)(z-z_0)^k
      \]
      for every $z \in \K(z_0,R-\abs{z_0})$.
\end{enumerate}
\end{proposition}
\begin{proof}
\proofitem{1}.
Rewrite $f^{(k)} = \sum_{j=k}^\infty \binom{j}{k}a_jx^{j-k}$.
Fix a $k$.
We have $\limsup \abs{a_j}^{1/j} \leq \limsup \abs{\binom{j}{k}a_j}^{1/j}$ since $\binom{j}{k} \geq 1$ for all $j \geq k$.
On the other hand, $\limsup \abs{\binom{j}{k}a_j}^{1/j} \leq \limsup \binom{j}{n}^{1/j}\cdot \limsup \abs{a_j}^{1/j}$.
For all $j$, we have $\binom{j}{k} \leq j^k$, so we have $\binom{j}{k}^{1/j} \leq (j^{1/j})^k$ for all $j$.
By \cref{lem:nth-root-limit}, $(j^{1/j})^k \to 1^k = 1$, so we see that $\limsup \binom{j}{n}^{1/j} \leq 1$.
Thus $\limsup \abs{\binom{j}{k}a_j}^{1/j} = \limsup \abs{a_j}^{1/j}$ and the result follows.

\proofitem{2}.
For every $r$ such that $\abs{z_0} \leq r \lt R$, we have
\[
\begin{aligned}
\sum \abs{f^{(k)}(z_0)}(r-\abs{z_0})^k & \leq \sum_{k=0}^\infty \sum_{j=k}^\infty \binom{j}{k}\abs{a_j}\abs{z_0}^{j-k}(r-\abs{z_0})^k \\
                                       & = \sum_{j,k} \abs{a_{j+k}}\binom{j+k}{j,k}\abs{z_0}^j(r-\abs{z_0})^k                         \\
                                       & = \sum_j \abs{a_j} \sum_{k=0}^j \binom{j}{k}\abs{z_0}^{j-k}(r-\abs{z_0})^k                   \\
                                       & = \sum_j \abs{a_j}r^j                                                                        \\
                                       & \lt \infty\nsp{.}
\end{aligned}
\]
As $r$ was arbitrary, this implies that the radius of convergence of $\sum_k f^{(k)}(z_0)y^k \in \K\lbb y\rbb$ is at least $R- \abs{z_0}$.

Now let $z \in \K(z_0,R-\abs{z_0})$.
Then $0 \leq \abs{z-z_0} \lt R-\abs{z_0}$ implies $\abs{z_0} \leq \abs{z-z_0}+\abs{z_0} \lt R$.
Then taking $r = \abs{z-z_0}+\abs{z_0}$ above, we see that the family $(a_{j+k}\binom{j+k}{j,k}z_0^j(z-z_0)^k)_{j,k}$ is absolutely summable, and hence summable.
By the triangle inequality, $\abs{z} \lt R$, so we have
\[
\begin{aligned}
\sum a_jz^j & = \sum_j a_j \sum_{k=0}^j \binom{j}{k}z_0^{j-k}(z-z_0)^k                \\
            & = \sum_{j,k} a_{j+k}\binom{j+k}{j,k}z_0^j(z-z_0)^k                      \\
            & = \sum_{k=0}^\infty \sum_{j=k}^\infty \binom{j}{k}a_jz_0^{j-k}(z-z_0)^k \\
            & = \sum_k f^{(k)}(z_0)(z-z_0)^k\nsp{.}
\end{aligned}
\]
\end{proof}

\begin{exposition}
Let $\K$ be a complete absolute valued field.
Let $f\colon D \to \K$ be a map defined on an open subset $D \subset \K$.

We say $f$ is \addindex{analytic at a point} $z_0 \in D$ if there is an $R \gt 0$ such that $\K(z_0,R) \subset D$, and there is a $\sum a_ix^i \in \K\lbb x\rbb$ with radius of convergence at least $R$ such that
\[
f(z) = \sum a_i(z-z_0)^i
\]
for all $z \in \K(z_0,R)$.

We say $f$ is \addindex{analytic} if it is analytic at every point in $D$.
\end{exposition}

\begin{example}
Let $\K$ be a complete valued field.
Suppose $\sum a_iz^i$ has radius of convergence $R$.
Then every map $\K(0,R) \to \K$ of the form
\[
z \mapsto \sum a_iz^i
\]
is analytic.
Indeed, this follows from \cref{prop:hasse-taylor-expansion}.
\end{example}

\begin{proposition}
Let $\K$ be a complete absolute valued field.
Let $D \subset \K$ be open.
Then for every $z_0 \in D$, the set of maps $D \to \K$ analytic at $z_0$ form a $\K$-algebra.
\end{proposition}
\begin{proof}
Omitted.
\end{proof}

\section{Stone--Weierstrass}

\begin{exposition}
Define the \addindex{Bernstein polynomials} by
\[
b_{n,k} \eqdef \binom{n}{k}x^k(1-x)^{n-k}
\]
(where $0 \leq k \leq n$).
\end{exposition}

\begin{proposition}
For $k \lt n$ we have
\[
b_{n,k} = (1-x)b_{n-1,k} + xb_{n-1,k-1}\nsp{.}
\]
\end{proposition}
\begin{proof}
We have
\[
\begin{aligned}
(1-x)b_{n-1,k} + xb_{n-1,k-1} & = \binom{n-1}{k}x^k(1-x)^{n-k} +  \binom{n-1}{k-1}x^k(1-x)^{n-k} \\
                              & = \left(\binom{n-1}{k} +  \binom{n-1}{k-1}\right)x^k(1-x)^{n-k}  \\
                              & = \binom{n}{k}x^k(1-x)^{n-k}\nsp{.}
\end{aligned}
\]
\end{proof}

\begin{lemma}
For every $j \leq n$,
\[
\sum_{k=0}^n \binom{k}{j}b_{n,k} = \binom{n}{j}x^j\nsp{.}
\]
\end{lemma}
\begin{proof}
For $n = 0,1,2$, this is straightforward to verify for all $j$.

Assume $n \geq 3$.
We induct on $j,n$.
The case $j = 0$ follows from the binomial theorem (applied to $(x + (1-x))^n$).
For $j \geq 1$ we have
\[
\begin{aligned}
\sum_{k = 0}^n \binom{k}{j}b_{n,k} & = \binom{n}{j}b_{n,n} + \sum_{k=0}^{n-1} \binom{k}{j}b_{n,k}                                                      \\
                                   & = \binom{n}{j}b_{n,n} + \sum_{k=0}^{n-1} \binom{k}{j}\left((1-x)b_{n-1,k} + xb_{n-1,k-1}\right)                   \\
                                   & = \binom{n}{j}x^n + (1-x)\sum_{k=0}^{n-1} \binom{k}{j}b_{n-1,k}  + x\sum_{k=0}^{n-1} \binom{k}{j}b_{n-1,k-1}      \\
                                   & = \binom{n}{j}x^n + (1-x)\binom{n-1}{j}x^j + x\sum_{k=-1}^{n-2}\binom{k+1}{j}b_{n-1,k}                            \\
                                   & = \binom{n}{j}x^n + (1-x)\binom{n-1}{j}x^j + x\sum_{k=-1}^{n-1}\binom{k+1}{j}b_{n-1,k} - x\binom{n}{j}b_{n-1,n-1} \\
                                   & = \binom{n}{j}x^n + (1-x)\binom{n-1}{j}x^j + x\sum_{k=0}^{n-1}\binom{k+1}{j}b_{n-1,k} - \binom{n}{j}x^n           \\
                                   & = (1-x)\binom{n-1}{j}x^j + x\sum_{k=0}^{n-1}\binom{k}{j}b_{n-1,k} + x\sum_{k=0}^{n-1}\binom{k}{j-1}b_{n-1,k}      \\
                                   & = (1-x)\binom{n-1}{j}x^j + x\binom{n-1}{j}x^j + x\binom{n-1}{j-1}x^{j-1}                                          \\
                                   & = \binom{n-1}{j}x^j + x\binom{n-1}{j-1}x^j                                                                        \\
                                   & = \binom{n}{j}x^j\nsp{.}
\end{aligned}
\]
\end{proof}

\begin{lemma}
The following hold:
\begin{enumerate}
\item $\sum_{k=0}^n b_{n,k} =1$
\item $\sum_{k=0}^n \frac{k}{n}b_{n,k} =x$
\item $\sum_{k=0}^n (\frac{k}{n}-x)^2b_{n,k} = \frac{(1-x)x}{n}$.
\end{enumerate}
\end{lemma}
\begin{proof}
\proofitem{1}.
Omitted.

\proofitem{2}.
Omitted.

\proofitem{3}.
We have
\[
\begin{aligned}
\sum_{k=0}^n \left(\frac{k}{n}-x\right)^2b_{n,k} & = \frac{1}{n^2}\sum_{k=0}^n\left(k^2 - 2knx + n^2x^2\right)b_{n,k}          \\
                                                 & =  \frac{1}{n^2} \sum_{k=0}^n\left(k(k-1) - (2nx-1)k + n^2x^2\right)b_{n,k} \\
                                                 & =  \frac{1}{n^2}\left(n(n-1)x^2 - (2nx-1)nx + n^2x^2\right)                 \\
                                                 & = \frac{1}{n^2}(-nx^2 + nx)                                                 \\
                                                 & = \frac{(1-x)x}{n}\nsp{.}
\end{aligned}
\]
\end{proof}

\begin{exposition}
Let $\K$ be a valued field.
A function $X \to \K$ is a \addindex{bounded function} if
\[
\sup_{x \in X} \abs{f(x)} \lt \infty\nsp{.}
\]

The set of bounded functions $X \to \K$ is a normed $\K$-vector space with norm
\[
f \mapsto \sup_{x \in X} \abs{f(x)}\nsp{.}
\]
We call this norm the \addindex{supremum norm}.
Sometimes this is written $\norm{\blank}_\infty$.

Then convergence with respect to the supremum norm is the same a uniform convergence.
\end{exposition}

\begin{theorem}[Weierstrass approximation theorem]
\label{thm:weierstrass-approximation-theorem}
Let $f\colon \interval{0}{1} \to \R$ be continuous.
Define
\[
b_n(f) \eqdef \sum_{k=0}^n f(k/n)b_{n,k}
\]
For every $\epsilon \gt 0$, there $N$ such that $n \gt N$ implies
\[
\norm{f(x) - b_n(f)(x)}_\infty \lt \epsilon\nsp{.}
\]
\end{theorem}
\begin{proof}
Since $f$ is continuous and $\interval{0}{1}$ is compact, we can choose $\delta \gt 0$ such that $\abs{f(x)-f(y)} \lt \frac{\epsilon}{2}$ for all $x$ and $y$.
By \cref{thm:extreme-value-theorem}, $\norm{f}_\infty \lt \infty$.
We have
\[
\begin{aligned}
\abs{b_n(f)(x) - f(x)} & = \abs{\sum f(k/n)b_{n,k}(x) - f(x)}                                                                                       \\
                       & = \abs{\sum  \left(f(k/n) -f(x) \right)b_{n,k}(x)}                                                                         \\
                       & \leq \sum  \abs{\left(f(k/n) -f(x) \right)b_{n,k}(x)}                                                                      \\
                       & = \sum  \abs{f(k/n) -f(x)}b_{n,k}(x)                                                                                       \\
                       & = \sum_{\abs{k/n-x} \lt \delta} \abs{f(k/n) -f(x)}b_{n,k}(x) + \sum_{\abs{k/n-x} \geq \delta} \abs{f(k/n) -f(x)}b_{n,k}(x) \\
                       & \leq \frac{\epsilon}{2}\sum_{\abs{k/n-x} \lt \delta}b_{n,k}(x) + 2\norm{f}_\infty \sum_{\abs{k/n-x} \geq \delta}b_{n,k}(x) \\
                       & \leq \frac{\epsilon}{2}\sum_{k=0}^nb_{n,k}(x) + 2\norm{f}_\infty \sum_{\abs{k/n-x} \geq \delta}b_{n,k}(x)                  \\
                       & \leq \frac{\epsilon}{2} + 2\norm{f}_\infty \sum_{\abs{k/n-x} \geq \delta}b_{n,k}(x)\nsp{.}
\end{aligned}
\]
We can write
\[
\sum_{\abs{k/n-x} \geq \delta}b_{n,k}(x) = \sum_{\abs{k/n-x} \geq \delta} \frac{\abs{k/n -x}^2}{\abs{k/n-x}^2}b_{n,k}(x) \leq \frac{1}{\delta^2}\sum_{\abs{k/n-x} \geq \delta} \frac{1}{\abs{k/n-x}^2}b_{n,k}(x) \leq \frac{x(1-x)}{n\delta^2}\nsp{,}
\]
so
\[
\abs{b_n(f)(x) - f(x)} \leq \frac{\epsilon}{2} + 2\norm{f}_\infty\frac{x(1-x)}{n\delta^2} \leq \frac{\epsilon}{2} + \frac{2\norm{f}_\infty}{n\delta^2}\nsp{.}
\]
Hence if we choose $N$ large enough, the result follows.
\end{proof}

% https://www.jirka.org/ra/html/sec_stoneweier.html

