\chapter{Classical Probability Theory}

\section{Conditional Probability}

\section{Independence}

\begin{exposition}
The \addindex{cumulative distribution function} or \addindex{cdf} of a random variable $X$ is the function $F(x) = \P(X \leq x)$.
This completely characterizes the distribution of $X$ (because half-intervals generate the Borel $\sigma$-algebra).
\end{exposition}

\section{Discrete Distributions}

\begin{exposition}
A \addindex{discrete distribution} is on that is supported on the integers.
If $X$ is distributed discretely, then it is determined by its density function $f(n) = \P(X = n)$.
\end{exposition}

\begin{exposition}
The \addindex{Binomial distribution} $\Binomial(n,p)$ is the (discrete) distribution such that if $X \sim \Binomial(n,p)$, then
\[
\P(X = k) = \binom{n}{k}p^k(1-p)^{n-k}\nsp{.}
\]

This has expectation $np$ and variance $npq$, where $q = 1-p$.
\end{exposition}

\begin{exposition}
The \addindex{Bernouilli distribution} $\Bernouilli(p)$ is by definition $\Binomial(1,p)$.
\end{exposition}

\begin{exposition}
The \addindex{uniform distribution} (on some measurable set of finite measure) has pdf $f(x) \propto c$ for some fixed $c$.

Usually the set of finite measure will be $\interval{0}{1}$.
In this case the expectation is $1/2$ and the variance is $1/12$.
\end{exposition}

\section{Exponential Distribution}

\begin{exposition}
A \addindex{continuous random variable} is one such that its probability distribution absolutely continuous (with respect to the Lebesgue measure), i.e. has a Radon--Nikodym derivative.
We call this Radon--Nikodym derivative the \addindex{probability distribution function} or \addindex{pdf}.
\end{exposition}

\begin{exposition}
By the Lebesgue differentiation theorem, we can get the pdf of a continuous random variable (as an element of $L^1$) from its cdf by differentiation.
\end{exposition}

\begin{exposition}
The \addindex{exponential distribution} with \addindex{rate} $\lambda \gt 0$ has pdf $f(x) \propto \exp(-\lambda x)$.
Note
\[
\int_0^\infty \exp(-\lambda x)\dd{x} = \frac{-1}{\lambda}\exp(-\lambda x)\eval_{x=0}^\infty = 0 - \frac{-1}{\lambda} = \frac{1}{\lambda}\nsp{.}
\]

It has expectation $1/\lambda$ and variance $1/\lambda^2$.
\end{exposition}

\begin{exposition}
The cdf of $\Exponential(\lambda)$ is $1-\exp(-\lambda x)$.
\end{exposition}

\begin{proposition}
Let $U$ be a uniform random variable (on $\interval{0}{1}$).
Then $-\log(U)$ is an exponential random variable (with rate $1$).
\end{proposition}
\begin{proof}
If $X = -\log(U)$, then $\dd{x} = -1/u\dd{u}$, so $\dd{u} = \exp(-x)\dd{x}$.
Thus $f(x) \propto \exp(-x)$.
\end{proof}

\begin{proposition}
Let $X \sim \Exponential(\lambda)$.
Then $cX \sim \Exponential(\lambda/c)$.
\end{proposition}
\begin{proof}
If $Y = cX$, then $\dd{y} = c\dd{x}$, so $\dd{x} = \frac{1}{c}\dd{y}$.
Then $f(y) \propto \frac{1}{c}\exp(-\lambda X/c)$.
\end{proof}

\begin{exposition}
A random variable $X$ is \addindex{memoryless} if $\P(X \geq s + t \given X \geq s) = \P(X \geq t)$.
\end{exposition}

\begin{theorem}
Let $X$ be a positive memoryless random variable.
Then $X \sim \Exponential(\lambda)$ for some $\lambda \gt 0$.
\end{theorem}
\begin{proof}
Let $G(x) \eqdef \P(X \geq x)$.
Then we have $G(s + t) = G(s)G(t)$ for all $s,t$.
Taking the logarithm we see that $\log(G(s+t)) = \log(G(s)) + \log(G(t))$, i.e. $\log \circ G$ satisfies Cauchy's functional equation.
Since $\log \circ G$ is measurable from $\R \to \R$, $\log(G(t)) = -\lambda t$ for some $\lambda$.
Hence $G(t) = \exp(-\lambda t)$.
Because $G$ is decreasing and nonzero, $\lambda \geq 0$.
If $\lambda = 0$, then $G(t) = 1$ for all $t$, which is a contradiction, so we must have $\lambda \neq 0$.
Thus $1-G$ is the cdf of an exponential.
\end{proof}

\begin{proposition}
Let $X \sim \Exponential(\lambda)$ and $Y \sim \Exponential(\mu)$ be indepedent.
Then $\min(X,Y) \sim \Exponential(\lambda + \mu)$.
\end{proposition}
\begin{proof}
Let $Z \eqdef \min(X,Y)$.
Then
\[
\P(Z \geq t) = \P(X \geq t,Y \geq t) = \P(X \geq t)\P(Y \geq t)  = \exp(-(\lambda+\mu)t)\nsp{.}
\]
\end{proof}

\section{Gamma Distribution}

\begin{exposition}
Define the \addindex{gamma function}
\[
\Gamma(\alpha) \eqdef \int_0^\infty x^{\alpha-1}e^{-x}\dd{x}\nsp{.}
\]
Note we have
\[
\int_0^\infty x^{\alpha-1}e^{-x}\dd{x} = \int_0^1 x^{\alpha-1}e^{-x}\dd{x} + \int_1^\infty x^{\alpha-1}e^{-x}\dd{x} \leq  \int_0^1 x^{\alpha-1}\dd{x}\nsp{,}
\]
and since $x^{\alpha-1} \leq e^{x/2}$ for large enough $x$, and $\int e^{-x/2} \lt \infty$, the gamma function is real-valued.
\end{exposition}

\begin{exposition}
The \addindex{gamma distribution} $\GammaD(\alpha,\lambda)$ has density $f(x) \propto x^{\alpha-1}\exp(-\lambda x)$.
\end{exposition}

\begin{example}
We have $\GammaD(1,\lambda) = \Exponential(\lambda)$.
\end{example}

\begin{proposition}
Let $X \sim \GammaD(\alpha,\lambda)$.
Then $cX \sim \GammaD(\alpha,\lambda/c)$.
\end{proposition}
\begin{proof}
If $Y = cX$, then $\dd{y} = c\dd{x}$, so $\dd{x} = \frac{1}{c}\dd{y}$.
Then $f(y) \propto \frac{1}{c^\alpha}x^{\alpha-1}\exp(-\lambda X/c)$.
\end{proof}

\begin{proposition}
The sum of an independent $\GammaD(\alpha,\lambda)$ and a $\GammaD(\beta,\lambda)$ has distribution $\GammaD(\alpha+\beta,\lambda)$.
\end{proposition}
\begin{proof}
You can check this by checking characteristic functions.
The characteristic function is (essentially) the fourier transform of the density.
We recall that the fourier transform is injective on $L^1$, and takes convolutions to products.
\end{proof}

\section{Poisson Distribution}

\begin{exposition}
A \addindex{point process} is an $\N$-valued random measure (...a locally finite transition kernel...) on a good space $S$ (locally compact second countable Hausdorff).
\end{exposition}

\begin{exposition}
Let $\xi$ be a point process.
Then we write $N(E) \eqdef \card{\xi(\blank,E)}$.
Each $N(E)$ is a random variable.
\end{exposition}

\begin{exposition}
We say $\xi$ is a \addindex{simple point process} if for all $s \in S$, $N(\{s\}) \in \{0,1\}$ almost surely.

\end{exposition}

\begin{exposition}
Put $N(t) \eqdef N(\linterval{-\infty}{t})$.
\end{exposition}

\begin{exposition}
We suppose $S = \rinterval{0}{\infty}$.
Define the arrival times $T_k \eqdef \inf\{t:N(t) = k\}$.
The random variable
\[
T_k - T_{k-1} = \inf\{t:N(T_{k-1}+t)-N(T_{k-1}) \gt 0\}
\]
is called the $k$-th \addindex{interarrival time}.
\end{exposition}

\begin{exposition}
We say $\xi$ has \addindex{stationary increments} if for all $t \geq 0$, $N(s+t) - N(s)$ has a distribution depending only on $t$ (i.e. the distribution of $N(I)$ for an interval $I$ depends only on its length).
\end{exposition}

\begin{proposition}
If $\xi$ has stationary increments, then all interarrival times have the same distribution.
\end{proposition}
\begin{proof}
Omitted.
\end{proof}

\begin{exposition}
We say $\xi$ has \addindex{independent increments} if for all disjoint intervals $I$ and $J$, $N(I)$ and $N(J)$ are independent.
\end{exposition}

\begin{lemma}
If $\xi$ has independent increments, then the interarrival times are independent.
\end{lemma}
\begin{proof}
Omitted.
\end{proof}

\begin{theorem}
Let $\xi$ be a simple point process on $\rinterval{0}{\infty}$ with stationary and independent increments.
Then the interarrival times are distributed as $\Exponential(\lambda)$ for some fixed $\lambda \gt 0$.
\end{theorem}
\begin{proof}
Because all interarrival times have the same distribution, it suffices to show that the first interarrival time $X_1$ is exponentially distributed.
Let $n \eqdef N(T_0)$.
For all $s,t \geq 0$,
\[
\begin{aligned}
\P(X_1 \geq s + t) & = \P(N(s+t) = n)                      \\
                   & = \P(N(s) = n,N(s+t)-N(s) = n)        \\
                   & = \P(N(s) = n)\P(N(t) = n)            \\
                   & = \P(X_1 \geq s)\P(X_1 \geq t)\nsp{.}
\end{aligned}
\]
so $X_1$ is memoryless, and hence exponentially distributed.
\end{proof}

\begin{exposition}
A \addindex{Poisson process} is a simple point process on $\rinterval{0}{\infty}$ with stationary independent increments.

If $X_1 \simeq \Exponential(\lambda)$, then we call $\lambda$ the \addindex{rate} of the Poisson process.
\end{exposition}

\begin{proposition}
The distribution of the $k$-th arrival time of a Poisson process is distributed as $\GammaD(k,\lambda)$.
\end{proposition}
\begin{proof}
Omitted.
\end{proof}

\section{Beta Distribution}

\begin{problem}
Show that $\int_0^1 x^{\alpha-1}(1-x)^{\beta -1}\dd{x} \lt \infty$ (the only only difficult part is when $\alpha \lt 1$ or $\beta \lt 1$).
\end{problem}

\begin{exposition}
The \addindex{beta distribution} $\BetaD(\alpha,\beta)$ has density $f(x) \propto x^{\alpha-1}(1-x)^{\beta -1}$ supported on $\interval{0}{1}$ (we leave $f(0)$ and $f(1)$ undefined).
\end{exposition}

\begin{proposition}
Suppose $X$ and $Y$ are independent, $X \sim \GammaD(\alpha,\lambda)$, and $Y \sim \GammaD(\beta,\lambda)$.
Let $T \eqdef X + Y$ and $W \eqdef \frac{X}{X+Y}$.
Then $T$ and $W$ are independent and $W \sim \BetaD(\alpha,\beta)$.
\end{proposition}
\begin{proof}
For simplicity we can assume $\lambda = 1$.
We compute the joint density $f(t,w)$.
We have $\pdv{(t,w)}{(x,y)} = \begin{psmallmatrix}1 & 1 \\ \frac{y}{x+y} & \frac{-x}{x^2+y^2} \end{psmallmatrix}$, so $\abs{\pdv{(x,y)}{(t,w)}} = \abs{\pdv{(t,w)}{(x,y)}}^{-1} = t$, giving
\[
f(t,w) = f_{X,Y}(x(t,w),y(t,w))\abs{\dv{(x,y)}{(t,w)}} \propto w^{\alpha-1}(1-w)^{\beta-1}t^{\alpha+\beta-1}\exp(-t)\nsp{.}
\]
This density factorizes, so $T$ and $W$ are independent.
Furthermore, integrating out $T$ (which is distributed as $\GammaD(\alpha+\beta,1)$), the result follows.
\end{proof}

\section{Order Statistics}

\begin{exposition}
Let $X_1,\dotsc,X_n$ be a family of random variables.
The $k$-th \addindex{order statistic} $X_{(k)}$ is the $k$-th smallest of the values.
\end{exposition}

\begin{proposition}
Let $X_1,\dotsc,X_n$ be iid continuous random variables with pdf $f$.
Then the joint density of $(X_{(1)},\dotsc X_{(n)})$ is
\[
f(x'_1,\dotsc,x'_n) = n!\prod f(x'_i)\chi_{x'_1 \lt \dotsb \lt x'_n}\nsp{.}
\]
\end{proposition}
\begin{proof}
Omitted.
\end{proof}

\begin{proposition}[Rényi Representation]
Let $X_1,\dotsc,X_n$ be iid $\Exponential(1)$ random variables.
Put $Y_i \eqdef (n-i+1)X_{(i)} - X_{(i-1)}$.
Then $X_{(k)} = \sum_{i=1}^k \frac{1}{n-i+1}Y_i$ and $Y_1,\dotsc,Y_n$ are iid $\Exponential(1)$ random variables.
\end{proposition}
\begin{proof}
For all real numbers $x_1,\dotsc,x_n$, we have
\[
\sum_{i=1}^n x_i  = \sum_{i=1}^n (n-i+1)\left(x_i-x_{i-1}\right)
\]
(where $x_0 = 0$).
The joint density of $(X_{(1)},\dotsc X_{(n)})$ is
\[
f(x_1,\dotsc,x_n) = n!\exp\left(-\sum x_i\right) = n!\exp\left(- \sum_{i=1}^n (n-i+1)\left(x_i-x_{i-1}\right)\right)\nsp{.}
\]
The Jacobian of the change of variables $Y_i = (n-i+1)\left(X_{(i)} - X_{(i-1)}\right)$ is constant, we obtain the joint density of $(Y_1,\dotsc,Y_n)$ as
\[
f(y_1,\dotsc,y_n)\propto  n!\exp\left(-\sum y_i\right)
\]
and the result follows.
\end{proof}

\begin{proposition}
Let $X_1,\dotsc,X_{n+1}$ be iid $\Exponential(1)$ random variables.
Define
\[
U_{(k)} = \frac{X_1 + \dotsb + X_k}{X_1 + \dotsb + X_{n+1}}\nsp{.}
\]
Then $U_{(1)},\dotsc U_{(n)}$ has the same joint distribution as the order statistics of iid $\Uniform(0,1)$ random variables $U_1,\dotsc,U_n$.
In particular, $U_{(k)} \sim \BetaD(k,n-k+1)$.
\end{proposition}
\begin{proof}
The joint density of $(X_1,\dotsc,X_{n+1})$ is
\[
f(x_1,\dotsc,x_{n+1}) = \exp\left(-\sum x_i\right)\nsp{.}
\]
Let $Y_k \eqdef \frac{X_1 + \dotsb + X_k}{X_1 + \dotsb + X_{n+1}}$ for $1 \leq k \leq n$, and $Y_{n+1} \eqdef X_1 + \dotsb + X_{n+1}$.
Put $S_k \eqdef x_1 + \dotsb + x_k$, $S \eqdef S_{n+1}$.
We calculate
\[
\det\left(\pdv{(y_1,\dotsc,y_{n+1})}{(x_1,\dotsc,x_{n+1})}\right) = \frac{1}{S^{2n}}\det\begin{pmatrix}
S - S_1 & -S_1   & \cdots & \cdots & -S_1   \\
S-S_2   & S-S_2  & -S_2   & \cdots & -S_2   \\
\vdots  & \vdots & \ddots & \vdots & \vdots \\
S-S_n   & S-S_n  & \cdots & S-S_n  & -S_n   \\
1       & \cdots & \cdots & 1      & 1      \\
\end{pmatrix}
= \frac{1}{S^{2n}}\det \begin{pmatrix}
S      & 0      & \cdots & \cdots & 0      & -S_1   \\
0      & S      & 0      & \cdots & 0      & -S_2   \\
\vdots & \vdots & \ddots & \ddots & \vdots & \vdots \\
0      & \cdots & 0      & S      & 0      & \vdots \\
0      & \cdots & \cdots & 0      & S      & -S_n   \\
0      & \cdots & \cdots & \cdots & 0      & 1      \\
\end{pmatrix}
= \frac{1}{S^{2n}}S^n = S^{-n}\nsp{.}
\]
(by subtracting the $k$-th column from the $k-1$-th column for all $2 \leq k \leq n+1$).
Then $\abs{\pdv{(x_1,\dotsc,x_{n+1})}{(y_1,\dotsc,y_{n+1})}} = y_{n+1}^n$, so
\[
f(y_1,\dotsc,y_{n+1}) = f(x_1(y_1,\dotsc,y_{n+1}),\dotsc,x_{n+1}(y_1,\dotsc,y_{n+1}) \abs{\pdv{(x_1,\dotsc,x_{n+1})}{(y_1,\dotsc,y_{n+1})}}  = \exp(-y_{n+1})y_{n+1}^n\nsp{,}
\]
where $y_1 \lt \dotsb \lt y_n$ and $y_{n+1} \in \rinterval{0}{\infty}$.
Integrating out $y_{n+1}$ we obtain
\[
f(y_1,\dotsc,y_n) = \int_0^\infty \exp(-y_{n+1})y_{n+1}^n\dd{y_{n+1}} = \Gamma(n+1) = n!
\]
and the result follows.
\end{proof}
