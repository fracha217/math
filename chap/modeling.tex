\chapter{Modeling}
\label{chap:modeling}

\section{Measurement}

\begin{exposition}
Statistics is at its core about \emph{data}.

Data is assumed to be organized into \addindex{samples} (or \addindex{instances}, \addindex{observations}).

Typically we assume samples are independent.
In the dependent case we have to go to mixed models.
\end{exposition}

\begin{exposition}
We generally seek to predict one variable in terms of the others.

In our setup, one the variables (columns) is designated as the \addindex{target variable} (or \addindex{response variable}).
The others are designated as \addindex{predictive variables} (predictors).
\end{exposition}

\begin{remark}
A step after prediction would be explanation, which when then lead to manipulation.

The role of theory is to generate explanations.
\end{remark}

\begin{exposition}
A \addindex{statistical model} is ...?
We make \addindex{statistical assumptions} ...

The \addindex{training set} is used (directly) to develop the model.
The \addindex{validation set} is used to evaluate the model.
\end{exposition}

\begin{exposition}
We generally care about variables that are either numerical or categorical.

We have the following so-called scales of measurement:
\begin{enumerate}
\item nominal, i.e. pure categorical (corresponds roughly to membership)
\item ordinal, can be thought of as ranked categories (corresponds roughly to ordered sorting)
\item intervals, which are numerical variables in which the difference of values makes sense
\item intervals, which are numerical variables in which the ratio of values makes sense.
\end{enumerate}

More classifications:
\begin{enumerate}
\item \addindex{continuous additive variable} (real number)
\item \addindex{continuous multiplicative variable} (strictly positive real number)
\item \addindex{binary variable} ($0$ or $1$)
\item \addindex{categorical variable}
\item \addindex{ordinal variable} (categorical with ordering)
\item \addindex{binomial variable} (integers with a maximum?)
\item \addindex{count variable} (integers, unbounded)
\end{enumerate}
\end{exposition}

% https://en.wikipedia.org/wiki/Statistical_data_type

\section{Preliminary Analysis}

Questions to ask:
\begin{enumerate}
\item are the samples independent?
\item what is the response distribution?
\item are predictors correlated?
\item are predictors sparse (many duplicates)?
\item what are the scales of the predictors?
\end{enumerate}

Consider number of samples $n$ versus number of predictive variables $p$ (if $n \lt p$, this may be a problem).

\subsection{Categorical Variables}

\begin{exposition}
Create a frequency table.
Use this to create a bar chart.

For pure categorical variables, consider sorting on frequency.
For ordinals, it make sense to sort on the ordinal values.
\end{exposition}

\begin{remark}
We do not like line charts (for purely categorical, ordinal may be different?)
Pie charts may look nice, but it is difficult to tell relative proportions.
\end{remark}

\begin{exposition}
Do a sanity check.

Look for:
\begin{enumerate}
\item redundant labels - certain groups should be merged
\item unknown/wrong labels
\item swapped labels
\item low sample sizes.
\end{enumerate}

A bit on the sample sizes.
If a group does not have many samples, this may cause difficulty in downstream analyses.
\end{exposition}

\subsection{Numeric Variables}

\begin{exposition}
Create a histogram.
\end{exposition}

\begin{exposition}
What to look for:

\begin{enumerate}
\item normally distributed variables are good
\item check outliers (data quality)
\item if the data is skewed (this can cause problems during estimation)
\item zero inflated data (traditional techniques may not apply)
\item bimodal data (means we should probably add a categorical variable)
\end{enumerate}
\end{exposition}

\subsection{Central Tendencies}

\begin{exposition}
The three most common ways to measure are mean, median, and mode (this is $\ell^2$, $\ell^1$, and $\ell^\infty$).
\end{exposition}

\begin{exposition}
We like the mean when data is not skewed/no outliers.

In the presence of skew/outliers, the median is better.
\end{exposition}

\begin{exposition}
We like the mode for categorical variables.

This is not very helpful when data is uniform, so we should say that the data is uniform.
\end{exposition}

\begin{exposition}
We can compute the range, standard deviation, and median absolute deviation.
\end{exposition}

\begin{exposition}
The range is not so good in the presence of outliers/skew.
\end{exposition}

\begin{exposition}
The standard deviation is not so good in the presence of outliers/skew.
\end{exposition}

\begin{exposition}
The median absolute deviation is better in the presence of outliers/skew.
\end{exposition}

\subsection{Bivariate Analysis}

\begin{exposition}
For numerics:
Plot one variable against another with loess.
\end{exposition}

\begin{exposition}
We are interested in the following:
\begin{enumerate}
\item positive/zero/negative relationships.
\item nonlinearity (check the shape of the loess).
\item outliers.
\end{enumerate}

For outliers, we want to be particularly aware of high leverage datapoints.
\end{exposition}

\begin{exposition}
For categorical, do a beeswarm plot.
\end{exposition}

\begin{exposition}
What to look out for:
\begin{enumerate}
\item different sample sizes - small sample sizes just mean we should be less confident in our conclusions
\item skew.
\end{enumerate}
\end{exposition}

\begin{exposition}
We can do a logistic plot.

We can do an association plot.
\end{exposition}

\begin{exposition}
Check correlations.
\end{exposition}

\begin{exposition}
For numerical variables condition on a categorical, the analogue for $r$ (the correlation), is Cohen's $d$.
\end{exposition}

\section{Data Preprocessing}

The characteristics of the model used determine which feature transformations are necessary.

\subsection{Unsupervised Techniques}

\subsubsection{Dates}
\begin{enumerate}
\item days since a reference date (e.g. Jan 01 1970)
\item Isolating month, day, year, day of week as separate predictors
\item numeric day of year (ignoring year), i.e. Julian date
\item holiday indicator, school day indicator, etc.
\end{enumerate}

\subsubsection{Centering and Scaling}

Centering and scaling will put numerical features to have a similar scale (and location)

The disadvantage is that the features are somewhat less interpretable.

\subsubsection{Removing Skewness}

A general rule of thumb to consider is that skewed data whose ratio of the highest value to the lowest value is greater than 20 have significant skewness.
Skewness is defined
\[
\frac{\sum (x_i - \overline{x})^3}{(n-1)\left(\frac{\sum(x_i - \overline{x})^2}{n-1}\right)^{3/2}}
\]
where x is the predictor variable, n is the number of values, and x is the sample mean of the predictor (it is the third standardized moment).

Replacing the data with the log, square root, or inverse may help to remove the skew (make sure this makes sense).

Try the Box--Cox transformation.

Why remove skew?
\begin{enumerate}
\item in the context of (linear) regression, make the residuals look as normal as possible.
\item if the model/subsequent transformation is sensitive to outliers, this may help
\end{enumerate}

\subsubsection{Removing Outliers}

First make sure there are no measurement errors.

If the sample size is small, points may appear to be outliers because data is skewed or data is lacking (e.g. a special group of the population not sampled enough yet)

To mitigate, you can use models resistant to outliers (e.g. random forests, SVM)

If the model is sensitive to outliers, you can use spatial sign (center and scale a set of variables, then projection onto the sphere)

\subsubsection{PCA}

Produces uncorrelated features.

However, extracted features may not be related to the response (see PLS)

Make sure to remove skew (e.g. Box--Cox), center, and scale before applying.

To determine target dimension, make a scree plot.
In an automated approach, cross validate.

In exploratory analysis:

Look for separation by plotting pairs of principal components against each other.
\begin{enumerate}
\item be wary of the scales of the PCs
\item don't overinterpret small scale/small variance components
\item if the percentage of variance explained is small, don't overintepret the scatter plots
\end{enumerate}

Look at the coefficients in the PCA change of basis to see how the predictor variables affect the PCs.

\subsubsection{Missing Data}

Make sure to understand why the data is missing
\begin{enumerate}
\item if we know it is relate to outcome, it can be used to produce a better model
\item missing data can be contribute to model bias (so you will need to revisit your modeling assumptions)
\end{enumerate}

\addindex{Censored data} is data where the exact value is missing, but we know something about its value.
In this case you can use a censored value for the observed value.

Missing values are often related to the predictor variables (rather than the sample).
So the missing data will be concentrated in a subset of the predictors rather than randomly across all predictors.
If too much missing data for a predictor, consider removing that predictor entirely.

When the missing values are concentrated in specifc samples, you can remove some samples, provided that the missingness is not informative (though this may be undesirable when the total sample size is small).

Trees can handle mising data.

You can impute the missing data.
This works by training a model to predict the missing values (note that e.g. just returning the mean value is technically a model).

If the number of predictors affected by missing values is small, an exploratory analysis of the relationships between the predictors is a good idea.
For example, visualizations or methods like PCA can be used to determine if
there are strong relationships between the predictors.
If a variable with missing values is highly correlated with another predictor that has few missing values, a focused model can often be effective for imputation.

You can use $K$-nn to impute data (though you need to tune $K$ and it may be expensive to compute nearest neighbors).

If one variable is highly correlated with another, you can use a simple linear regression to predict missing values.

\subsubsection{Removing Predictors}

Decreases computational time.
May lead to more interpretable and parsimonious model.
Some models do not work well when predictors have degenerate distributions.

We can get improved model performance and (numerical) stability.

Examples:
May need to get rid of zero variance predictors (constant valued predictors) or so-called ``near-zero variance'' predictors.

Rule of thumb for finding near-zero variance predictors:
\begin{enumerate}
\item the fraction of unique values over the sample size is low (say $10\%$)
\item the ratio of the frequency of the most prevalent value to the frequency of the second most prevalent value is large (say around $20$).
\end{enumerate}

\subsubsection{Collinearity}

\addindex{Collinearity} is when the predictors have a substantial correlation (let us include multicollinearity here too).

Look at the correlation matrix.

You can use PCA.
For example, if the first PC accounts for a lot of variance, then this implies there is a group of predictors that represent the same information (well, this is because the predictors all contribute to the variance of the first component, which is close to the total variance).
The PCA coefficients can then be use to get the related predictors.

Why avoid correlated predictors?
Redundant predictors add (modeling) complexity, fewer variables are better.
Mathematically, correlated predicts lead to unstable models, numerical errors.

To remove correlated predictors, the following method is suggested.
\begin{enumerate}
\item calculate correlations
\item find the two predictors with largest (absolute) correlation
\item remove the predictor with larger average correlation (average over the other predictors)
\item repeat until no correlations exceed some threshold.
\end{enumerate}

\subsubsection{Adding Predictors}

Dummy variables.
One-hot encoding.

\subsubsection{Binning Predictors}

\subsection{Supervised Techniques}

\section{Data Splitting}

Split how you want to predict.
e.g. if predict new instances, split by time
predict within population, take sample

How to decide the amount of data in training set?

The issue arises when we don't have a lot of data.
In this case one can try resampling (e.g. cross validation)

\section{Feature Selection}

\section{Performance Evaluation}

\begin{exposition}
Check histogram of residuals.
\end{exposition}

\begin{exposition}
To see the impact of residuals, do a ``sensitivity analysis.''
Fit a robust model and see if the models are different.
If so, then the residuals are a problem.
\end{exposition}

\begin{exposition}
You should see a straight line on the residual dependence plot.
\end{exposition}

\begin{exposition}
You should also see a straight line on the SL (spread location) plot.

We can kind of fix it with weighted least squares.
\end{exposition}

\begin{exposition}
To deal with outliers we can do the ``sensitivity analyis'' by fitting a robust model.
If fits are different, report both results and choose a model.
Or, on seeing a difference, we could wait for more data.

Outliers only really matter for small sample sizes.
\end{exposition}

Why RMSE? for regression

We can compare ROC curves for dev vs validation.
This is an art.

Gains Table, lift chart.

\section{Model Selection}

\section{Feature Importance}

to model zero-inflated data, it's common to use models that combine logistic regression (to predict the 0 vs 1) and logistic regression (to predict 1+).

% source
% applied predictive modeling
%https://simplistics.net/stats_modeling

