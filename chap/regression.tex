\chapter{Regression}
\label{chap:regression}

\section{Linear Regression}
\label{sec:linear-regression}

\begin{proposition}
\label{prop:moore-penrose-pseudoinverse-existence-characterization}
Let $\mcalH$ and $\mcalK$ be Hilbert spaces.
Let $T \in \mcalB(\mcalH,\mcalK)$.
The following are equivalent:
\begin{enumerate}
\item $\im(T)$ is closed
\item $\im(T^*T) = \im(T^*)$ is closed
\item there exists a unique operator $T^\dag \in \mcalB(\mcalK,\mcalH)$ such that
      \begin{enumerate}
      \item $TT^\dag T = T$
      \item $T^\dag T T^\dag = T^\dag$
      \item $TT^\dag$ is self-adjoint
      \item $T^\dag T$ is self-adjoint
      \end{enumerate}
\item there exists an operator $T^\dag \in \mcalB(\mcalK,\mcalH)$ such that $TT^\dag T = T$ and $TT^\dag$ is a projection.
\end{enumerate}
\end{proposition}
\begin{proof}
\itemimply{1}{2}.
By \cref{prop:bounded-linear-operator-adjoint-properties}, $\im(T) = \ker(T^*)^\perp$.
Since $T^*$ is an isometry on $\ker(T^*)^\perp$ and $\ker(T^*)^\perp$ is complete, $T^*(\im(T))$ is closed.
But $T^*(\im(T)) = \im(T^*)$ since $\im(T) = \ker(T^*)^\perp$ (i.e. $\im(T) \oplus \ker(T^*) = \mcalK$), so $\im(T^*)$ is closed.

We show that $\im(T^*) = \im(T^*T)$.
It suffices to show that $\im(T^*) \subset \im(T^*T)$ (the other inclusion is clear).

Let $\eta \in \mcalK$.
Since $\im(T)$ is closed, $\mcalH = \im(T) \oplus \im(T)^\perp = \im(T) \oplus \ker(T^*)$ (\cref{prop:bounded-linear-operator-adjoint-properties,prop:hilbert-space-closed-subspace-direct-summand-characterization,prop:hilbert-space-topologically-generated-subspace}).
Let $\eta'$ be the projection of $T^*\eta$ onto $\im(T)$.
Choose $\xi \in \mcalH$ such that $T\xi = \eta'$.
Then $T^*T\xi = T^*\eta' = T^*\eta$ (since $\eta -\eta' \in \ker(T^*)$).
The result follows.

\itemimply{2}{3}.
Let's settle uniqueness first.
Let $S$ also satisfy the required properties.
Then
\[
\begin{aligned}
TT^\dag & = TT^\dag T T^\dag                   \\
        & = (TST)T^\dag(TST)T^\dag             \\
        & = (TS)(TT^\dag)(TS)(TT^\dag)         \\
        & = (TS)^*(TT^\dag)^*(TS)^*(TT^\dag)^* \\
        & = S^*(TT^\dag T)^*S^* (T T^\dag T)^* \\
        & = (TSTS)^*                           \\
        & = (TS)^*                             \\
        & =  TS\nsp{.}
\end{aligned}
\]
Similarly,
\[
\begin{aligned}
T^\dag T & = T^\dag T T^\dag T                    \\
         & = T^\dag(TST)T^\dag(TST)               \\
         & = (T^\dag T)(ST)(T^\dag T)(ST)         \\
         & = (T^\dag T)^*(ST)^*(T^\dag T)^*(ST)^* \\
         & = (TT^\dag T)^*S^*(TT^\dag T)^*S^*     \\
         & = (STST)^*                             \\
         & = (ST)^*                               \\
         & = ST\nsp{.}
\end{aligned}
\]
Hence $T^\dag = T^\dag T T^\dag = STT^\dag = STS = S$.

Let us settle existence.
Let $V \eqdef \ker(T)^\perp = \ker(T^*T)^\perp = \im(T^*T)$ (\cref{prop:bounded-linear-operator-adjoint-properties}).
Then $T^*T$ is a bijective continuous map $V \to V$, so has a continuous inverse since $V$ is closed (\cref{thm:banach-schauder-theorem}).
Let $S$ be the composition $\mcalH \to V \to V \subobj \mcalH$, where $(T^*T)\res_V^\inv$ is in the middle, and put $T^\dagger \eqdef ST^*$.

Then it is straightforward to verify that $T^\dagger$ satisfies the required properties.

\itemimply{3}{4}.
Details left to the reader.

\itemimply{4}{1}.
We have $\im(TT^\dag) \subset \im(T)$, but also $\im(T) = \im(TT^\dag T) \subset \im(TT^\dag)$, so $\im(T) = \im(TT^\dag)$.
Since $TT^\dag$ is a projection, we see that $\im(T)$ is closed (see \cref{prop:hilbert-space-closed-subspace-direct-summand-characterization}).
\end{proof}

\begin{exposition}
\label{exp:moore-penrose-pseudoinverse}
Let $\mcalH$ and $\mcalK$ be Hilbert spaces.
Let $T \in \mcalB(\mcalH,\mcalK)$.
We call $T^\dag \in \mcalB(\mcalK,\mcalH)$ as in \cref{prop:moore-penrose-pseudoinverse-properties} the \addindex{pseudoinverse} of $T$, if it exists (which happens if and only if $\im(T)$ is closed).
\end{exposition}

\begin{proposition}
\label{prop:moore-penrose-pseudoinverse-properties}
Let $\mcalH$ and $\mcalK$ be Hilbert spaces.
Let $T \in \mcalB(\mcalH,\mcalK)$.
Assume $T$ has pseudoinverse $T^\dag$.
The following hold:
\begin{enumerate}
\item $(T^*)^\dag$ exists and equals $(T^\dag)^*$
\item $(T^*T)^\dag$ exists if and only if $T^\dag$ exists, and in this case $(T^*T)^\dag T^* = T^\dag$
\item $T^\dag  T= 1$ if and only if $T$ is injective
\item $TT^\dag$ is the range projection of $T$.
\end{enumerate}
\end{proposition}
\begin{proof}
\proofitem{1}.
Details left to the reader (use uniqueness of the pseudoinverse of $T^*$).

\proofitem{2}.
Assume $T^\dag$ exists.
By \cref{prop:moore-penrose-pseudoinverse-properties}, $\im(T^*T)$ is closed, and thus has $T^*T$ has a pseudoinverse.

Conversely, assume $(T^*T)^\dag$ exists.

Let $S \eqdef (T^*T)^\dag T^*$.
We have
\[
T^* T = (T^*T)(T^*T)^\dag T^*T = T^*TST\nsp{,}
\]
so $T^*T(1-ST) = 0$.
Then $(1-ST)^*T^*T(1-ST) = (T-TST)^*(T-TST) = 0$, so $T = TST$ (\cref{prop:bounded-linear-operator-adjoint-properties}).
We also have
\[
(T^*T)^\dag (T^* T)(T^*T)^\dag = ST(T^*T)^\dag\nsp{.}
\]

Finally, using \proofitem{1} we have
\[
(TS)^* = S^*T^* = ((T^*T)^\dag T^*)^*T^* = T((T^*T)^\dag)^*T^* =  T(T^*T)^\dag T^* = TS\nsp{,}
\]
and
\[
(ST)^* = T^*S^* = T^*T((T^*T)^\dag)^* = T^*T(T^*T)^\dag = (T^*T(T^*T)^\dag)^* = ((T^*T)^\dag)^*T^*T = ((T^*T)^\dag)^*T^*T = (T^*T)^\dag T^*T =  ST\nsp{.}
\]

\proofitem{3}.
If $1 = T^\dag T$, then it is clear that $T$ is injective.

On the other hand, suppose $T$ is injective.
Then $T$ is a monomorphism (in e.g. $\Set$), so $T = T T^\dag T$ implies $1= T^\dag T$.

\proofitem{4}.
We have $\im(TT^\dag) \subset \im(T)$, but also $\im(T) = \im(TT^\dag T) \subset \im(TT^\dag)$, so $\im(T) = \im(TT^\dag)$.
Hence $TT^\dag$ is the projection with image $\im(T) = \cl(\im(T))$, i.e. is the range projection of $T$.
\end{proof}

\begin{proposition}
\label{prop:least-squares-solution}
Let $A \in \mcalB(\mcalH,\mcalK)$ have closed image.
Fix $b \in \mcalK$.
Let $z \eqdef A^\dag b$.
The following hold:
\begin{enumerate}
\item for every $x \in \mcalH$, $\norm{Ax-b} \geq \norm{Az-b}$
\item $\norm{Ax-b} = \norm{Az-b}$ if and only if $x = z + (1-A^\dag A)w$ for some $w \in \mcalH$
\item amongst all elements $x$ of $\mcalH$ which minimize $\norm{Ax-b}$, $z$ has smallest norm.
\end{enumerate}
\end{proposition}
\begin{proof}
\proofitem{1}.
Let $P \eqdef AA^\dag$.
Then $P$ is a self-adjoint and $PA = A$.
We have
\begin{equation}
\label{eq:least-squares-1}
\begin{aligned}
A^*(Az -b) & = A^*(AA^\dag Az -b) \\
           & = A^*(AA^\dag b-b)   \\
           & = A^*Pb - A^*b       \\
           & = (PA)^*b - A^*b     \\
           & = (PA-A)^*b          \\
           & = 0\nsp{,}
\end{aligned}
\end{equation}
so for any $x \in \mcalH$, we have
\begin{equation}
\label{eq:least-squares-2}
\begin{aligned}
\norm{Ax-b}^2 & = \ip{Az-b + A(x-z)}{Az-b + A(x-z)}                                           \\
              & = \norm{Az-b}^2 + \norm{A(x-z)}^2 + \ip{Az-b}{A(x-z)} + \ip{A(x-z)}{Az-b}     \\
              & = \norm{Az-b}^2 + \norm{A(x-z)}^2 + \ip{A^*(Az-b)}{x-z} + \ip{x-z}{A^*(Az-b)} \\
              & = \norm{Az-b}^2 + \norm{A(x-z)}^2                                             \\
              & \geq \norm{Az-b}^2\nsp{.}
\end{aligned}
\end{equation}

\proofitem{2}.
Using \cref{eq:least-squares-2} $\norm{Ax-b} = \norm{Az-b}$ if and only if $Ax = Az$.
It suffices to show that all elements of the kernel of $A$ are of the form $(1-A^\dag A)w$.
We have $A(1-A^\dag A)w = (A- AA^\dag A)w = 0$ for every $w \in \mcalH$.
On the other hand, observe that for $y \in \ker(A)$, we have
\[
y = y - A^\dag Ay = (1-A^\dag A)y\nsp{.}
\]

\proofitem{3}.
By \proofitem{1} and \proofitem{2}, minimizers of $\norm{Ax-b}$ are of the form $x = z + (1-A^\dag A)w$ for $w \in \mcalH$.
Let $x$ be a such a minimizer.
Since $z \in A^\dag b = (A^\dag A) A^\dag b$, $z$ is the image of $A^\dag A$.
Recalling that $A^\dag A$ is a projection, we see that $z \perp (1-A^\dag A)w$.
Hence using the Pythagorean theorem we conclude that $\norm{x}^2 = \norm{z}^2 + \norm{(1-A^\dag A)w}^2$.
The result follows from this.
\end{proof}

\begin{exposition}
\label{exp:least-squares-regression}
Let $A \in \mcalB(\mcalH,\mcalK)$ have closed image.
By \cref{prop:least-squares-solution}, $z \eqdef A^\dag b$ is a smallest norm solution to the minimization problem
\[
\argmin_{x \in \mcalH} \norm{b- Ax}
\]
By \cref{prop:moore-penrose-pseudoinverse-properties} we see that $z$ is the unique solution to the above if and only if $A$ is injective.

We call $z$ the \addindex{least squares solution} to the minimization problem above.
\end{exposition}

$n \times p$ design matrix $X$ and scalar responses $y$.
I guess $X$ can be random too..

In linear regression, we assume $y$ is generated by a process
\[
y = X\beta + \epsilon
\]
where $\epsilon$ is random and $\beta$ is fixed.

We call the range projection $P \eqdef XX^\dag$ (see \cref{prop:moore-penrose-pseudoinverse-properties}) the \addindex{hat matrix}.

Let $\hat{\beta}$ be the least squares solution to $\norm{y -X\beta}$ (i.e. $\hat{\beta} \eqdef X^\dag y$).
Put $\hat{y} \eqdef X\hat{\beta}$.
The \addindex{residuals} are defined to be
\[
\hat{\epsilon} \eqdef y - \hat{y} = y - X\hat{\beta} = (1-XX^\dag)y = (1-P)y = (1-P)(X\beta + \epsilon) = (1-P)\epsilon\nsp{.}
\]
Define
\[
\begin{aligned}
s^2 & \eqdef \frac{1}{n-p}\norm{\hat{\epsilon}}^2   \\
    & = \frac{1}{n-p}\norm{y-X\hat{\beta}}^2\nsp{.}
\end{aligned}
\]

Assume $\E(\epsilon \given X) = 0$ (\addindex{strict exogeneity}) and columns of $X$ are linearly independent (so that $X^\dag X = 1$, see \cref{prop:moore-penrose-pseudoinverse-properties}).
%TODO explain what strict means)
\[
\begin{aligned}
\E(\hat{\beta}) & = \E(\E(X^\dag y \given X))                                     \\
                & = \E(\E(X^\dag X\beta\given X) + \E(X^\dag X\epsilon \given X)) \\
                & =  \E(X^\dag X\beta) + \E(X^\dag X\E(\epsilon \given X))        \\
                & = X^\dag X\beta + 0                                             \\
                & = \beta\nsp{.}
\end{aligned}
\]

check hayashi p 27 p 30

% https://github.com/mavam/stat-cookbook

%%
regression is the same as a t test?
% https://lindeloev.github.io/tests-as-linear/

% fixed effects
% variance inflation factor
% fractional model
% beta regression
% Instrumental variables estimation

% Foundations of Linear and Generalized Linear Models
% Alan Agresti

\section{Univariate Linear Regression}

\begin{exposition}
Recall that the least squares solution to $y = X\beta$ is
\[
\hat{\beta} = (X^\transpose X)^{-1}X^\transpose y\nsp{.}
\]

Let $\overline{x} \eqdef \frac{1}{n}\sum x_i$, $\overline{xy} \eqdef \frac{1}{n}\sum x_iy_i$, $\overline{x^2} \eqdef \frac{1}{n}\sum x_i^2$, etc.

Let $X$ be the matrix augmented with $1$, i.e.
\[
X \eqdef \begin{pmatrix} 1 & x_1 \\ \vdots & \vdots \\ 1 & x_n\end{pmatrix}\nsp{.}
\]
Then
\[
\frac{1}{n} X^\transpose y = \begin{pmatrix} \overline{y} \\ \overline{xy} \end{pmatrix}
\]
and
\[
\frac{1}{n} X^\transpose X = \begin{pmatrix}1 & \overline{x} \\ \overline{x} & \overline{x^2}\end{pmatrix}\nsp{.}
\]
Put $s_{xy} \eqdef \overline{xy}-\overline{x}\cdot \overline{y}$, and $s_x^2 \eqdef s_{xx}$.
Using formulas for inverses, we have
\[
\left(\frac{1}{n} X^\transpose X\right)^{-1} = \frac{1}{\overline{x^2} -\overline{x}^2}\begin{pmatrix} \overline{x^2} & -\overline{x}  \\ -\overline{x} & 1\end{pmatrix} = \frac{1}{s_x^2}\begin{pmatrix} \overline{x^2} & -\overline{x}  \\ -\overline{x} & 1\end{pmatrix}\nsp{.}
\]
Thus
\[
\begin{aligned}
(X^\transpose X)^{-1}X^\transpose y & = \frac{1}{s_x^2}\begin{pmatrix} \overline{x^2} & -\overline{x}  \\ -\overline{x} & 1\end{pmatrix}  \begin{pmatrix} \overline{y} \\ \overline{xy} \end{pmatrix}          \\
                                    & =  \frac{1}{s_x^2}\begin{pmatrix}\overline{x^2}\overline{y} - \overline{x} \cdot \overline{xy}\\ -(\overline{x} \cdot \overline{y}) + \overline{xy}\end{pmatrix}         \\
                                    & =  \frac{1}{s_x^2}\begin{pmatrix}(s_x^2 + \overline{x}^2)\overline{y} - \overline{x}(s_{xy} + \overline{x}\cdot \overline{y})\\ s_{xy}\end{pmatrix}                      \\
                                    & = \frac{1}{s_x^2}\begin{pmatrix} s_x^2\overline{y} + \overline{x}^2\overline{y} - \overline{x}s_{xy} - \overline{x}^2\overline{y}\\ s_{xy}\end{pmatrix}                  \\
                                    & = \begin{pmatrix} \overline{y} - \frac{s_{xy}}{s_x^2}\overline{x}\\ \frac{s_{xy}}{s_x^2}\end{pmatrix}\nsp{,}
\end{aligned}
\]
giving an ``explicit'' formula for $\widehat{\beta}$.
\end{exposition}

\section{Weighted Least Squares}

\begin{exposition}

We fix a positive self-adjoint matrix $W$.
Let $W^{1/2}$ be the unique positive self-adjoint matrix which squares to $W$.
We instead try to minimize
\[
\argmin \norm{W^{1/2}y - W^{1/2}Ax}\nsp{.}
\]

This leads to the solution
\[
\hat{\beta} = ((W^{1/2}X)^\transpose (W^{1/2}X))^{-1}(W^{1/2}X)^\transpose W^{1/2}y\nsp{,}
\]
which can be simplified to
\[
\hat{\beta} = (X^\transpose WX)^{-1}XWy\nsp{,}
\]
using that $(W^{1/2})^\transpose = W^{1/2}$.
\end{exposition}
