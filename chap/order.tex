\chapter{Order Theory}

\section{Partially Ordered Sets}

\begin{exposition}
A \addindex{relation on a set} $X$ is a relation such that $xRy \implies (x,y) \in X \times X$.
\end{exposition}

\begin{exposition}
A \addindex{preordered set} is a set with a (fixed) preorder on it.
We write $\lsim$ for the relation.
We write $a \gsim b$ if $a \lsim b$.
We write $a \simeq b$ if $a \lsim b$ and $a \gsim b$.
\end{exposition}

\begin{exposition}
Let $X$ be a preordered set.
Fix $x \in X$.
We define
\[
X_{\gsim x} \eqdef \{x' \in X:x' \geq x\}\nsp{.}
\]
Similarly we define $X_{\lsim x}$.
\end{exposition}

\begin{exposition}
A \addindex{partially ordered set} is a set with a (fixed) partial order.
We write $\leq$ for the relation.
We write $a \geq b$ if $b \leq a$.
\end{exposition}

\begin{example}
Let $X$ be a set.
Then $\powset{X}$ is a partially ordered set with its partial order given by inclusion, i.e. $A \leq B \iff A \subset B$.
\end{example}

\begin{example}
Let $X$ be a partially ordered set.
Then for every $x \in X$, $X_{\geq x}$ is a partially ordered set.
\end{example}

\begin{proposition}
\label{prop:preorder-category}
Let $X$ be a (possibly large) set and let $R$ be a relation on it.
Define $s\colon R \to X$ by
\[
\left<x,y\right> \mapsto x
\]
and $t\colon R \to X$ by
\[
\left<x,y\right> \mapsto y\nsp{.}
\]
The following are equivalent:
\begin{enumerate}
\item $(X,R,s,t)$ is a category
\item $R$ is a preorder.
\end{enumerate}
\end{proposition}
\begin{proof}
Omitted.
\end{proof}

\begin{exposition}
There is a category of preordered sets, with increasing maps as morphisms.
Then \cref{prop:preorder-category} defines a functor from this category into $\CAT$ (in particular, an increasing map becomes a functor).
\end{exposition}

\begin{exposition}
A \addindex{order isomorphism} is a isomorphism in the category of preordered sets.
\end{exposition}

\begin{example}
If $f$ is an order isomorphism, then its set-theoretic inverse is automatically an order isomorphism.
\end{example}

\begin{proposition}
\label{prop:thin-category-characterization}
Let $C$ be a category.
The following are equivalent:
\begin{enumerate}
\item for each $x$ and $y$, $C(x,y)$ has at most $1$ morphism.
\item there is a preordered set $X$ such that $C$ is (up to isomorphism of categories) the category associated to $X$.
\end{enumerate}
\end{proposition}
\begin{proof}
Omitted.
\end{proof}

\begin{exposition}
We say a category $C$ is a \addindex{thin category} or is a \addindex{preorder} if it satisfies any of the conditions in \cref{prop:thin-category-characterization}.

We will conflate the two by identifying a (small) thin category with its associated preordered set.
\end{exposition}

\begin{exposition}
Let $C$ be a preordered set.
Then $x \simeq y$ (i.e. $x \lsim y$ and $y \lsim x$) if and only if $x$ and $y$ are isomorphic in the the associated category.
\end{exposition}

\begin{exposition}
Let $C$ be a preorder.
We get a (well-defined) partial order on isomorphism classes given by
\[
[x] \leq [y]
\]
if $x \lsim y$.
\end{exposition}

\begin{example}
A \addindex{directed set} is a preordered set such that its associated category is a directed category.
\end{example}

\begin{exposition}
Let $X$ be a preordered set.
Let $A$ be a subset of $X$.

An \addindex{upper bound} of $A$ (in $X$) is a element $y \in X$ such that $x \leq y$ for any $x \in A$.
If an upper bound of $A$ exists, then we say $A$ is \addindex{bounded above}.

A \addindex{lower bound} of $A$ (in $X$) is a element $y \in X$ such that $y \leq x$ for any $x \in A$.
If a lower bound of $A$ exists, then we say $A$ is \addindex{bounded below}.

A \addindex{maximal element} of $X$ is an element $x$ such that $x \leq y$ implies $x = y$ for any $y \in X$.

A \addindex{minimal element} of $X$ is an element $x$ such that $y \leq x$ implies $x = y$ for any $y \in X$.

The reader can formulate what upper bounds, lower bounds, etc. of families of elements should be.
\end{exposition}

\begin{exposition}
Let $X$ be a preordered set.
Let $A \subset X$.

A \addindex{greatest lower bound}, or \addindex{infimum}, of $A$ is a lower bound $m$ of $A$ such that if $m'$ is another lower bound of $A$, then $m' \leq m$.

A \addindex{least upper bound}, or \addindex{supremum}, of $A$ is an upper bound $m$ of $A$ such that if $j'$ is another upper bound of $A$, then $j \leq j'$.

The reader can formulate what infima and suprema of families of elements should be.
\end{exposition}

\begin{example}
Let $X$ be a set.
Then in the partially ordered set $\powset{X}$, infima exist and are given by intersections.
Similarly, suprema exist, and are given by unions.
\end{example}

\begin{proposition}
Let $X$ be a preordered set.
Then the limit (resp. colimit) of a functor $F\colon I \to X$ is a equivalently an infimum (resp. supremum) of $\{Fi:i \in I\}$.
\end{proposition}
\begin{proof}
We prove the limit case.

Since there is a morphism $\lim(F) \to Fi$ for each $i$, $\lim(F) \lsim Fi$ for all $i$.

Suppose $x \lsim Fi$ for all $i$.
Any family of morphisms $(x \to Fi)$ defines a cone because $X$ is thin when considered as a category.
Thus we get a morphism $x \to \lim(F)$, i.e. $x \lsim \lim(F)$.
\end{proof}

\begin{exposition}
Let $Y$ be a preordered set.
Then set of mappings $X \to Y$ becomes a preordered  set by defining
\[
f \lsim g \iff (\forall x \in X)[f(x) \lsim g(x)]\nsp{.}
\]
If $Y$ is partially ordered, then the preorder above is also a partial ordering.

In particular, if $(X_i)$ is a family of preordered (resp. partially ordered) sets, then $\prod X_i$ has a preorder (resp. partial order) on it.
\end{exposition}

\begin{exposition}
In a partially ordered set we write $\sup$ and $\inf$ for suprema and infima.
\end{exposition}

\begin{lemma}
Let $X$ be a partially ordered set.
Let $A \subset B \subset X$.
Assume the necessary suprema and infima exist.
The following hold:
\begin{enumerate}
\item if for every $a \in A$, there is a $b \in B$ such that $a \leq b$, then $\sup A \leq \sup B$
\item if for every $a \in A$, there is a $b \in B$ such that $a \geq b$, then $\inf A \geq \inf B$
\item if $A \subset B$, then $\sup A \leq \sup B$
\item if $A \subset B$, then $\inf A \geq \inf B$.
\end{enumerate}
\end{lemma}
\begin{proof}
Omitted.
\end{proof}

\section{Adjunctions of Preordered Sets}

\begin{proposition}
\label{prop:preordered-set-adjunction-characterization}
Let $f\colon X \to Y$ and $g\colon Y \to X$ be maps between preordered sets.
The following are equivalent:
\begin{enumerate}
\item for all $x \in X$ and $y \in Y$,
      \[
      f(x) \lsim y \iff x \lsim g(y)
      \]
\item $f$ and $g$ define an adjunction between (the categories associated to) $X$ and $Y$.
\end{enumerate}
\end{proposition}
\begin{proof}
\itemimply{1}{2}.
If $f$ and $g$ were increasing, then using the hypothesis we can define a natural isomorphism of functors $X(f(\blank),\blank) \isoto Y(\blank,g(\blank))$, so it suffices to show that $f$ and $g$ are increasing.

Let us show that $g$ is increasing.
Suppose $y \lsim y'$.
Let $x \eqdef g(y)$.
Then $x \lsim g(y)$, so $f(x) \lsim y \lsim y'$.
Hence $g(y) = x \lsim g(y')$.

The proof for $f$ is similar.

\itemimply{2}{1}.
Omitted.
\end{proof}

\begin{exposition}
An \addindex{adjunction between preordered sets} $X$ and $Y$ is a pair of mappings $(f,g)$ satisfying any of the conditions in \cref{prop:preordered-set-adjunction-characterization}.
\end{exposition}

\begin{example}
Let $f \colon X \to Y$ be a mapping.
Then for every $A \subset X$ and $B \subset Y$, we have
\[
f(A) \subset B \iff A \subset f^\pre(B)
\]
and hence we have an adjunction between $\powset{X}$ and $\powset{Y}$.
\end{example}

\begin{proposition}
\label{prop:galois-correspondence}
Let $(f,g)$ be an adjunction between partially ordered sets $X$ and $Y$.
Then $fgf = f$, $gfg = g$, and $f$ and $g$ are inverses when restricted to $gf(X)$ and $fg(Y)$.
\end{proposition}
\begin{proof}
We have $x \leq gf(x)$ for all $x \in X$, and $fg(y) \leq y$ for all $y \in Y$.
Since $f$ is increasing, $f(x) \leq f(gf(x)) = fg(f(x)) \leq f(x)$, which implies $f(x) = fgf(x)$.
Similarly, $g(y) = gfg(y)$.

Then $gfgf(x) = gf(x)$ and $fgfg(y) = y$ and the result follows.
\end{proof}

\begin{proposition}
\label{prop:galois-connection-injective-surjective-properties}
Let $(f,g)$ be an adjunction between partially ordered sets $X$ and $Y$.
The following are equivalent:
\begin{enumerate}
\item $g$ is injective
\item $f$ is surjective
\item $f$ and $g$ are inverses when restricted to $g(Y)$ and $Y$.
\end{enumerate}
\end{proposition}
\begin{proof}
We apply \cref{prop:galois-correspondence}.

\itemimply{1}{2}.
Since $gfg = g$ and $g$ is injective, $fg = 1$, and $f$ is surjective.

\itemimply{2}{3}.
Then $gf(X) = g(Y)$ since $f$ is surjective, and $fg(Y) = fgf(X) = f(X) = Y$ (again since $f$ is surjective), and the result follows.

\itemimply{3}{1}.
The condition says that $g$ is a bijection onto its image.
\end{proof}

\begin{exposition}
The \addindex{opposite preordered set} $X^\op$ of a preordered set is the preordered set with the same underlying set but the inverse relation.
To be clear, we sometimes write this relation as $\lsim_\op$, so $a \lsim b \iff b \lsim_\op a$.
\end{exposition}

\begin{proposition}
\label{prop:relation-associated-galois-connection}
Let $X$ and $Y$ be sets.
For every relation $R \subset X \boxtimes Y$, define maps $f_R\colon \powset{X} \to \powset{Y}$ and $g_R\colon \powset{Y} \to \powset{X}$ by
\[
f_R(A) \eqdef \{y \in Y:\text{$xRy$ for every $x \in A$}\} = \bigcap_{x \in A} \{y \in Y:xRy\}
\]
and
\[
g_R(B) \eqdef \{x \in X:\text{$xRy$ for every $y \in B$}\} = \bigcap_{y \in B} \{x \in X:xRy\}
\nsp{.}
\]
Then the map $R \mapsto (f_R,g_R)$ defines a bijection
\[
\powset{X \boxtimes Y} \bijto \{(f,g):\text{$(f,g)$ is an adjunction between $\powset{X}$ and $\powset{Y}^\op$}\}\nsp{.}
\]
\end{proposition}
\begin{proof}
Let $R \subset X \boxtimes Y$.
For every $A \subset X$ and $B \subset Y$, we have
\[
f_R(A) \supset B \iff A \times B \subset R \iff A \subset g_R(B)
\]
so we see that $f_R$ and $g_R$ are adjuncts.

For injectivity, suppose $(x,y) \in X \times Y$, and let $R$ and $R'$ be relations such that $f_R = f_{R'}$.
Then we have
\[
xRy \iff y \in f_R(\{x\}) \iff y \in f_{R'}(\{x\}) \iff xR'y\nsp{.}
\]
Thus $R = R'$.

For surjectivity, let $(f,g)$ be an adjunction between $\powset{X}$ and $\powset{Y}^\op$.
Define a relation $R$ by $xRy$ if $y \in f(\{x\})$ (i.e. $\{y\} \subset f(\{x\})$, or equivalently $\{x\} \subset g(\{y\})$ or $y \in g(\{x\})$).
Let $A \subset X$.
Since $f$ is a left adjoint, it preserves colimits and hence suprema, i.e. so it follows
\[
f(A) = f\left(\bigcup_{x \in A} \{x\}\right) = \bigcap f(\{x\}) = \{y \in Y:\text{$xRy$ for all $x \in A$}\} = f_R(A)\nsp{.}
\]
Thus $f = f_R$.
Similarly, $g = g_R$.
\end{proof}

\section{Zorn's Lemma}

\begin{lemma}[Zorn's lemma]
\label{lem:zorn-lemma}
Let $P$ be a nonempty partially ordered set.
Suppose every totally ordered subset of $P$ has an upper bound.
Then $P$ has a maximal element.
\end{lemma}
\begin{proof}
Suppose not.
Then for every $x \in P$, there exists $y \in P$ such that $y \gt x$.
Define $G(f)$ to be any strict upper bound of $\ran(f)$ if $\ran(f)$ is a totally ordered subset of $P$ (this is possible by the hypothesis and using axiom of choice).
By recursion, there exists a unique function $F \colon \On \to P$ such that $F(0) = x$ and $F(\alpha) = G(F\res_\alpha)$ for all $\alpha$.
By induction, $\ran(F\res_\alpha)$ is totally ordered for each $\alpha$ and $\alpha \lt \beta$ implies $F(\alpha) \lt F(\beta)$.
We get a one-to-one function $\On \to P$.
But $P$ is a set and (the image of) $\On$ is not, a contradiction.
\end{proof}

\section{Closures}

\begin{proposition}
\label{prop:closure-map-characterization}
Let $X$ be a preordered set.
Let $x \mapsto x^\cl$ be a mapping $X \to X$.
The following are equivalent:
\begin{enumerate}
\item for all $x$ and $y$ in $X$:
      \begin{enumerate}
      \item $x \lsim x^\cl$
      \item $x \lsim y \implies x^\cl \lsim y^\cl$
      \item $x^\cl \simeq (x^\cl)^\cl$ for all $x \in X$
      \end{enumerate}
\item $x \lsim y^\cl \iff x^\cl \lsim y^\cl$ for all $x,y \in X$.
\end{enumerate}
\end{proposition}
\begin{proof}
Omitted.
\end{proof}

\begin{exposition}
A mapping $\blank^\cl\colon X \to X$ is said to be a \addindex{closure mapping} if it satisfies any of the conditions of \cref{prop:closure-map-characterization}.
\end{exposition}

\begin{proposition}
\label{prop:adjunction-closure-mapping}
Let $(f,g)$ be an adjunction between preordered sets $X$ and $Y$.
Then $gf$ is a closure mapping.
\end{proposition}
\begin{proof}
Let $h \eqdef gf$.
We have $x \lsim h(x)$ (consider the unit of the adjunction), and $h$ is increasing because $g$ and $f$ are increasing.

It remains to show that $h(x) \simeq h(h(x))$.
We clearly have $h(x) \lsim h(h(x))$.
On the other hand
\[
gf(x) \lsim gf(x) \implies fgf(x) \lsim f(x) \implies gfgf(x) \lsim gf(x)
\]
and the result follows.
\end{proof}

\begin{exposition}
Let $X$ be a set.
Let $\gen{\blank}\colon \powset{X} \to \powset{X}$ be a closure mapping.
In this situation, if $(A_i)_{i \in I}$ is a family of subsets of $X$, we may write $\gen{A_i:i \in I}$, $\gen{(A_i)}$, or $\gen{\{A_i\}}$ for $\gen{\bigcup A_i}$, or, if $I = \{1,\dotsc,n\}$, write $\gen{A_1,\dotsc,A_n}$.
For a family $(x_i)_{i \in I}$, instead of writing $\gen{\{x_i:i \in I\}}$, we may write $\gen{(x_i)}$ or $\gen{x_i:i \in I}$, and if $I = \{1,\dotsc,n\}$, we may write $\gen{x_1,\dotsc,x_n}$.
\end{exposition}

\begin{exposition}
Let $\gen{\blank}\colon \powset{X} \to \powset{X}$ be closure mapping.
Let $S$ and $S'$ be subsets of $X$.
If $\gen{S'} = S$ we say $S'$ \addindex{generates} $S$.
If $\gen{\{x_i\}} = S$ we say the elements $x_i$ generate $S$.
The $x_i$ are called \addindex{generators} of $S$.
We frequently identify sets with sets indexed by themselves, so that we can talk about e.g. $S'$ being a set of generators of $S$ (this means $S'$, considered as as an indexed family $(s)_{s \in S'}$, generates $S$, which agrees with the previous notion of a set generating another).
\end{exposition}

\begin{proposition}
\label{prop:closure-mapping-intersection}
Let $X$ be a set.
Let $\gen{\blank}\colon \powset{X} \to \powset{X}$ be a closure mapping.
Then every family $(A_i)$ of elements of $\powset{X}$,
\[
\gen{\bigcap \gen{A_i}} = \bigcap \gen{A_i}\nsp{.}
\]
\end{proposition}
\begin{proof}
We have $\bigcap \gen{A_i} \subset \gen{\bigcap \gen{A_i}}$.

Applying $\gen{\blank}$, we then also have for each $j$ that
\[
\gen{\bigcap \gen{A_i}} \subset \gen{\gen{\bigcap \gen{A_i}}} = \gen{\bigcap \gen{A_i}} \subset \gen{\gen{A_j}} = \gen{A_j}\nsp{.}
\]
Then we have $\gen{\bigcap \gen{A_i}} \subset \bigcap \gen{A_i}$.
\end{proof}

\begin{proposition}
Let $X$ be a set.
Let $\gen{\blank}\colon \powset{X} \to \powset{X}$ be a closure mapping.
Let $F \subset \powset{X}$ is the set of elements of the form $\gen{A}$ for some $A \subset X$ (equivalently those $A \subset X$ such that $A = \gen{A}$), partially ordered by inclusion.
The following hold:
\begin{enumerate}
\item for every family of elements $(A_i)$ of $F$, $\bigcap A_i$ is the infimum of $(A_i)$ and $\gen{\bigcup A_i}$ is the supremum of $(A_i)$
\item for every $A \subset X$,
      \[
      \gen{A} = \bigcap_{B \in F,B \supset A} B\nsp{.}
      \]
\end{enumerate}
\end{proposition}
\begin{proof}
\proofitem{1}.
It is clear that $\bigcap A_i$ is the infimum of $(A_i)$ in $\powset{X}$.
Hence to prove that $\bigcap A_i$ is the infimum of $(A_i)$ in $L$, we just need to show that $\bigcap A_i \in F$.
This follows from \cref{prop:closure-mapping-intersection}.

It is clear that $A_j \subset \gen{\bigcup A_i}$ for each $j$.
Let $A \in F$ be such that $A_i \subset A$ for all $i$.
Then $\bigcup A_i \subset A$.
Since $A = \gen{A}$, we then have $\gen{(A_i)} = \gen{\bigcup A_i} \subset \gen{A} = A$.

\proofitem{2}.
Omitted ($\gen{A} \supset A$ and $\gen{A} \in F$).
\end{proof}

\section{Independence}

\begin{exposition}
A mapping from preordered set $X$ to itself is \addindex{extensive} if $x \lsim f(x)$ for all $x \in X$.
\end{exposition}

\begin{exposition}
Let $X$ be a set.
Let $\gen{\blank}\colon \powset{X} \to \powset{X}$ be an increasing extensive map.
We say$A \subset X$ is \addindex{independent} if for every $x \in A$, $x \notin \gen{A\setminus \{x\}}$.
Otherwise it is said to be \addindex{dependent}.
\end{exposition}

\begin{exposition}
Let $X$ be a set.
We say an increasing extensive map $\gen{\blank}\colon \powset{X} \to \powset{X}$ is \addindex{finitary} if for every $A$ and $x$ such that $x \in \gen{A}$, there is a finite subset $A' \subset A$ such that $x \in \gen{A'}$.
\end{exposition}

\begin{proposition}
Let $\gen{\blank}\colon \powset{X} \to \powset{X}$ be a finitary increasing extensive map and let $A$ be an independent subset of $X$.
The following hold:
\begin{enumerate}
\item if $A \subset B \subset X$, then the set
      \[
      \{A' \subset X:\text{$A'$ is independent and $A \subset A' \subset B$}\}
      \]
      has a maximal element (with respect to inclusion)
\item $A$ is contained in a independent subset of $X$ maximal with respect to inclusion.
\end{enumerate}
\end{proposition}
\begin{proof}
\proofitem{1}.
The set
\[
P \eqdef \{A' \subset X:\text{$A'$ is independent and $A \subset A' \subset B$}\}
\]
is partially ordered by inclusion.
Let $C$ be a totally ordered subset of $P$.
We claim $\bigcup C$ is independent.
Suppose $x \in C$ and $x \in \gen{C\setminus\{x\}}$.
Then $x \in \gen{C'}$ for some finite $C' \subset C \setminus\{x\}$.
But $C' \cup \{x\}$ is a subset of some element $C''$ of $C$.
Then $x \in \gen{C'} \subset \gen{C'' \setminus \{x\}}$, which contradicts the independence of $C''$.
This implies the claim.
Since $A \subset \bigcup C \subset B$, it follows that we can get the result by applying \cref{lem:zorn-lemma}.

\proofitem{2}.
This follows from \proofitem{1}.
\end{proof}

\begin{proposition}
\label{prop:infinite-number-generators-equality}
Let $X$ be a set and let $\gen{\blank}\colon \powset{X} \to \powset{X}$ be a finitary closure mapping.
Let $S$ be a set minimal (with respect to inclusion) such that $\gen{S} = X$.
Suppose $S$ is infinite.
The following hold:
\begin{enumerate}
\item any other set $S' \subset X$ such that $\gen{S'} = X$ has cardinality greater than or equal to $\card{S}$
\item if $S'$ is another minimal set such that $\gen{S'} = X$, then $\card{S} = \card{S'}$.
\end{enumerate}
\end{proposition}
\begin{proof}
\proofitem{1}.
For each $s' \in S'$ we may find a finite $S_{s'} \subset S$ such that $s' \in \gen{S_{s'}}$.
Then $S' \subset \bigcup_{s'\in S'} \gen{S_{s'}}$.
Since $\gen{S_{s'}} \subset \gen{\bigcup S_{s'}}$ for each $s'$, it follows $X = \gen{S'} \subset \gen{\bigcup S_{s'}}$.
On the other hand $\bigcup S_{s'} \subset S$, so by minimality of $S$, $\bigcup S_{s'} = S$.
The set $S'$ cannot be finite since otherwise $S$ would be a finite union of finite sets.
Then we have $\card{S} \leq \card{S'}\aleph_0 = \card{S'}$.

\proofitem{2}.
This follows from \proofitem{1}.
\end{proof}

\begin{proposition}
Let $X$ be a set.
Let $\gen{\blank}\colon \powset{X} \to \powset{X}$ be a closure mapping.
Let $A$ be a subset of $X$.
The following are equivalent:
\begin{enumerate}
\item $A \subset X$ is independent and $\gen{A} =X$
\item $A$ is minimal with respect to inclusion in the set $\{A \subset X:
      \gen{A} = X\}$.
\end{enumerate}
\end{proposition}
\begin{proof}
\itemimply{1}{2}.
Let $A' \subset A$ be another set with $\gen{A'} = X$.
Suppose $a \in A \setminus A'$.
Because $A$ is independent, $a \notin \gen{A'} = X$ (otherwise $x \in \gen{A'} \subset \gen{A \setminus \{x\}}$), an absurdity.
It follows $A' = A$.

\itemimply{2}{1}.
Suppose $a \in \gen{A \setminus \{a\}}$.
Then $A = (A\setminus \{a\}) \cup \{a\} \subset \gen{A \setminus \{a\}}$, so $X = \gen{A} \subset \gen{\gen{A \setminus \{a\}}} = \gen{A \setminus \{a\}}$.
This contradicts minimality of $A$.
Hence $A$ is independent.
\end{proof}

\section{Lattices}

\begin{exposition}
A \addindex{lattice} is a tuple $(L,\lor,\land)$ such that
\begin{enumerate}
\item $\lor$ is a commutative associative operation $L \times L \to L$
\item $\land$ is a commmutative associative operation $L \times L \to L$
\item $a \lor (a \land b) = a$ for all $a,b \in L$
\item $a \land (a \lor b) = a$ for all $a,b \in L$.
\end{enumerate}
\end{exposition}

\begin{example}[Dual lattice]
Let $(L,\lor,\land)$ be a lattice.
Then $(L,\land,\lor)$ is also a lattice.
\end{example}

\begin{proposition}
Let $L$ be a lattice.
Then every element of $L$ is an idempotent with respect to $\land$ and $\lor$.
\end{proposition}
\begin{proof}
We have
\[
a = a \lor (a \land (a \lor a)) = a \lor a
\]
and dually we get $a = a \land a$.
\end{proof}

\begin{proposition}
\label{prop:partial-ordered-induced-lattice}
Let $X$ be a partially ordered set with all finite upper and lower bounds.
Then defining
\[
a \lor b \eqdef \sup(a,b)
\]
and
\[
a \land b \eqdef \inf(a,b)
\]
makes $X$ a lattice.
\end{proposition}
\begin{proof}
Omitted.
\end{proof}

\begin{proposition}
Let $L$ be a lattice.
For every $a,b \in L$, we have
\[
a  = a \land b \iff b = a \lor b\nsp{.}
\]
\end{proposition}
\begin{proof}
We have
\[
b  = b \lor (a \land b) = b \lor a
\]
and dually.
\end{proof}

\begin{proposition}
\label{prop:lattice-associated-partial-order}
Let $L$ be a lattice.
Define
\[
a \leq b \iff a = a \land b \iff b= a \lor b\nsp{.}
\]
Then $\leq$ is a the unique partial order on $L$ such that:
\begin{enumerate}
\item $\leq$ has all finite upper and lower bounds of nonempty families
\item $\land$ and $\lor$ are given as in \cref{prop:partial-ordered-induced-lattice}.
\end{enumerate}
\end{proposition}
\begin{proof}
Omitted.
\end{proof}

\begin{exposition}
We always consider a lattice as a partially ordered set as in \cref{prop:lattice-associated-partial-order}.
\end{exposition}

\begin{proposition}
Let $L$ be a lattice.
Let $0 \in L$.
The following are equivalent:
\begin{enumerate}
\item $0 \lor a = a$ for all $a \in L$
\item $0 \leq a$ for all $a \in L$.
\end{enumerate}
\end{proposition}
\begin{proof}
Omitted.
\end{proof}

\begin{proposition}
Let $L$ be a lattice.
Let $1 \in L$.
The following are equivalent:
\begin{enumerate}
\item $1 \land a = a$ for all $a \in L$
\item $a \leq 1$ for all $a \in L$.
\end{enumerate}
\end{proposition}
\begin{proof}
Omitted.
\end{proof}

\begin{exposition}
A \addindex{bounded lattice} is a lattice $L$ with all finite upper and lower bounds.
We see that $\land$ and $\lor$ define commutative monoid structures on $L$.
\end{exposition}

\begin{exposition}
A \addindex{complete lattice} is a lattice such that its associated partial order has all upper and lower bounds.
\end{exposition}

\begin{proposition}
\label{prop:distributive-lattice-characterization}
Let $L$ be a lattice.
The following are equivalent:
\begin{enumerate}
\item $a \land (b \lor c) = (a \land b) \lor (a \land c)$ for all $a,b,c$
\item $a \lor (b \land c) = (a \lor b) \land (a \lor c)$ for all $a,b,c$.
\end{enumerate}
\end{proposition}
\begin{proof}
\itemimply{1}{2}.
We have
\[
\begin{aligned}
a \lor (b \land c) & = a \lor (a \land c) \lor (b \land c)            \\
                   & = a \lor ((a \lor b) \land c)                    \\
                   & = ((a \lor b) \land a) \lor ((a \lor b) \land c) \\
                   & = (a \lor b) \land (a \lor c)\nsp{.}
\end{aligned}
\]

\itemimply{2}{1}.
Consider the dual lattice.
\end{proof}

\begin{exposition}
A \addindex{distributive lattice} is a lattice satisfying any of the conditions in \cref{prop:distributive-lattice-characterization}.
\end{exposition}

\begin{exposition}
A \addindex{modular lattice} is a lattice $L$ such that
\[
a \lor (x \land b) = (a \lor x) \land b
\]
for all $a,b,x \in L$ such that $a \leq b$.
\end{exposition}

\begin{example}
Every distributive lattice is modular.
Indeed, if $a \leq b$ we have
\[
a \lor (x \land b) = (a \lor x) \land (a \lor b) = (a \lor x) \land b\nsp{.}
\]
\end{example}

\begin{example}
For every set $X$, $\powset{X}$ is a distributive complete lattice, with its operations given by $\lor = \cup$ and $\land = \cap$.
The corresponding partial order is given by inclusion.
\end{example}

\section{Chain Conditions}

\begin{exposition}
Let $X$ be a partially ordered set.
If $(x_i)$ is an increasing (or decreasing) sequence of elements of $X$ such that there is an index $n$ such that $x_i = x_n$ for all $i \geq n$, we say that $(x_i)$ \addindex{stabilizes}.
\end{exposition}

\begin{exposition}
Let $X$ be a partially ordered set.

We say $X$ satisfies the \addindex{ascending chain condition} if every increasing sequence of elements of $X$ stabilizes.
\end{exposition}

\begin{exposition}
We say a partially ordered set satisfies the \addindex{descending chain condition} if $X^\op$ satisfies the ascending chain condition.
\end{exposition}

\begin{proposition}
\label{prop:noetherian-characterization}
Let $X$ be a partially ordered set.
The following are equivalent:
\begin{enumerate}
\item $X$ satisfies the ascending chain condition
\item every nonempty subset of $X$ has a maximal element.
\end{enumerate}
\end{proposition}
\begin{proof}
\itemimply{1}{2}.
Let $A$ be a subset with no maximal element.
Pick some $x_0 \in A$.
At step $i$, since $x_{i-1}$ is not a maximal element of $A$, there is an $x_i \geq x_{i-1}$ and $x_i \neq x_{i-1}$.
Proceeding in this manner we may find an increasing sequence $(x_i)$ which does not stabilize.

\itemimply{2}{1}.
Let $(x_i)$ be an increasing sequence.
Consider the subset $\{x_i\}$ of $X$.
A maximal element $x_n$ satisfies $x_i = x_n$ for $i \geq n$.
\end{proof}

\begin{exposition}
We say $X$ is \addindex{Noetherian} if it it satisfies any of the conditions in \cref{prop:noetherian-characterization}.
\end{exposition}

\begin{exposition}
We say $X$ is \addindex{Artinian} if $X^\op$ is Noetherian.
\end{exposition}
