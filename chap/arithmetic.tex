\chapter{Arithmetic}

\section{Natural Numbers}

\begin{exposition}
A \addindex{monoid} is an algebra in the monoidal category $\Set$ (given the cartesian monoidal structure).
A \addindex{commutative monoid} is a commutative monoid in the same monoidal category.

We can write monoids multiplicatively or additively (generally reserving additive notation of commutative monoids).

The multiplicative identity is written $1$ and the additive identity as $0$.
\end{exposition}

\begin{exposition}
A \addindex{semiring} is a set with two commutative monoid structures, addition and multiplication, such that multiplication distributes over addition.

We write $0$ for the additive identity and $1$ for the multiplicative identity in a semiring.
\end{exposition}

\begin{exposition}
There is a unique way to make $\omega$ a commmutative monoid such that $0 = \emptyset$ and $(m+n)^+ = m + n^+$, namely ordinal addition.

There a unique way to make $\omega$ a semiring such that $1= \emptyset^+$ and $m(n+p) = mn + mp$ (in fact demanding that $m(n+1)= mn + m$ is enough).

We write $\N$ for $\omega$ considered as this semiring.

Multiplication and addition on $\N$ are \addindex{cancellative} in the following sense:
\begin{enumerate}
\item if $ab = ac$ and $a \neq 0$, then $a = c$
\item if $a + b = a + c$, then $b = c$.
\end{enumerate}
\end{exposition}

\begin{exposition}
Let $A$ be a commutative monoid.
We write $a \divides b$ if there is a $c$ such that $ac = b$ and say $a$ \addindex{divides} $b$.
We say $a$ is a \addindex{divisor} of $b$ and $b$ is a \addindex{multiple} of $a$.
This defines a preorder on $G$.
\end{exposition}

\begin{example}
The divisbility relation on $\N$ is a partial order.
This follows from the fact that multiplication on $\N$ is cancellative.
\end{example}

\begin{exposition}
A least upper bound with respect to this relation is called a \addindex{least common multiple}, and greatest lower bound with respect to this relation is called a \addindex{greatest common divisor}.

We write $\gcd$ for greatest common divisor and $\lcm$ for least common multiple.
\end{exposition}

\begin{proposition}
\label{prop:divisibility-properties}
The following hold:
\begin{enumerate}
\item if $a = b + c$ and $d\divides a$ and $d \divides b$, then $d \divides c$.
\end{enumerate}
\end{proposition}
\begin{proof}
\proofitem{1}.
Write $c = dq + r$ with $0 \leq r \lt d$ by \cref{thm:ordinal-division-theorem}.

Write $b = b'd$ and $a = a'd$.
Then $a'd = a = b + c = (b'+q)d + r$.
By uniqueness of division applied to $a$ and $d$ (\cref{thm:ordinal-division-theorem}), it follows $r = 0$.
Hence $c = dq + r = dq$, so $d \divides c$.
\end{proof}

\begin{exposition}
Let $A$ be a monoid.
We say $x \in A$ is left (resp. right) \addindex{invertible} if there exists $y \in A$ such that $yx = 1$ (resp. $xy = 1$).
\end{exposition}

\begin{lemma}
If $x$ is left and right invertible and $yx = xz = 1$, then $y = z$.
\end{lemma}
\begin{proof}
Omitted.
\end{proof}

\begin{exposition}
Let $A$ be a monoid.
We say $x \in A$ is a \addindex{unit} if it is is (left and right) invertible, i.e. $x \divides 1$.
We write $x^{-1}$ for the unique element of $A$ such that $xx^{-1} = x^{-1}x = 1$.
\end{exposition}

\begin{example}
The only unit of $\N$ is $1$.
\end{example}

\begin{exposition}
Let $A$ be a commutative monoid.
We say $p \in A$ is \addindex{irreducible} if $p = ab$ implies $a$ or $b$ is a unit.
\end{exposition}

\begin{lemma}
Every natural number is a product of irreducibles.
\end{lemma}
\begin{proof}
Suppose $a \in \N$ be the smallest number which is not a product of irreducibles.
Then $a$ is not irreducible, so there exist nonunits $b$ and $c$ such that $a = bc$.
Then $b,c \lt a$, so $b$ and $c$ are products of irreducibles, which then implies we can write $a$ as a product of irreducibles, a contradiction.
\end{proof}

\begin{exposition}
Let $A$ be a commutative monoid.
We say $p \in A$ is a \addindex{prime} if $p \divides ab$ implies $p \divides a$ or $p \divides b$.
\end{exposition}

\begin{lemma}[Euclid's lemma]
\label{lem:euclid-lemma}
Let $p \in \N$ be nonzero and not equal to $1$.
The following are equivalent:
\begin{enumerate}
\item $p$ is irreducible
\item $p$ is prime.
\end{enumerate}
\end{lemma}
\begin{proof}
\itemimply{1}{2}.
By induction on $p$.
Assume $p$ is irreducible and for all irreducible $q \lt p$, $q$ is prime.
Suppose $p \divides ab$.
By \cref{thm:ordinal-division-theorem}, write $b = dp + r$ for some $d$ and $0 \leq r \lt p$.
If $r = 0$, then $p \divides b$ and we are done.
Assume $r \gt 0$.
We have $ab = adp + ar$, so we see that $p \divides ar$.
If $r = 1$, then $p \divides a$ and we are done.
Assume $r \gt 1$.
Write $pc = ar$ for some $c$.
Let $q$ be an irreducible factor of $r$.
Then $q \lt p$, so is prime.
Thus $q \divides p$ or $q \divides c$.
Since $p$ is irreducible, $q \not\divides p$, so $q \divides c$.
Cancelling $q$ from $pc'q = ar'q$, we obtain $pc' = ar'$ for some $1 \leq r' \lt r$.
Repeating the process (on $r'$), we eventually obtain $a = pc''$ for some $c''$, i.e. $p \divides a$ (this process terminates after finitely many steps because $\N$ is well-ordered).

\itemimply{2}{1}.
Suppose $p = ab$.
Without loss of generality, the condition implies $p \divides a$.
Hence $p \leq a$.
On the other hand $a \leq ab = p$, so it follows $b = 1$ and $a = p$.
The result follows.
\end{proof}

\begin{theorem}[Fundamental theorem of arithmetic]
\label{thm:fundamental-theorem-arithmetic}
Every nonzero $n \in \N$ has a unique \addindex{prime factorization}: it can be uniquely written as a product $\prod_{i=1}^k p_i^{e_i}$ where $p_1 \lt \dotsb \lt p_k$ are primes and each $e_i \gt 0$.
\end{theorem}
\begin{proof}
We already know that every $n \in \N$ can be written as a product of irreducibles (i.e. primes).

For uniqueness, assume $n = \prod p_i^{e_i} = \prod q_j^{f_j}$ is the smallest number which can be written with distinct prime factorizations.
Without loss of generality, $p_1$ divides $q_1$.
Since $p_1$ and $q_1$ are prime, it follows $p_1 = q_1$, and cancellation then gives a smaller number which can be written with distinct prime factorizations, a contradiction.
\end{proof}

\begin{proposition}
\label{prop:natural-number-gcd-lcm-formula}
The following hold:
\begin{enumerate}
\item if all $p_i$ are primes, then $\gcd(p_1^{m_1}\dotsm p_k^{m_k},p_1^{n_1}\dotsm p_k^{n_k}) = p_1^{\min(m_1,n_1)}\dotsm p_k^{\min(m_k,n_k)}$
\item if all $p_i$ are primes, then $\lcm(p_1^{m_1}\dotsm p_k^{m_k},p_1^{n_1}\dotsm p_k^{n_k}) = p_1^{\max(m_1,n_1)}\dotsm p_k^{\max(m_k,n_k)}$
\item $ab = \gcd(a,b)\lcm(a,b)$.
\end{enumerate}
\end{proposition}
\begin{proof}
Omitted.
\end{proof}

\begin{exposition}
We say natural numbers $a$ and $b$ are \addindex{coprime} if $\gcd(a,b) = 1$.
\end{exposition}

%TODO remainders mod p
