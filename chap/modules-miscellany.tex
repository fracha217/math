\chapter{Topics in Modules}

\section{Base Change}

\begin{exposition}
Let $A \to B$ be $\K$-algebra homomorphism.
The restriction of scalars functor $\Mod_B \to \Mod_A$ is given by considering every $B$-module $N$ as an $A$-module via $A \to B \to \End_\K(N)$, and considering every $B$-module homomorphism as an $A$-module homomorphism.
\end{exposition}

\begin{proposition}
Let $A \to B$ be $\K$-algebra homomorphism.
There is a adjunction between the functor $\blank \otimes_A B$ and the restriction of scalars functor $\Mod_B \to \Mod_A$ with unit $(\eta_M\colon M \to M \otimes_A B)$ given by
\[
\eta_M(x) \eqdef x \otimes 1
\]
counit $(\epsilon_N \colon N \otimes_A B \to N)$ given by
\[
\eta_N(y \otimes b) \eqdef yb\nsp{.}
\]
\end{proposition}
\begin{proof}
Omitted.
\end{proof}

\begin{proposition}
For every ideal $\mfraka \subset A$, $M \otimes_A A/\mfraka \simeq M/M\mfraka$ naturally in $M$ via $m \otimes [a] \mapsto [ma]$.
\end{proposition}
\begin{proof}
We can show that $M/M\mfraka$ is a tensor product of $M$ and $A/\mfraka$.
Alternatively, we can construct the inverse $M/M\mfraka$ explicitly.
The $A$-module homomorphism $M \to M \otimes_A A/\mfraka$ vanishes on $M\mfraka$, so we get $M/M\mfraka \to M \otimes_A A/\mfraka$ given by $[m] \mapsto m \otimes 1$.
\end{proof}

\begin{proposition}
Let $M$ be an $A$-module.
Let $N$ be an $(A,B)$-bimodule.
The following hold:
\begin{enumerate}
\item if $M$ and $N$ are finite right modules, then $M \otimes_A N$ is a finite $B$-module
\item if $M$ and $N$ are finitely presented right modules, then $M \otimes_A N$ is a finitely presented $B$-module
\item if $M$ and $N$ are Mittag-Leffler right modules, then $M \otimes_A N$ is a Mittag-Leffler $B$-module.
\end{enumerate}
\end{proposition}
\begin{proof}
\proofitem{1}.
Choose an exact sequence $0 \to K \to A^{\oplus n} \to M \to 0$.
Tensoring with $N$ we get an exact sequence $K \otimes_A N \to N^{\oplus n} \to M \otimes_A N \to 0$.
If $N$ is finite, then $N^{\oplus n}$ is finite and the result follows.

Alternatively, consider the commutative diagram
\[
\xymatrix{
(M \otimes_A N) \otimes_B (\prod P_i) \ar[rr] \ar[d]^{\sim} & & \prod ((M \otimes_A N) \otimes_B P_i) \ar[d]^{\sim} \\
M \otimes_A (N \otimes_B (\prod P_i) \ar[r] & M \otimes_A (\prod N \otimes_B P_i)) \ar[r] & \prod (M \otimes_A (N \otimes_B P_i))\nsp{.}
}
\]
The result follows from this (using here that $\blank \otimes_A M$ preserves surjectivity).

\proofitem{2}.
Similar to the proof of \proofitem{1}.

\proofitem{3}.
Write $M = \colim_{i \in I} M_i$ and $N = \colim_{j \in J} N_j$ as colimits of Mittag-Leffler systems.
Each $M_i \otimes_A N_j$ is finitely presented by \proofitem{2}.
Then $M \otimes_A N = \colim M_i \otimes_A N_j$, and the $M_i \otimes_A N_j$ form a Mittag-Leffler system.
\end{proof}

\begin{proposition}
\label{prop:pure-module-base-change}
If $M \to N$ is a pure monomorphism of $A$-modules, for every $(A,B)$-bimodule $P$, $M \otimes_A P \to N\otimes_A P$ is a pure monomorphism of $B$-modules.
\end{proposition}
\begin{proof}
Follows from $(X \otimes_A P) \otimes_B \blank \simeq X \otimes_A (P \otimes_B \blank)$.
\end{proof}

\begin{proposition}
\label{prop:base-change-flat-module}
Let $M$ be an $A$-module.
Let $N$ be an $(A,B)$-bimodule.
The following hold:
\begin{enumerate}
\item if $M$ and $N$ are flat right modules, then $M \otimes_A N$ is a flat $B$-module
\item if $M$ and $N$ are faithfully right modules, then $M \otimes_A N$ is a faithfully flat $B$-module.
\end{enumerate}
\end{proposition}
\begin{proof}
Follows from $(M \otimes_A N) \otimes_B \blank \simeq (M \otimes_A \blank) \circ (N \otimes_B \blank)$.
\end{proof}

\begin{proposition}
\label{prop:base-change-free-module}
Let $M$ be a free $A$-module with basis $(m_i)$
Let $N$ be a $(A,B)$-bimodule which is a free $B$-module with basis $(n_j)$.
Then $M \otimes_A N$ is free with basis $(m_i \otimes n_j)$.
\end{proposition}
\begin{proof}
Follows from the fact that $A^{\oplus I} \otimes_A B^{\oplus J} \simeq (A \otimes_A B)^{\oplus(I \times J)} \simeq B^{\oplus(I \times J)}$.
\end{proof}

\begin{proposition}
\label{prop:base-change-projective-module}
Let $M$ be an $A$-module.
Let $N$ be an $(A,B)$-bimodule.
The following hold:
\begin{enumerate}
\item if $M$ and $N$ are pure-projective right modules, then $M \otimes_A N$ is a pure-projective $B$-module
\item if $M$ and $N$ are projective right modules, then $M \otimes_A N$ is a projective $B$-module.
\end{enumerate}
\end{proposition}
\begin{proof}
Omitted (tensor products preserve direct sums, free modules, and finitely presented modules).
\end{proof}

\begin{proposition}
\label{prop:tensor-product-dual-basis}
Let $M$ be a projective $A$-module with dual basis $((m_i),(f_i))$.
Let $N$ be a $(A,B)$-bimodule which is a free $B$-module with dual basis $((n_j),(g_j))$.
Assume each $g_j$ is also an $A$-module homomorphism.
Then $M \otimes_A N$ has dual basis given by $(m_i \otimes n_j)$ and the homomorphisms
\[
\xymatrix{
M \otimes_A N \ar[r]^-{f_i \otimes_A g_j} & A \otimes_A B \ar[r]^-{\lambda} & B\nsp{.}
}
\]
\end{proposition}
\begin{proof}
We have
\[
\begin{aligned}
\sum (m_i \otimes n_j)(\lambda \circ (f_i \otimes_A g_j))(m \otimes n) & = \sum (m_i \otimes n_j)f_i(m)g_j(n)                     \\
                                                                       & = \sum m_i \otimes n_jg_j(f_i(m)n)                       \\
                                                                       & = \sum_i m_i \otimes \left(\sum_j n_jg_j(f_i(m)n)\right) \\
                                                                       & = \sum_i m_i \otimes f_i(m)n                             \\
                                                                       & = \sum_i m_if_i(n) \otimes n                             \\
                                                                       & = m \otimes n\nsp{.}
\end{aligned}
\]
\end{proof}

\section{Dévissage}

\begin{exposition}
A \addindex{filtration of a module} $M$ over $A$ is an indexed family $(M_i)_{i \in I}$ of submodules of $M$ indexed by a totally ordered index set $I$ which is increasing, i.e. for every $i$ and $j$ in $I$ such that $i \leq j$, we have $M_i \subset M_j$.
\end{exposition}

\begin{exposition}
A \addindex{transfinite dévissage} of a module $M$ is a filtration $(M_\alpha)_{\alpha \in \beta}$ indexed by an ordinal number $S$ such that
\begin{enumerate}
\item $M_0 = 0$
\item $M = \bigcup M_\alpha$
\item if $\alpha \in S$ is a limit ordinal, then $M_\alpha = \bigcup_{\gamma \lt \alpha} M_\gamma$.
\end{enumerate}
\end{exposition}

\begin{exposition}
A \addindex{direct sum dévissage} of a module $M$ is a transfinite dévissage $(M_\alpha)_{\alpha \in S}$ such that if $\alpha^+ \in S$, then $M_\alpha$ is an internal direct summand of $M_{\alpha^+}$.
\end{exposition}

\begin{lemma}
\label{lem:direct-sum-devissage}
Let $(M_\alpha)_{\alpha \in S}$ be a direct sum dévissage of $M$.
Then $M \simeq \bigoplus_{\alpha^+ \in S} M_{\alpha^+}/M_\alpha$.
\end{lemma}
\begin{proof}
Since $M_\alpha$ is an internal direct summand of $M_{\alpha^+}$, we get an injective homomorphisms $M_{\alpha^+}/M_\alpha \to M_{\alpha^+} \subobj M$.
We claim that the induced map $f\colon\bigoplus_{\alpha^+ \in S} M_{\alpha^+}/M_\alpha \to M$ is an isomorphism.
Let $f_\beta$ be the restriction to $\bigoplus_{\alpha^+ \leq \beta} M_{\alpha^+}/M_\alpha$.
Then by induction one shows the image of $f_\beta$ is $M_\beta$ for all $\beta$.
It follows that $f$ is surjective.
By induction each $f_\beta$ is injective.
For example, suppose $f_{\beta^+}(x) = 0$.
Write $x = x_{\beta} + x_{\beta^+}$, where $x_\beta \in \bigoplus_{\alpha^+ \leq \beta} M_\alpha$ and $x_{\beta^+} \in M_{\beta^+}/M_\beta$.
Then $f_{\beta^+}(x_\beta) = f_\beta(x_\beta) = 0$.
By the induction hypothesis, $x_\beta = 0$, and hence $x = x_{\beta^+}$, so $f_{\beta^+}(x_{\beta^+}) = 0$.
The restriction of $f_{\beta^+}$ to $M_{\beta^+}/M_\beta$ is injective by definition of $f$, so we see that $x = 0$.
From this it follows $f$ is bijective.
\end{proof}

\section{Differentials}

\begin{exposition}
Let $D_\K(A)$ be the quotient of the free $A$-bimodule on the set $A$ by the submodule generated by elements of the form $e_{x+y} - e_x - e_y$ and $e_{xy} - xe_{y} - e_x y$.

We have a $\K$-linear derivation $d\colon A \to D_\K(A)$ given by $x \mapsto [e_x]$.
\end{exposition}

\begin{proposition}
The association
\[
\phi \mapsto \phi \circ d
\]
defines a natural isomorphism of functors $\Mod_{A,A}(D_\K(A),M) \isoto \Der_\K(A,M)$.
\end{proposition}
\begin{proof}
Omitted.
\end{proof}

\begin{lemma}
\label{lem:multiplication-map-kernel}
Let $\mfraka$ be the kernel of multiplication $A \otimes_\K A \to A$.
Then
\[
x \mapsto x \otimes 1 - 1 \otimes x
\]
is a $\K$-linear derivation into $\mfraka$ and $\mfraka$ is generated as a (right) $A$-module by the image of this derivation.
\end{lemma}
\begin{proof}
Let $d$ be the map in question.
Then it is straightforward to verify that $d$ is $\K$-linear and has image contained in $\mfraka$.
The fact that $d$ is a derivation follows from the fact that
\[
xy \otimes 1 - 1 \otimes xy = (x \otimes 1 - 1 \otimes x)y + x(y \otimes 1 - 1 \otimes y)\nsp{.}
\]
Let $\sum x_i \otimes y_i \in \mfraka$.
Then $\sum x_iy_i = 0$ (by definition of $\mfrakb$), so
\[
\sum x_i \otimes y_i = \sum x_i \otimes y_i - \sum 1 \otimes x_iy_i = \sum (x_i \otimes 1 - 1 \otimes x_i)y_i
\]
so $\mfraka$ is generated by the image of $d$.
\end{proof}

\begin{lemma}
\label{lem:universal-derivation-ideal}
Let $\mfraka$ be the kernel of multiplication $A \otimes_\K A \to A$.
Then the unique $A$-bimodule homomorphism $\phi \colon D_\K(A) \to \mfraka$ such that
\[
\phi(dx) = x \otimes 1 - 1 \otimes x
\]
for all $x \in A$ is an isomorphism.
\end{lemma}
\begin{proof}
We can construct the inverse.
The map $(x,y) \mapsto -xdy$ is $(A,\K,A)$-balanced.
We get an $A$-bimodule homomorphism $A \otimes_\K A \to D_\K(A)$ such that $x \otimes y \mapsto -xdy$.
Then $x \otimes 1 - 1 \otimes x \mapsto -xd(1) + 1d(x) = d(x)$.
The elements $dx$ generate $D_\K(A)$ and the elements $x \otimes 1 - 1 \otimes x$ generate $\mfrakb$, so the result follows.
\end{proof}

\section{Square-Zero Extensions}

\begin{exposition}
Let $A$ be a $\K$-algebra.
Let $M$ be a $\K$-module.
An \addindex{algebra extension} of $A$ by $M$ is a short exact sequence of $\K$-modules
\[
0 \to M \to B \to A \to 0
\]
such that $B$ is a $\K$-algebra and $B \to A$ is a $\K$-algebra homomorphism.
\end{exposition}

\begin{exposition}
Let $0 \to M \to B \to A \to 0$ be an algebra extension.
We can identify $A$ with a quotient $B/\mfrakb$ of $B$.
In particular, the image of $M$ in $B$ is an ideal of $B$.
\end{exposition}

\begin{exposition}
Let A \addindex{morphism of algebra extensions} between extensions $0 \to M \to B_1 \to A \to 0$ and $0 \to M \to B_2 \to A \to 0$ of $A$ by $M$ is a $\K$-algebra homomorphism $B_1 \to B_2$ such that the diagram
\[
\xymatrix{
M \ar[r] \ar@{=}[d] & B_1 \ar[d]\ar[r]& A \ar@{=}[d] \\
M \ar[r] & B_2 \ar[r] & A
}
\]
commutes.
By \cref{lem:short-five-lemma}, every morphism of extensions is an isomorphism.
\end{exposition}

\begin{exposition}
A \addindex{square-zero extension} is an algebra extension $0 \to M \to B \to A \to 0$ such that $\im(M \to B)^2 = 0$.
\end{exposition}

\begin{exposition}
Let $0 \to M \to B \to A \to 0$ be a square-zero extension.
We can identify $M$ with its image, an ideal $\mfrakb$ of $B$.
Because $\mfrakb^2 = 0$, $\mfrakb$ is a $B/\mfrakb$-module.
Thus we can make $M$ an $A$-module.
This can be described as follows: if $x \in B$ has image $[x] \in A$, then $[x]m = xm$ and $m[x] = mx$.
\end{exposition}

\begin{exposition}
Let $\phi_1$ and $\phi_2$ be morphisms of square-zero extensions $B \to B'$.
We consider the diagram
\[
\xymatrix{
0 \ar[r] & M \ar[r]^i \ar@{=}[d] & B \ar[d]\ar[r]^q& A \ar@{=}[d] \ar[r] & 0\\
0 \ar[r] & M \ar[r]^{i'} & B' \ar[r]^{q'} & A \ar[r] & 0
}
\]
for reference.
Since $\phi_1 - \phi_2 = 0$ on $\im(M \to B)$, there is an induced algebra homomorphism $\phi\colon A \to B'$ such that $\phi q = \phi_1-\phi_2$.
Since $q'\phi q = \phi_1q- \phi_2q = 1-1 = 0$ and $q$ is surjective, $q'\phi = 0$.
Then exactness of the bottom row gives a map $d\colon A \to M$ such that $i'd = \phi$.

Then $d$ is a derivation $A \to M$.
Indeed, this follows from the fact that
\[
\phi_1(xy)- \phi_2(xy) =  \phi_1(x)\phi_1(y)- \phi_2(x)\phi_2(y) = (\phi_1(x)-\phi_2(x))\phi_2(y) + \phi_1(x)(\phi_1(y) - \phi_2(y))
\]
(because $\phi_i(q(x)) = q'(x)$, they have the same action on elements of $M$ in the bottom row).
\end{exposition}

\begin{exposition}
Suppose we have a square-zero extension $0 \to M \to B \to A \to 0$.
Let $\sigma_1$ and $\sigma_2$ be sections $A \to B$.
Let $q$ be the map $B \to A$, and let $i$ be the map $M \to B$.
We have $q(\sigma_1 - \sigma_2) = 1 -1 = 0$, so $\sigma_1 - \sigma_2 = id$ for some $d \colon A \to M$.

Then $d$ is a derivation.
This follows from the fact that
\[
\sigma_1(xy)- \sigma_2(xy) =  \sigma_1(x)\phi_1(y)- \sigma_2(x)\sigma_2(y) = (\sigma_1(x)-\sigma_2(x))\sigma_2(y) + \sigma_1(x)(\sigma_1(y) - \sigma_2(y))
\]
and the fact that $\sigma_1(z)$ and $\sigma_2(z)$ are lifts of $z \in A$.
\end{exposition}

\begin{exposition}
Let $0 \to M \to B \to A \to 0$ be a split square-zero extension.

Let $\sigma\colon A \to B$ a section.
Then $\sigma(x)$ is a lift of $B \to A$.
Thus $\sigma(x)m = xm$ and $m\sigma(x) = mx$.
\end{exposition}

\begin{exposition}
Let $M$ be an $A$-bimodule.
We give $A \oplus M$ a multiplication defined by
\[
(a,m)(a',m') \eqdef (aa',am' + ma')\nsp{.}
\]
Then $A \oplus M$ is a $\K$-algebra, and $A \oplus M \to A$ is a $\K$-algebra homomorphism.
We call $0 \to M \to A \oplus M \to 0$ the \addindex{trivial square-zero extension}.

Observe that the $A$-bimodule structure on $M$ coincides with the one coming from being part of a square-zero extension.
\end{exposition}

\begin{proposition}
Let $0 \to M \to B \to A \to 0$ be a split square-zero extension.
Any section $\eta\colon A \to B$ induces an isomorphism $B \simeq A \oplus M$ of $\K$-algebras, where the multiplication on $A \oplus M$ is defined by
\[
(a,m)(a',m') \eqdef (aa',am' + ma')\nsp{.}
\]
\end{proposition}
\begin{proof}
Omitted.
\end{proof}

\begin{lemma}
Let $A$, $A'$, $B$, and $B'$ be $\K$-algebras.
Suppose $A' = A \oplus M$, where $M \subset A'$ is an $A$-subbimodule of $A'$ (where $A'$ is an $A$-bimodule via $A \to A \oplus M$).
Similarly assume $B' = B \oplus N$, where $N \subset B'$ is a $B$-subbimodule of $B'$.
Suppose $\phi\colon A \to B$ is a $\K$-algebra homomorphism, and $f\colon M \to N$ is an $A$-bimodule homomorphism (where $N$ is considered as an $A$-bimodule via $A \to B$).
Then $\phi \oplus f \colon A' \to B'$ is a $\K$-algebra homomorphism.
\end{lemma}
\begin{proof}
Omitted.
\end{proof}

\begin{lemma}
\label{lem:beck-module-structure}
Let $A$ be a $\K$-algebra.
Let $\epsilon\colon B \to A$ be an object of $(\Alg_\K)_{/A}$.
Suppose $\eta\colon A \to B$ is such that $\epsilon\eta = 1_A$ and $\ker(\epsilon)^2 = 0$.
Then there is a unique algebra homomorphism $\nabla \colon B \times_A B \to B$ and a unique algebra homomorphism $s\colon B \to B$ such that $(\eta,\nabla,s)$ makes $B \to A$ a commutative group object in $(\Alg_\K)_{/A}$.
The morphism $\nabla$ is given by $\nabla(x,y) = x + y - \eta\epsilon(x)$, and the morphism $s$ is given by $s(x) = -x + 2\eta\epsilon(x)$.
\end{lemma}
\begin{proof}
We need to show that $\nabla \colon B \times_A B \to B$ given by $(x,y) \mapsto x + y - \eta(\epsilon(x)) = x + y -\eta(\epsilon(y))$ makes $B \times_A B$ a commutative group object.
It is straightforward to check that $\nabla$ is a abelian group homomorphism and that $\nabla \circ (\eta \times_A 1) = \lambda$ and $\nabla \circ (1 \times_A \eta) = \rho$.
Let $u,v,x,y \in B$, and suppose $\epsilon(u) = a = \epsilon(v)$ and $\epsilon(x) =b = \epsilon(y)$.
Observe that $u-\eta(a),v-\eta(a),x-\eta(b),y-\eta(b) \in \ker(\epsilon)$.
We have
\[
\begin{aligned}
\nabla(u,v)\nabla(x,y) & = (u+v-\eta(a))(x+y-\eta(b))                                                                                                   \\
                       & = ((u-\eta(a))+v)(x+(y-\eta(b)))                                                                                               \\
                       & = (u-\eta(a))x + vx + v(y-\eta(b))                                           &  & \text{because $\ker(\epsilon)^2 = 0$}        \\
                       & = (ux + vy) -\eta(a)x + v(x -\eta(b))                                                                                          \\
                       & = (ux + vy) -\eta(a)x +\eta(a)(x-\eta(b))+ v(x -\eta(b)) -\eta(a)(x-\eta(b))                                                   \\
                       & = (ux+ vy) -\eta(a)\eta(b) + (v-\eta(a))(x-\eta(b))                                                                            \\
                       & = ux+vy - \eta(ab)                                                           &  & \text{because $\ker(\epsilon)^2 = 0$}\nsp{.}
\end{aligned}
\]
Since $\nabla(1,1) = 1 + 1 - 1 = 1$, $\nabla$ is a $\K$-algebra homomorphism.
It is straightforward to check that $\nabla$ and $\eta$ make $B \to A$ a commutative monoid object.

Define $s(x) \eqdef -x+2\eta\epsilon(x)$.
Then $s(1) = -1 + 2 = 1$, $s$ is an abelian group homomorphism, and
\[
\begin{aligned}
s(x)s(y) & = (-x+2\eta\epsilon(x))(-y+2\eta\epsilon(y))                                                                                                                                                 \\
         & = ((\eta\epsilon(x)-x) + \eta\epsilon(x))((\eta\epsilon(y)-y) + \eta\epsilon(y))                                                                                                             \\
         & = (\eta\epsilon(x)-x)\eta\epsilon(y) + \eta\epsilon(x)(\eta\epsilon(y)-y) +\eta\epsilon(xy)                                                &  & \text{because $\ker(\epsilon)^2 = 0$}        \\
         & = (\eta\epsilon(x)-x)\eta\epsilon(y) - (\eta\epsilon(x)-x)y  +  (\eta\epsilon(x)-x)y+ \eta\epsilon(x)(\eta\epsilon(y)-y) +\eta\epsilon(xy)                                                   \\
         & = (\eta\epsilon(x)-x)(\eta\epsilon(y)-y) +  \eta\epsilon(x)y-xy+ \eta\epsilon(xy)-\eta\epsilon(x)y +\eta\epsilon(xy)                                                                         \\
         & = -xy +2\eta\epsilon(xy)                                                                                                                   &  & \text{because $\ker(\epsilon)^2 = 0$}\nsp{,}
\end{aligned}
\]
so $s$ is a $\K$-algebra homomorphism (this also follows from the fact that $s$ is given by $-1 \oplus 1\colon \ker(\epsilon) \oplus A \to \ker(\epsilon) \oplus A$).
Furthermore, $\epsilon(s(x)) = -\epsilon(x) + 2\epsilon\eta\epsilon(x) = \epsilon(x)$, so $s$ is a morphism in $(\Alg_\K)_{/A}$.
Since $\nabla(s(x),x) = -x + 2\eta\epsilon(x) + x - \eta\epsilon(x) = \eta\epsilon(x)$, and similarly, $\nabla(x,s(x)) = \eta\epsilon(x)$, $s$ is indeed an antipode.

For uniqueness, we know the antipode is unique for a given pair $(\eta,\nabla)$, so it suffices to show that $\nabla$ is unique.
Suppose $(\eta,\nabla)$ makes $B \to A$ a commutative group object.
Let $(x,y) \in B \times_A B$.
This means that $\epsilon(x) = \epsilon(y)$.
Let $a$ be this common value.
Note $\epsilon(x-\eta(a)) = 0 = \epsilon(y - \eta(a))$, so $(x-\eta(a),y-\eta(a)) \in B \times_A B$.
Then
\[
\begin{aligned}
\nabla(x,y) & = \nabla(\eta(a)+x-\eta(a),\eta(a)-y-\eta(a))                                                                                                                                 \\
            & = \nabla(\eta(a),\eta(a)) + \nabla(x-\eta(a),y-\eta(a))            &  & \text{because $\nabla$ is an abelian group homomorphism}                                              \\
            & = \eta(a) + \nabla(x-\eta(a),y-\eta(a))                            &  & \text{because $\nabla \circ (\eta \times_A 1) = \lambda$ and $\nabla \circ (1 \times_A \eta) = \rho$} \\
            & =  \eta(a) + \nabla((x-\eta(a)) + 0,0 + (y-\eta(a)))                                                                                                                          \\
            & =  \eta(a) + \nabla(x-\eta(a),0) + \nabla(0,y-\eta(a))             &  & \text{because $\nabla$ is an abelian group homomorphism}                                              \\
            & =  \eta(a) + \nabla(x-\eta(a),\eta(0)) + \nabla(\eta(0),y-\eta(a))                                                                                                            \\
            & =  \eta(a) + x-\eta(a) + y-\eta(a)                                 &  & \text{because $\nabla \circ (\eta \times_A 1) = \lambda$ and $\nabla \circ (1 \times_A \eta) = \rho$} \\
            & = x+ y - \eta(a)\nsp{.}
\end{aligned}
\]
\end{proof}

\begin{proposition}
Let $A$ be a $\K$-algebra.
Let $\epsilon\colon B \to A$ be an object of $(\Alg_\K)_{/A}$.
The following hold:
\begin{enumerate}
\item if $\eta\colon A \to B$ is such that $\epsilon\eta = 1_A$, then $\ker(\epsilon)^2 = 0$ if and only if there is an algebra homomorphism $\nabla \colon B \times_A B \to B$ and an algebra homomorphism $s \colon B \to B$ such that $(\eta,\nabla,s)$ makes $B \to A$ a commutative group object in $(\Alg_\K)_{/A}$
\item if $\ker(\epsilon)^2 = 0$, then there is a bijection between commutative group object structures $B \to A$ in $(\Alg_\K)_{/A}$ and $\K$-algebra homomorphisms $\eta \colon A \to B$ such that $\epsilon\eta = 1$ given by $(\eta,\nabla,s) \mapsto \eta$.
\end{enumerate}
\end{proposition}
\begin{proof}
\proofitem{1}.
Suppose $\ker(\epsilon)^2 = 0$.
Then $B \to A$ is a commutative group object by \cref{lem:beck-module-structure}.

Suppose $\nabla$ exists.
Let $(x,y) \in B \times_A B$.
Put $a \eqdef \epsilon(x) = \epsilon(y)$.
We have
\[
\nabla(\eta(a)x,y\eta(b)) = \nabla(\eta(a),y)\nabla(x\eta(b)) = yx
\]
because $\nabla$ is a $\K$-algebra homomorphism and $\nabla \circ (\eta \times_A 1) = \lambda$ and $\nabla \circ (1 \times_A \eta) = \rho$.
Hence if $a = 0$, i.e. $x,y \in \ker(\epsilon)$, we have $yx = 0$.
Thus $\ker(\epsilon)^2 = 0$.

\proofitem{2}.
Follow from \cref{lem:beck-module-structure}.
\end{proof}

\begin{exposition}
Let $A$ be a $\K$-algebra and let $M$ be an $A$-bimodule.
By \cref{lem:beck-module-structure} there is a unique way to make $A \oplus M \to A$ a commutative group object in $(\Alg_\K)_{/A}$ such that the structure morphism $\eta$ is the coprojection $A \to A \oplus M$.
The morphism $\nabla$ is given by
\[
\nabla((a,m),(a',m')) = m + m - a
\]
(we must have have $a = a'$ for $((a,m),(a',m))$ to be an element of the fiber product) and the morphism $s$ is given by
\[
s((a,m)) = (a,-m)\nsp{.}
\]
In particular, when restricted to $M \subset A \oplus M$, we just get the abelian group structure on $M$.
\end{exposition}

\begin{proposition}
\label{prop:module-tangent-category-fiber}
The functor $M \mapsto (A \oplus M \to A)$ is an equivalence of categories $\Mod_{A,A} \to \Ab((\Alg_\K)_{/A})$ with quasiinverse $(B \to A) \mapsto \ker(B \to A)$.
\end{proposition}
\begin{proof}
We have $\ker(A \oplus M \to A) \simeq M$.
On the other hand, given a $(B \to A) \in \Ab(\Alg_{/A})$, the structure morphism $\eta \colon (A \to A) \to (B\to A)$ is a section $A \to B$, and we get an induced isomorphism of $\K$-algebras $A \oplus \ker(B\to A) \isoto B$.
Using \cref{lem:beck-module-structure}, this is verified to be an isomorphism of commutative group objects.
It is straightforward that these define natural isomorphisms of functors.
\end{proof}

\begin{exposition}
We define the category of modules $\Mod = \Mod_{\Alg_\K}$ as follows.
Objects are pairs $(A,M)$, where $A$ is a $\K$-algebra and $M$ is an $A$-bimodule.
Morphisms $(A,M) \to (B,N)$ are pairs of morphisms $A \to B$ and $M \to N$ such that $A \to B$ is a $\K$-algebra homomorphism, and $M \to N$ is an $A$-module homomorphism.
\end{exposition}

\begin{exposition}
There is a functor $\Mod \to \Alg_\K$ given by forgetting the module.
The fiber category over $A \in \Alg_\K$ is (equivalent) to $\Mod_{A,A}$.
\end{exposition}

\begin{exposition}
Define a category $T = T_{\Alg_\K}$ as follows.
Objects are pairs $(A',A)$, where $A \in \Alg_\K$ and $A' \in \Ab((\Alg_\K)_{/A})$.
Morphisms $(A',A) \to (B',B)$ are pairs $(\psi \in \Alg_\K(A',B'),\phi \in \Alg_\K(A,B)$ such that the diagram
\[
\xymatrix{
A' \ar[r] \ar[d] & A \ar[d] \\
B' \ar[r] & B
}
\]
commutes and such that $(\psi,\phi)\colon A' \to B' \times_B A$ is morphism in $\Ab((\Alg_\K)_{/A})$ (the functor $\blank \times_B A$ is symmetric monoidal essentially because it preserves products).
\end{exposition}

\begin{lemma}
\label{lem:square-zero-base-change}
Let $N$ be a $B$-module.
Fix $\phi\colon A \to B$ and write $N_A$ for $N$ considered as an $A$-bimodule.
The square
\[
\xymatrix{
A \oplus N_A \ar[r] \ar[d] & A \ar[d]\\
B \oplus N \ar[r] & B
}
\]
is cartesian in $\Alg_\K$.
\end{lemma}
\begin{proof}
It is straightforward to verify that the square commutes.
We show that the induced map $A \oplus N_A \to (B \oplus N) \times_B A$ is an isomorphism.
Suppose $(a,n)$ maps to $0$.
This means that $((\phi(a),n),a) = ((0,0),0)$ is zero, so $(a,n) = (0,0)$.
Let $((b,n),a) \in (B \oplus N) \times_B A$.
Then $\phi(a) = b$ and thus $(a,n)$ maps to $((b,n),a)$.
\end{proof}

\begin{lemma}
A morphism $(\phi,f)\colon (A,M) \to (B,N)$ in $\Mod$ defines a morphism $(\phi \oplus f,\phi)\colon (A \oplus M \to A,A) \to (B \oplus N,B)$ in $T$.
\end{lemma}
\begin{proof}
We only verify that $A \oplus M \to (B \oplus N) \times_B A$ is a morphism of commutative group objects (over $A$).
By \cref{lem:square-zero-base-change}, this is the same as asking $A \oplus M \to A \oplus N$ to be a morphism of commutative group objects (over $A$), which is clear (since e.g. taking trivial square-zero extensions is a functor from $\Mod_{A,A} \to \Ab((\Alg_\K)_{/A})$).
\end{proof}
\begin{exposition}
We see that taking square-zero extensions defines a functor $\Mod \to T$.
\end{exposition}

\begin{proposition}
The functor $\Mod \to T$ is an equivalence of categories.
\end{proposition}
\begin{proof}
By \cref{prop:module-tangent-category-fiber}, the functor is essentially surjective.
We are reduced to showing that every morphism $(A \oplus M \to A,A) \to (B \oplus N,B)$ in $T$ comes uniquely from $\phi\colon A \to B$ and $f\colon M \to N$.
Uniqueness and existence of $\phi$ is clear.
Since $A \oplus M \to (B \oplus N) \times_B A \simeq A \oplus N_A$ is a morphism of abelian group objects, by \cref{prop:module-tangent-category-fiber}, it is given by ``$1_A \oplus f $'' for a unique morphism $f\colon M \to N$.
This gives the required $f$.
\end{proof}
