\chapter{Algebra}

\section{Rings}

\begin{exposition}
A \addindex{noncommutative ring} is an algebra in the symmetric monoidal $\Ab$ (given the tensor product).

A \addindex{commutative ring}, or just \addindex{ring}, is a \emph{commutative} algebra in the symmetric monoidal $\Ab$ (given the tensor product).
A \addindex{ring homomorphism} (or morphism of rings) is a algebra homomorphism.

We get a category $\Ring$ of (commutative) rings.

Then $\Z$ is initial in $\Ring$.
\end{exposition}

\begin{exposition}
Since $\Z$ is initial in $\Ring$, for every ring $A$, if $n \in \Z$, we sometimes write $n$ for the image of $n \in A$.
\end{exposition}

\begin{exposition}
The \addindex{additive group} of a ring $A$ is the abelian group $\G_a(A)$ given by forgetting the multiplication on $A$.
\end{exposition}

\begin{exposition}
Let $A$ be a ring with multiplication $\cdot$.

We call the (pointed) monoid $A$ (with multiplication $\cdot$ and multiplicative identity $1$ as in the definition of a ring) the \addindex{multiplicative monoid} of $A$.

Inverses for $\cdot$ are called \addindex{multiplicative inverses}, or just inverses.

If $a \in A$ has an inverse, written $a^{-1}$, $a$ is called a \addindex{unit} (of or in $A$).
Denote the \addindex{group of units} (or \addindex{multiplicative group}) of a ring $A$ as
\[
\G_m(A) \eqdef \{a \in A:\text{$a$ is a unit of $A$}\}\nsp{.}
\]
This an abelian group.

A \addindex{nonunit} in $A$ is an element of $A$ which is not a unit.
\end{exposition}

\begin{proposition}
\label{prop:zero-ring-characterization}
Let $A$ be a ring.
The following are equivalent:
\begin{enumerate}
\item $A$ has only one element (i.e. $A$ is zero as an abelian group)
\item $1=0$ in $A$
\item $0$ is a unit in $A$.
\end{enumerate}
\end{proposition}
\begin{proof}
\itemimply{1}{2}.
Clear.

\itemimply{2}{3}.
Indeed, then $0$ a multiplicative inverse of itself, as $0 \cdot 0 = 1 \cdot 1 = 1$.

\itemimply{3}{1}.
Let $a \in A$.
Then $a \cdot 0 = 0 = 0 \cdot 0$, so multiplying by a inverse of $0$ gives $a = 0$.
Hence $A = \{0\}$.
\end{proof}

\begin{exposition}
We say a ring is a \addindex{zero ring} if it satisfies any of the conditions in \cref{prop:zero-ring-characterization}.
We sometimes write $0$ or $\zero$ for a zero ring.

Then every zero ring is a terminal object in $\Ring$.
\end{exposition}

\begin{exposition}
A \addindex{zero divisor} in a ring $A$ is an element $a \in A$ such that there exist nonzero $b \in A$ such that $ab = 0$.
\end{exposition}

\begin{proposition}
\label{prop:domain-characterization}
Let $A$ be a nonzero ring.
The following are equivalent:
\begin{enumerate}
\item $A$ has no nonzero zero divisors
\item $A$ is a cancellative monoid.
\end{enumerate}
\end{proposition}
\begin{proof}
\itemimply{1}{2}.
Omitted.

\itemimply{2}{1}.
Let $a \in A$ be a zero divisor.
Suppose $ab = 0$, where $b$ is nonzero.
Then $ab =0 = 0 \cdot b$, which implies $a = 0$.

Since $1 \cdot 0 = 0 \cdot 0$,
\end{proof}

\begin{exposition}
A ring is a \addindex{domain} if it is nonzero and satisfies any of the conditions in \cref{prop:domain-characterization}.
\end{exposition}

\begin{example}
The integers $\Z$ are a domain.
\end{example}

\begin{exposition}
A \addindex{field} is a ring $A$ such that the set $A \setminus \{0\}$ is the group of units of $A$.
\end{exposition}

\begin{exposition}
Let $A$ be a domain.

We define the (standard) \addindex{field of fractions} $\Frac(A)$ as follows.
As a set, we define $\Frac(A)$ to be the set $A \times (A\setminus\{0\})/{\sim}$ where $\sim$ is the equivalence relation defined by $(a,b) \sim (c,d)$ if $ad - bc = 0$.

We define
\[
[(a,b)]+[(c,d)] \eqdef [(ad+bc,bd)]
\]
and
\[
[(a,b)][(c,d)] \eqdef [(ac,bd)]\nsp{.}
\]
This defines an addition and multiplication on $\Frac(A)$ making a $\Frac(A)$ a field (the additive identity is $[(0,1)]$, the multiplicative identity is $[(1,1)]$, and the inverse of a nonzero $[(a,b)]$ is $[(b,a)]$).

There is an injective map $A \to \Frac(A)$ given by $a \mapsto [(a,1)]$.
Inspection of the definitions above shows that this makes $A$ into a subring of $\Frac(A)$, and we frequently make this identification (writing e.g. $A \subset \Frac(A)$).

Elements of $\Frac(A)$ are written like $a/b$ or $\frac{a}{b}$ instead of $[(a,b)]$.
We often write $a$ instead of $a/1$ (there is no risk of ambiguity because the map $A \to \Frac(A)$ is injective).
Observe that $1 = [(1,1)]$ is the multiplicative identity of $\Frac(A)$ and $0 = [(0,1)]$ is the additive identity of $\Frac(A)$.
\end{exposition}

\begin{proposition}
Let $A$ be a domain.
Let $B$ be a field.
Every injective ring homomorphism $A \to B$ extends uniquely to a ring homomorphism $\Frac(A) \to B$.
\end{proposition}
\begin{proof}
Omitted.
\end{proof}

\section{Permutation Groups}

\begin{exposition}
Let $n$ be a natural number.
The group
\[
\Perm_n \eqdef \Aut(\{1,\dotsc,n\})
\]
of bijections $\{1,\dotsc,n\}\to \{1,\dotsc,n\}$is called the \addindex{permutation group} on $n$ elements.
An element of $\Perm_n$ is called a \addindex{permutation}.
Observe $\Perm_n$ is finite a group (it is a subset of of $\End(\{1,\dotsc,n\})$, which has cardinality $n^n$).

If $\sigma \in \Perm_n$ and $1 \leq i \leq n$, we sometimes write $\sigma_i \eqdef \sigma(i)$.
\end{exposition}

\begin{exposition}
Let $n \in \N$.
We define the \addindex{factorial} of $n$ to be
\[
n! \eqdef \card{\Perm_n}\nsp{.}
\]
Clearly we have
\[
0! = 1\nsp{.}
\]

Now let $n \gt 0$.
Let $X$ be the set of $n-1$ element subsets of $\{1,\dotsc,n\}$.
Then $\card{X} = n$ (just pick an element to exclude).
The group $\Perm_n$ acts on the set of $n-1$ element subsets of $X$ (by taking images under maps in $\Perm_n$).
This is clearly transitive (details left to the reader).
Hence there is only one orbit, so it follows from \cref{thm:orbit-stabilizer-theorem,thm:lagrange-theorem} that
\[
n = \card{X} = \card{\Perm_n/(\Perm_n)_{\{1,\dotsc,n-1\}}} = \card{\Perm_n}/\card{(\Perm_n)_{\{1,\dotsc,n-1\}}}\nsp{.}
\]
There is a group isomorphism $(\Perm_n)_{\{1,\dotsc,n-1\}} \simeq \Perm_{n-1}$ (details left to the reader), so it follows that
\[
n! = n \cdot (n-1)!\nsp{.}
\]
In other words,
\[
n! = n \cdot (n-1) \cdot\dotsb \cdot 1\nsp{.}
\]
\end{exposition}

\begin{exposition}
Let $n \in \N$.
A permutation in $\Perm_n$ is called a \addindex{transposition} if it fixes all elements of $\{1,\dotsc,n\}$ except two elements.
\end{exposition}

\begin{proposition}
Let $n \in \N$.
The following hold:
\begin{enumerate}
\item the transpositions switching $i$ and $i+1$ in $\Perm_n$ generate $\Perm_n$
\item if $\tau$ and $\tau'$ are transpositions, then there is a $\sigma \in \Perm_n$ such that $\tau = \sigma\tau'\sigma^{-1}$ (i.e. all transpositions in $\Perm_n$ are conjugates).
\end{enumerate}
\end{proposition}
\begin{proof}
\proofitem{1}.
Omitted.

\proofitem{2}.
Suppose $\tau$ switches $i$ and $j$, and $\tau'$ switches $i'$ and $j'$.
Then we can let $\sigma$ be the element of $\Perm_n$ which switches $i$ and $i'$ and also switches $j$ and $j'$.
\end{proof}

\begin{exposition}
Let $n \in \N$.
The \addindex{length of a permutation} $\sigma \in \Perm_n$ is defined to be the number of two element subsets $\{i,j\}$ of $\{1,\dotsc,n\}$ such that $i \lt j$ and $\sigma(i) \gt \sigma(j)$, i.e. $\sigma$ reverses the order of $i$ and $j$.
\end{exposition}

\begin{proposition}
\label{prop:sign-homomorphism}
Let $n \in \N$.
For each $\sigma \in \Perm_n$, write $l(\sigma)$ for the length of $\sigma$.
Define a map $\sgn\colon \Perm_n \to \G_m(\Z)$ by
\[
\sgn(\sigma) \eqdef (-1)^{l(\sigma)}\nsp{.}
\]
The following hold:
\begin{enumerate}
\item $\sgn$ is a group homomorphism
\item if $\tau \in \Perm_n$ is a transposition, then $\sgn(\tau) = -1$
\item if $n \gt 1$, then $\sgn$ is the unique group nontrivial homomorphism $\Perm_n \to \G_m(\Z)$.
\end{enumerate}
\end{proposition}
\begin{proof}
\proofitem{1}.
Let $\sigma$ and $\sigma'$ be elements of $\Perm_n$.
Consider a two element subset $\{i,j\}$ of $\{1,\dotsc,n\}$, and without loss of generality assume $i \lt j$.
Then $\sigma\sigma'(i) \gt \sigma\sigma'(j)$ if and only if one of the following hold:
\begin{enumerate}[(a)]
\item $\sigma'(j) \lt \sigma'(i)$ and $\sigma(\sigma'(j)) \lt \sigma(\sigma'(i))$, or in other words $\sigma'$ switches the order of $i$ and $j$ and $\sigma$ does not switch the order of $\sigma'(i)$ and $\sigma'(j)$
\item $\sigma'(i) \lt \sigma'(j)$ and $\sigma(\sigma'(j)) \lt \sigma(\sigma'(i))$, or in other words $\sigma'$ does not switch the order of $i$ and $j$ and $\sigma$ switches the order or $\sigma'(i)$ and $\sigma'(j)$.
\end{enumerate}
Since $\sigma$ is a bijection, it gives a bijection on two element subsets, and it follows $\sgn(\sigma\sigma') = \sgn(\sigma)\sgn(\sigma')$ (details left to the reader; basically each ``contribution'' on the left corresponds to a ``contribution'' on the right coming from exactly one of $\sgn(\sigma)$ and $\sgn(\sigma')$).

\proofitem{2}.
Let $\tau$ be a transposition in $\Perm_n$.
Assume $i \lt j$ and $\tau(i) = j$.
We only get factors in $\sgn(\tau)$ for subsets of the form $\{i,k\}$ for $i \lt k$ and $\{k,j\}$ for $k \lt j$.
There are exactly $(j-i-1) + (j-i-1) - 1$ subsets (the $-1$ comes from double counting $\{i,j\}$).
This is an odd number, so $\sgn(\tau) = -1$.

\proofitem{3}.
The fact that $\sgn$ is nontrivial follows from \proofitem{2}.

Let $\sgn'\colon \Perm_n \to \G_m(\Z)$ be another nontrivial group homomorphism.
Furthermore, since the transpositions generate $\Perm_n$, nontriviality of $\sgn'$ implies that $\sgn'(\tau) = -1$ for some transposition $\tau \in \Perm_n$.
But all transpositions in $\Perm_n$ are conjugates, so it follows (using the fact that $\G_m(\Z)$ is abelian) that $\sgn'(\tau) = -1$ for every transposition $\tau \in \Perm_n$.
Then $\sgn'$ and $\sgn$ agree on all transpositions.
Since the transpositions generate $\Perm_n$, it follows that $\sgn = \sgn'$.
\end{proof}

\begin{exposition}
Let $n \in \N$.
Then the group homomorphism $\sgn\colon \Perm_n \to \G_m(\Z)$ (as in \cref{prop:sign-homomorphism}) is sometimes called the \addindex{sign homomorphism}.
\end{exposition}

\section{Multinomial Theorem}

\begin{exposition}
Let $n \in \N$ and fix $m \in \N_{\gt 0}$ (we will assume $m \geq 2$; the definition makes sense for $m = 1$, but it is not very useful).
Fix a family of integers $(i_k)_{k = 1}^m$.
We define
\[
\binom{n}{i_1,\dotsc,i_m}
\]
to be the number of partitions $(X_k)_{k = 1}^m$ of $\{1,\dotsc,n\}$ such that $\card{X_k} = i_k$ for all $k$.
We refer to $\binom{n}{i_1,\dotsc,i_m}$ as a \addindex{multinomial coefficient}.
It is customary to write $\binom{n}{i_1}$ instead of $\binom{n}{i_1,i_2} = \binom{n}{i_1,n-i_1}$, and $\binom{n}{i_1}$ is referred to as a \addindex{binomial coefficient}.

The permutation group $\Perm_n$ acts on $X$ by taking images of the $X_k$.
This action is transitive.

Pick $x \in X$.
By \cref{thm:orbit-stabilizer-theorem,thm:lagrange-theorem}, we have
\[
\card{X} = \subindex{\Perm_n}{(\Perm_n)_x} = \card{\Perm_n}/\card{(\Perm_n)_x}\nsp{.}
\]
There is a group isomorphism $G_x \simeq \prod^m_{k=1} \Perm_{i_k}$ (details left to the reader).
Thus we see that
\[
\binom{n}{i_1,\dotsc,i_j} = \frac{n!}{\prod_{k=1}^m i_k!} = \frac{n!}{i_1!\cdot\dotsc\cdot i_m!}\nsp{.}
\]

If $\iota \in \N^m$ with $\iota = (i_1,\dotsc,i_m)$ and $\sum_{k=1}^m i_k= n$, we may write $\binom{n}{\iota}$ instead of $\binom{n}{i_1,\dotsc,i_m}$.
\end{exposition}

\begin{theorem}[Pascal's identity]
\label{thm:pascal-identity}
Let $n \geq 1$.
Then
\[
\binom{n}{i_1,\dotsc,i_k} = \binom{n-1}{i_1-1,\dotsc,i_k} + \dotsb + \binom{n-1}{i_1,\dotsc,i_k-1}\nsp{.}
\]
\end{theorem}
\begin{proof}
Details left to the reader (hint: for a partition $(X_k)$ of $\{1,\dotsc,n\}$ and a fixed $j$ count the number of partitions where $1 \in X_j$).
\end{proof}

\begin{lemma}
Let $\K$ be a ring.
For every $x \in \K$, and $n \in \N$, define
\[
(x)_n \eqdef x \cdot (x-1) \cdot \dotsb \cdot (x-(n-1))\nsp{.}
\]
Then for every $x \in \K$ and $n \geq 1$,
\[
(x+1)_{n+1} = (x)_{n+1} + (n+1)(x)_n\nsp{.}
\]
\end{lemma}
\begin{proof}
We have
\[
\begin{aligned}
(x)_{n+1} + (n+1)(x)_n & = x \cdot (x-1) \cdot \dotsb \cdot (x-n) + (n+1) \cdot x \cdot (x-1) \cdot \dotsb \cdot (x-(n-1)) \\
                       & = (x-n + n+1)\left(x \cdot (x-1) \cdot \dotsb \cdot (x-(n-1))\right)                              \\
                       & = (x+1) \cdot ((x+1)-1) \cdot \dotsb ((x + 1) -((n+1)-1)))                                        \\
                       & = (x+1)_{n+1}\nsp{.}
\end{aligned}
\]
\end{proof}

\begin{proposition}[Chu--Vandermonde identity]
Let $\K$ be a ring.
For every $x \in \K$, and $n \in \N$, define
\[
(x)_n \eqdef x \cdot (x-1) \cdot \dotsb \cdot (x-(n-1))\nsp{.}
\]
Then for every $x$ and $y$ in $\K$ and $n \in \N$,
\[
(x+y)_n = \sum_{k=0}^n \binom{n}{k}(x)_k(y)_{n-k}\nsp{.}
\]
\end{proposition}
\begin{proof}
We induct on $n$.
The result is clear when $n = 1$.

Assuming the result holds for $n$, we have
\[
\begin{aligned}
(x+y)_n & = (x+y-n)(x+y)_n                                                                                                                                                                           \\
        & = (x+y-n)\sum_{k=0}^n \binom{n}{k}(x)_k(y)_{n-k}                                                                                                                 &  & \text{by hypothesis} \\
        & = \sum_{k=0}^n \binom{n}{k}\left((x-k)+(y-(n-k))\right)(x)_k(y)_{n-k}                                                                                                                      \\
        & = \sum_{k=0}^n \binom{n}{k}(x)_{k+1}(y)_{n-k} + \sum_{k=0}^n \binom{n}{k}(x)_k(y)_{n-k+1}                                                                                                  \\
        & =  \sum_{k=0}^{n-1} \binom{n}{k}(x)_{k+1}(y)_{n-k} + \binom{n}{n}(x)_{n+1}(y)_{0} + \binom{n}{0}(x)_0(y)_{n+1} + \sum_{k=1}^n \binom{n}{k}(x)_k(y)_{n-k+1}                                 \\
        & =  \sum_{k=1}^{n} \binom{n}{k-1}(x)_{k}(y)_{n-k+1} + \binom{n+1}{n+1}(x)_{n+1}(y)_{0} + \binom{n+1}{0}(x)_0(y)_{n+1} + \sum_{k=1}^n \binom{n}{k}(x)_k(y)_{n-k+1}                           \\
        & =  \binom{n+1}{0}(x)_0(y)_{n+1} + \sum_{k=1}^n\left(\binom{n}{k-1} + \binom{n}{k}\right)(x)_k(y)_{n-k+1} + \binom{n+1}{n+1}(x)_{n+1}(y)_{0}                                                \\
        & =  \binom{n+1}{0}(x)_0(y)_{n+1} + \sum_{k=1}^n\binom{n+1}{k}(x)_k(y)_{n-k+1} + \binom{n+1}{n+1}(x)_{n+1}(y)_{0}                                                                            \\
        & = \sum_{k=0}^{n+1}\binom{n+1}{k}(x)_k(y)_{n+1-k}
\end{aligned}
\]
and the result follows.

\end{proof}

\begin{theorem}[Multinomial theorem]
\label{thm:multinomial-theorem}
Let $A$ be a ring.
If $m \in \N$, $\iota = (i_1,\dotsc,i_m)\in \N^m$, write $\abs{\iota}$ for $\sum_{k=1}^m i_k$.
If $(x_i)_{i=1}^m$ is a family of elements of $A$, then
\[
(x_1 + \dotsb + x_m)^n = \sum_\iota \binom{n}{\iota}x^\iota\nsp{,}
\]
where the sum is over all $\iota \in \N^m$ such that $\abs{\iota} = m$.
\end{theorem}
\begin{proof}
Omitted.
\end{proof}

\begin{exposition}
The \addindex{characteristic} of a ring is the smallest positive integer dividing zero in the divisibility preorder.
\end{exposition}
