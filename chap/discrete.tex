\chapter{Discrete Mathematics}
\label{chap:discrete}

\section{Linear Recurrences}
\label{sec:linear-recurrences}

\begin{exposition}
Let $\K$ be commutative ring.

We define the \addindex{shift operator} $S\colon \K^\N \to \K^\N$ by
\[
S\left((a_i)_{i \in \N}\right) \eqdef (a_{i+1})_{i \in \N}\nsp{.}
\]

The \addindex{forward difference operator} is defined as
\[
\Delta \eqdef S - 1\nsp{.}
\]

It is clear that $S$ and $\Delta$ are $\K$-linear maps.
\end{exposition}

\begin{proposition}
\label{prop:forward-difference-properties}
The following hold:
\begin{enumerate}
\item for all $k \geq 0$,
      \[
      \Delta^k = \sum_{i=0}^k(-1)^i\binom{k}{i}S^{k-i}
      \]
\item for all $k \geq 0$,
      \[
      S^k = \sum_{i=0}^k\binom{k}{i}\Delta^i
      \]
\item if $a = (a_n) \in \K^n$, then $\Delta^ka = 0$ if and only if there exists a polynomial $g \in \K[x]$ such that $a_n = g(n)$ for all $n$ and $\deg(g) \lt k$.
\end{enumerate}
\end{proposition}
\begin{proof}
\proofitem{1}.
This follows from $\Delta^k = (S-1)^k$ (then use \cref{thm:multinomial-theorem}).

\proofitem{2}.
This follows from $S^k = (\Delta+1)^k$ (then use \cref{thm:multinomial-theorem}).

\proofitem{3}.
Assume $\Delta^k =0$.
By \proofitem{2}, we can write
\[
a_n = (S^na )_0 = \left(\sum_{i=0}^n \binom{n}{i}\Delta^ia\right)_0 = \sum_{i=0}^n \binom{n}{i}\Delta^ia_0 =\sum_{i=0}^{k-1} \binom{n}{i}\Delta^ia_0\nsp{.}
\]
We can then take $g(x) \eqdef \sum_{i=0}^{k-1} \binom{x}{i}\Delta^ia_0$.

Assume $g(n) = a_n$ for some polynomial $g$ with $\deg(g) \lt k$.
Then $(\Delta a)_n = g(n+1)-g(n)$ for all $n$, and $\deg(g(x+1)-g(x)) \lt k-1$, so by induction $\Delta^k = \Delta^{k-1}(\Delta a) = 0$.
\end{proof}

\begin{exposition}
Let $\K$ be an algebraically closed field.
A sequence $(a_n)_{n \in \N}$ of elements of a $\K$-vector space satisfies a \addindex{linear recurrence} with \addindex{coefficients} $(c_i \in \K)_{i=0}^k$ if
\[
c_ka_{n+k} + \dotsb + c_0a_n = 0
\]
for all $n \in \N$.

Given $(c_i)_{i=0}^k$ with $c_k \neq 0$ and $c_0 \neq 0$, we want to look at sequences satisfying the linear recurrence defined by $(c_i)$.
Any such sequence determined by its first $k$ terms ($a_0,\dotsc,a_{k-1}$), which can be arbitrary.

We have a matrix equation
\[
\begin{pmatrix}a_{n+k} \\ \vdots \\ a_{n+1}\end{pmatrix} = \begin{pmatrix}-\frac{c_{k-1}}{c_k} & \cdots & -\frac{c_1}{c_k} & -\frac{c_0}{c_k} \\ 1 &  &  &  &  \\ &  \ddots &  & \\ &  & 1 \end{pmatrix}\begin{pmatrix}a_{n+k-1} \\ \vdots \\ a_n\end{pmatrix}
\]
for each $n \in \N$.
Let $C$ be the matrix above and let $e_n \eqdef \begin{pmatrix}a_{n+k-1} \\ \vdots \\ a_n\end{pmatrix}$ for all $n \in \N$.
We then get
\[
e_n = C^ne_0
\]
for all $n$.

The characteristic polynomial of $C$ is the determinant of
\[
x1 - C = \begin{pmatrix}x+\frac{c_{k-1}}{c_k} & \frac{c_{k-2}}{c_k} & \cdots & \frac{c_0}{c_k}   \\
               -1                    & x                   &        &                 & \\
                                     & \ddots              & \ddots &                   \\
                                     &                     & -1     & x\end{pmatrix}\nsp{.}
\]
Doing cofactor expansion on the bottom row we get that this determinant equals
\[
x\det \begin{pmatrix}x+\frac{c_{k-1}}{c_k} & \frac{c_{k-2}}{c_k} & \cdots &   \frac{c_1}{c_k} \\ -1 & x &  &  &  \\    & \ddots & \ddots &  \\ &  & -1& x \end{pmatrix} -(-1) \det \begin{pmatrix}x+\frac{c_{k-1}}{c_k} & \frac{c_{k-2}}{c_k} & \cdots &  \frac{c_2}{c_k} & \frac{c_0}{c_k} \\ -1 & x &  &  &  \\  & \ddots & \ddots & & \\ &  & -1 & x &  \\ & &  & -1&  \end{pmatrix}
\]
and then doing cofactor expansion on the rightmost column of the second (right) matrix we get that this equals
\[
x\det \begin{pmatrix}x+\frac{c_{k-1}}{c_k} & \frac{c_{k-2}}{c_k} & \cdots &   \frac{c_1}{c_k} \\ -1 & x &  &  &  \\    & \ddots & \ddots &  \\ &  & -1& x \end{pmatrix} -(-1)(-1)^k\left(\frac{c_0}{c_k}\right)(-1)^{k-2} = x\det \begin{pmatrix}x+\frac{c_{k-1}}{c_k} & \frac{c_{k-2}}{c_k} & \cdots &   \frac{c_1}{c_k} \\ -1 & x &  &  &  \\    & \ddots & \ddots &  \\ &  & -1& x \end{pmatrix}+\frac{c_0}{c_k}
\]
and an inductive argument shows that this equals
\[
x\left(x\left(\dotsm\left(x+\frac{c_{k-1}}{c_k}\right)\right)\right) + \frac{c_0}{c_k} = x^k + \frac{c_{k-1}}{c_k}x^{k-1} + \dotsb + \frac{c_0}{c_k}\nsp{.}
\]

Since $c_0 \neq 0$, all eigenvalues of $C$ are nonzero ($0$ is not a root of the characteristic polynomial).
Given a Jordan block $J$ of size $m$ with eigenvalue $\lambda$, we can write $J = \lambda I + N$, where $N$ is the matrix with only nonzero entries equal to one on the superdiagonal.
By the binomial theorem we have $J^n = (\lambda I + N)^n = \sum \binom{n}{r}\lambda^{n-r}N^r$ so for every $n \in \N$,
\[
J^n = \begin{pmatrix}\binom{n}{0}\lambda^n & \binom{n}{1}\lambda^{n-1} & \cdots & \cdots                & \binom{n}{m-1}\lambda^{n-(m-1)} \\
                                     & \binom{n}{0}\lambda^n     & \ddots & \ddots                & \vdots                          \\
                                     &                           & \ddots & \ddots                & \vdots                          \\
                                     &                           &        & \binom{n}{0}\lambda^n & \binom{n}{1}\lambda^{n-1}       \\
                                     &                           &        &                       & \binom{n}{0} \lambda^n
\end{pmatrix} =
\begin{pmatrix}\binom{n}{0}\lambda^n & \lambda^{-1}\binom{n}{1}\lambda^n & \cdots & \cdots                & \lambda^{-(m-1)}\binom{n}{m-1}\lambda^n \\
                                     & \binom{n}{0}\lambda^n             & \ddots & \ddots                & \vdots                                  \\
                                     &                                   & \ddots & \ddots                & \vdots                                  \\
                                     &                                   &        & \binom{n}{0}\lambda^n & \lambda^{-1}\binom{n}{1}\lambda^n       \\
                                     &                                   &        &                       & \binom{n}{0} \lambda^n
\end{pmatrix}
\nsp{.}
\]
If $(v_i)$ are the generalized eigenvectors corresponding to this Jordan block, it follows that there exist polynomials $f_i \in \K[x]$ such that $\deg(f_i) \leq m - 1$ and such that
\[
C^n(v) = \sum f_i(n)\lambda^nv_i
\]
for every $n \in \N$.

Let $\prod (x-\lambda_j)^{m_j}$ be the factorization of the characteristic polynomial of $C$ (where the $\lambda_j$ are distinct).
Let $p$ the projection onto the last component of $\K^n$.
Then we have  $a_n = p(e_n) = p(C^ne_0)$ for each $n$, and from all the considerations above we see that we can find polynomials $g_j \in \K[x]$ such that $\deg(g_j) \lt m_j$ for each $j$ and such that
\[
a_n = \sum_j g_j(n)\lambda_j^n
\]
for each $n \in \N$.

Conversely, fix some $j$ and let $m \eqdef 0$ and let $\lambda \eqdef \lambda_j$.
Let $g \in \K[x]$ have degree $\deg(g) \lt m$.
We show that $a \eqdef (g(n)\lambda^n)$ satisfies the linear recurrence with coefficients $(c_i)$.
Since $\lambda$ is a root of the characteristic polynomial $p$ of $C$, which equals $x^k + \frac{c_{k-1}}{c_k}x^{k-1} + \dotsb + \frac{c_0}{c_k}$.
We can write the linear recurrence as
\[
p(S)a = 0\nsp{,}
\]
and factoring $p$, we see that it enough to prove that
\[
(S - \lambda 1)^ma= 0\nsp{.}
\]
We define $a' \in \K^\N$ by $a'_n \eqdef g(n)$.
If $c = \lambda^nc'$, then $((S-\lambda)c)_n = (\lambda^{n+1}c')_n$, so by induction we see that
\[
((S-\lambda 1)^ma)_n = \lambda^{n+1}((S-\lambda1)^ma')_n\nsp{.}
\]
The latter is zero since $\deg(g) \lt m$ (\cref{prop:forward-difference-properties}).
Thus $a$ satisfies the linear recurrence.

Thus we see that if $a$ is a sequence and there are polynomials $g_j \in \K[x]$ such that $\deg(g_j) \lt m_j$ for each $j$ and such that
\[
a_n = \sum_j g_j(n)\lambda_j^n
\]
for each $n \in \N$, then $a$ satisfies the linear recurrence.
\end{exposition}

%https://mathcircle.berkeley.edu/sites/default/files/BMC3/Bjorn1.pdf
%https://www.math.uvic.ca/faculty/gmacgill/guide/recurrences.pdf

\section{Muirhead's inequality}

\begin{exposition}
A matrix $A$ is \addindex{bistochastic matrix} if it has positive entries, and the rows and columns of $A$ each sum to $1$.
By summing over rows and columns and comparing the results, such a matrix necessarily is square.
\end{exposition}

\begin{lemma}
The following hold:
\begin{enumerate}
\item the multiplication of bistochastic matrices is bistochastic
\item the $n \times n$ bistochastic matrices form a compact convex subset of $\Mat_n(\R)$.
\end{enumerate}
\end{lemma}
\begin{proof}
Omitted.
\end{proof}

\begin{exposition}
A \addindex{permutation matrix} is a matrix representing a linear transformation which permutes basis vectors.
\end{exposition}

\begin{proposition}
The permutation matrices are the extreme points of the set of bistochastic matrices.
\end{proposition}
\begin{proof}
It is straightforward to see that permutation matrices are bistochastic matrices, and that a bistochastic matrix is a permutation matrix if and only if each row has an entry equal to $1$.

Let $X$ be permutation matrix.
Suppose $X = tY + (1-t)Z$, where $Y$ and $Z$ are bistochastic and $t \in \ointerval{0}{1}$.
If $x_{ij} = 0$, then $ty_{ij} + (1-t)z_{ij} = 0$ implies $y_{ij} = 0 = z_{ij}$ since $0 \leq y_{ij},z_{ij} \leq 1$.
A similar argument shows $y_{ij} = 1= z_{ij}$ whenever $x_{ij} = 1$.
It follows $X$ is an extreme point.

Now let $X$ be an extreme point.
Suppose $X$ is not a permutation matrix.
We claim there exists $Y \in \Mat_n(\R)$ such that $X\pm Y$ are bistochastic, giving a contradiction.
Choose $x_{i_0j_0}$ such that $0 \lt x_{i_0j_0} \lt 1$.
There exists $j_1 \neq j_0$ such that $0 \lt x_{i_0j_1} \lt 1$.
There exists $i_1 \neq i_0$ such that $0 \lt x_{i_1j_1} \lt 1$.
Continuing in this manner (alternating changing the row or column), we get indices $(i_0,j_0), (i_0,j_1),(i_1,j_1),\dotsc$.
There exists a cycle in this path of minimal length (because $X$ has only finitely many entries).
Without loss of generality it occurs starting with $(i_0,j_0)$.
Let $(i,j)$ be the indices right before the final step of the path.
Then the transition $(i,j) \to (i_0,j_0)$ must change $i$ to $i_0$, otherwise we could instead transition $j$ to $j_1$ to get a smaller length path (by omitting $(i_0,j_0)$.
Define $Y$ by following the path, alternating entries $\epsilon$ and $-\epsilon$.
Then the sum of each row and column of $Y$ is zero, so the sums of rows and columns of $X \pm Y$ is $1$.
If we take $\epsilon$ small enough, we can ensure that $X \pm Y$ is bistochastic.
\end{proof}

\begin{theorem}[Birkhoff--von Neumann]
Every bistochastic matrix is a convex combination of permutation matrices.
\end{theorem}
\begin{proof}
The bistochastic matrices form a compact convex subset of $\Mat_n(\R)$.
Hence by \cref{thm:krein-milman-theorem}, it equals the closed convex hull of its extreme points.
The extreme points are the permutation matrices.
Since there are only finitely many permutation matrices in $\Mat_n(\R)$, the set $E$ of permutation matrices is compact.
Since $\Mat_n(\R)$ is finite-dimensional, we see that $\co(E)$ is compact, so $\clco(E) = \co(E)$.
\end{proof}

\begin{proposition}
\label{prop:majorize-characterization}
Let $x$ and $y$ be elements of $(\R_{\geq 0})^n$.
Suppose $x$ and $y$ are decreasing.
The following are equivalent:
\begin{enumerate}
\item $\sum_{i=1}^k y_i \leq \sum_{i=1}^k x_i$ for all $k \lt n$ and $\sum_{i=1}^n y_i = \sum_{i=1}^n x_i$
\item there exists a bistochastic matrix $A \in \Mat_n(\R)$ such that $y = Ax$.
\end{enumerate}
\end{proposition}
\begin{proof}
\itemimply{1}{2}.
We induct on $n$.
The case $n = 1$ is clear.

We have $y_1 \leq x_1$ by hypothesis, and $x_n \leq y_1$ (because $x_n \leq y_n \leq y_1$).
Choose $k \gt 1$ such that $x_k \leq y_1 \leq x_{k-1}$.
We can write $y_1 = tx_1 + (1-t)x_k$ for some $t \in \interval{0}{1}$.
Let $T = t1 + (1-t)T_{1k}$, where $T_{ik}$ is the permutation matrix corresponding to the transposition of indices $1$ and $k$.
Then the first component of $Tx$ is $tx_1 + (1-t)x_k = y_1$, and the $k$-th component of $Tx$ is $tx_k+(1-t)x_1$.
For $1 \lt m \lt k$, the $m$-th component of $Tx$ is $x_m$.
Since $x_1 \geq \dotsb \geq x_{k-1} \geq y_1 \geq \dotsb \geq y_n$,
\[
\sum_{i=2}^m y_i = \sum_{i = 2}^m y_i \leq \sum_{i=2}^m x_i = \sum_{i=2}^m (Tx)_i
\]
for all such $1 \lt m \lt k$.
For $k \leq m \leq n$,
\[
\begin{aligned}
\sum_{i=2}^m (Tx)_i & = \sum_{i=2}^{k-1} x_i + \left(tx_k + (1-t)x_1\right) + \sum_{i=k+1}^m x_i \\
                    & = \sum_{i=2}^m x_i -tx_1 + (t-1)x_k                                        \\
                    & = \sum_{i=1}^m x_i - y_1\nsp{.}
\end{aligned}
\]
Thus we see that we can apply induction to $(x_2,\dotsc,x_n)$ and $((Tx)_2,\dotsc,(Tx)_n)$ to get a bistochastic matrix $B \in \Mat_{n-1}(\R)$ such that $B(((Tx)_2,\dotsc,(Tx)_n)) = (x_2,\dotsc,x_n)$.
If we put $B' \eqdef 1 \oplus S$, then $y = B'Tx$ and $A \eqdef B'T$ is bistochastic.

\itemimply{2}{1}.
Let $t_i \eqdef \sum_{j=1}^k a_{ij}$.
Note
\[
\sum_{i=1}^n t_i = \sum_{i=1}^n \sum_{j=1}^k a_{ij} = \sum_{j=1}^k \sum_{i=1}^n a_{ij} = k\nsp{.}
\]
We have
\[
\begin{aligned}
\sum_{i=1}^k y_i - \sum_{i=1}^k x_i & = \sum_{i=1}^k \sum_{j=1}^n a_{ij}x_j - \sum_{i=1}^k x_i             \\
                                    & = \sum_{i=1}^n t_ix_i - \sum_{i=1}^k x_i + (k - \sum_{i=1}^n t_i)x_k \\
                                    & = \sum_{i=1}^k (t_i-1)(x_i-x_k) + \sum_{i \gt k} t_i(x_i-x_k)\nsp{.}
\end{aligned}
\]
The two summands are negative ($t_i \leq 1$ for all $i$ and $x_i \leq x_k$ for $i \gt k$).
When $k = n$, then $t_i = 1$ for all $k$ so both summands are zero.
\end{proof}

\begin{exposition}
Let $x$ and $y$ be elements of $(\R_{\geq 0})^n$.
We say $x$ \addindex{majorizes} $y$ if after sorting $x$ and $y$ in decreasing order, the conditions in \cref{prop:majorize-characterization} hold.
\end{exposition}

\begin{theorem}[Muirhead's inequality]
Let $p$ and $q$ be elements of $(\R_{\geq 0})^n$.
Suppose $p$ majorizes $q$.
Then if $x_1,\dotsc,x_n$ are positive reals, then
\[
\sum x_{\sigma(1)}^{p_1} \dotsm x_{\sigma(n)}^{p_n} \geq \sum x_{\sigma(1)}^{q_1} \dotsm x_{\sigma(n)}^{q_n}\nsp{.}
\]
where $\sigma$ ranges over all permutations of $\{1,\dotsc,n\}$.
\end{theorem}
\begin{proof}
Write $q = Ap$ for some bistochastic matrix $A$.
Write $A$ as a convex combination $\sum t_iP_i$ of permutation matrices.
Then
\[
\begin{aligned}
\sum x_{\sigma(1)}^{p_1} \dotsm x_{\sigma(n)}^{p_n} & = \left(\sum_i t_i\right)\sum_\sigma x_{\sigma(1)}^{p_1} \dotsm x_{\sigma(n)}^{p_n}   \\
                                                    & = \sum_i t_i\sum_\sigma x_{\sigma(1)}^{p_1} \dotsm x_{\sigma(n)}^{p_n}                \\
                                                    & = \sum_i t_i\sum_\sigma x_{\sigma(1)}^{P_ip_1} \dotsm x_{\sigma(n)}^{P_ip_n}          \\
                                                    & = \sum_\sigma \sum_i t_i x_{\sigma(1)}^{P_ip_1} \dotsm x_{\sigma(n)}^{P_ip_n}         \\
                                                    & \geq \sum_\sigma x_{\sigma(1)}^{\sum t_iP_ip_1} \dotsm x_{\sigma(n)}^{\sum t_iP_ip_n} \\
                                                    & = \sum_\sigma x_{\sigma(1)}^{q_1} \dotsm x_{\sigma(n)}^{q_n}\nsp{.}
\end{aligned}
\]
where we used \cref{prop:weighted-am-gm-inequality} for the inequality step.
\end{proof}

%https://math.stackexchange.com/questions/4899513/defining-majorization-of-vectors-using-doubly-stochastic-matrix
