\chapter{Harmonic Analysis}

\section{Haar Measure}

\begin{lemma}
\label{lem:lp-support}
Let $X$ be a Hausdorff topological space.
Fix a Radon measure on $X$.
Let $p \in \rinterval{1}{\infty}$.
If $f \in L^p(X)$, then $f = 0$ a.e. on the complement of a $\sigma$-compact (and hence $\sigma$-finite) set.
\end{lemma}
\begin{proof}
By \cref{lem:integral-supremum-compact-sets}, we can choose compact $K_n$ such that $\int \abs{f}^p - \abs{f}^p\chi_{K_n} \lt \frac{1}{n}$.
Then $\int_{X \setminus \bigcup K_n} \abs{f}^p = 0$, so $f$ is zero a.e. on $X \setminus \bigcup K_n$ (see \cref{prop:complex-almost-everywhere-zero-characterization}).
\end{proof}

\begin{exposition}
We will be integrating functions on $X \times Y$ with respect to the product Radon measure, where $X$ and $Y$ are Hausdorff topological spaces with fixed Radon measures.
We will frequently apply \cref{thm:fubini-theorem} even though $X$ and $Y$ may not be $\sigma$-finite.
This is valid if the function being integrated has compact support, or if the function is built up out of products of functions in $L^1(X)$ and $L^1(Y)$ by \cref{lem:lp-support}.
\end{exposition}

\begin{lemma}
\label{lem:slice-radon-integral-continuous}
Let $X,Y,Z$ be topological spaces.
Assume $Y$ is Hausdorff.
Let $(x,y) \mapsto xy$ be a continuous map from $X \times Y \to Z$.
Let $x \mapsto u_x$ be the map $X \to Z^Y$, where $u_x(y) = xy$.
Let $f\colon Z \to \C$ be continuous with support $S$.
Let $\mu$ be a Radon measure on $Y$.
Assume for every $x_0 \in X$, there is a neighborhood $V$ of $x_0$ in $X$ such that $\bigcup_{x \in V} u_x^\pre(S)$ is relatively compact in $Y$.
The following hold:
\begin{enumerate}
\item each $f \circ u_x$ is continuous on $Y$ with compact support
\item the map $x \mapsto \int f(xy)\dd{\mu(y)}$ is continuous on $X$.
\end{enumerate}
\end{lemma}
\begin{proof}
\proofitem{1}.
The map $f \circ u_{x_0}$ is given by $y \mapsto f(x_0y)$, so is continuous on $Y$.
The support of $f \circ u_{x_0}$ is the closure of the set
\[
\{y \in Y:f(x_0y) \neq 0\} = u_{x_0}^\pre(\{f \neq 0\}) \subset u_{x_0}^\pre(S)
\]
so since the latter is relatively compact, the former is also relatively compact.

\proofitem{2}.
Fix some $x_0 \in X$.
Since continuity is a local property, by the hypothesis on $X$, we can assume $\bigcup_{x \in X}u_x^\pre(S)$ is contained in a compact subset $Y' \subset Y$.
Then $f(xy) \neq 0$ implies $y \in Y'$.
The map $(x,y) \mapsto f(xy)$ is a continuous map $X \times Y \to \C$, so by \cref{lem:slice-continuous-compact-open}, $f \circ u_x \to f \circ u_{x_0}$ uniformly on $Y'$ as $x \to x_0$ (i.e. is continuous at $x_0$ with respect to the topology of uniform convergence).
By continuity of Radon integration with respect to uniform convergence on compact sets (\cref{lem:radon-integration-compact-support-continuity}), we thus have that as $x \to x_0$,
\[
\int f(xy) \dd{\mu(y)} = \int_{Y'} f(xy)\dd{\mu(y)} = \int_{Y'} f \circ u_x \dd{\mu} \to \int_{Y'} f \circ u_{x_0}\dd{\mu} = \int_{Y'} f(x_0y)\dd{\mu(y)} = \int f(x_0y) \dd{\mu(y)}\nsp{.}
\]

\end{proof}

\begin{exposition}
Let $G$ be a topological group.
Let $f$ be a measurable function on $G$ (with values in $\C$ or $\interval{0}{\infty}$).
If $s \in G$, we define the measurable function $L_sfC$ by
\[
L_sf(x) \eqdef f(s^{-1}x)\nsp{.}
\]
This is well-defined on $L^0(\mu)$ for any measure $\mu$ on $G$.
\end{exposition}

\begin{example}
We have $L_s\chi_E = \chi_{sE}$ since $x \in sE \iff s^{-1}x \in E$.
\end{example}

\begin{exposition}
If $\mu$ is a measure on $G$, we define $L_s\mu$ by
\[
L_s\mu(E) = \mu(s^{-1}E)\nsp{.}
\]
If $\mu$ is a Radon measure, then so is $L_s\mu$.

Then we have
\[
\int L_{s^{-1}}f(x) \dd{\mu(x)} = \int f(sx)\dd{\mu(x)} = \int f(x) \dd{(L_s\mu)(x)}
\]
whenever the leftmost integral makes sense (consider first characteristic functions).
We write $\dd{\mu(s^{-1}x)}$ instead of $\dd{L_s\mu(x)}$, giving
\[
\int f\dd{\mu(s^{-1}x)} = \int f(sx)\dd{\mu(x)}\nsp{.}
\]
\end{exposition}

\begin{exposition}
Let $G$ be a topological group.
Let $f$ be a measurable function on $G$ (with values in $\C$ or $\interval{0}{\infty}$).
If $s \in G$, we define the measurable function $R_sf\colon X \to \C$ by
\[
R_sf(x) \eqdef f(xs)\nsp{.}
\]
This is well-defined on $L^0(\mu)$ for any measure $\mu$ on $G$.
\end{exposition}

\begin{example}
We have $R_s\chi_E = \chi_{Es^{-1}}$ since $x \in Es^{-1} \iff xs \in E$.
\end{example}

\begin{exposition}
If $\mu$ is a measure on $G$, we define $R_s\mu$ by
\[
R_s\mu(E) = \mu(Es)\nsp{.}
\]
If $\mu$ is a Radon measure, then so is $R_s\mu$.

Then we have
\[
\int R_{s^{-1}}f(x) \dd{\mu(x)} = \int f(xs^{-1})\dd{\mu(x)} = \int f(x) \dd{(R_s\mu)(x)}
\]
whenever the leftmost integral makes sense (consider first characteristic functions).
We write $\dd{\mu(xs)}$ instead of $\dd{R_s\mu(x)}$, giving
\[
\int f\dd{\mu(xs)} = \int f(xs^{-1})\dd{\mu(x)}\nsp{.}
\]
\end{exposition}

\begin{exposition}
Since
\[
\begin{aligned}
L_s(f\mu)(E) & = \int_{s^{-1}E} f\dd{\mu}                                \\
             & = \int \chi_{s^{-1}E}(x)f(x)\dd\mu(x)                     \\
             & = \int \chi_{s^{-1}E}(s^{-1}x)f(s^{-1}x)\dd{\mu(s^{-1}x)} \\
             & =  \int \chi_E(x)f(s^{-1}x)\dd{\mu(s^{-1}x)}              \\
             & = \int_E f(s^{-1}x)\dd{\mu(s^{-1}x)}
\end{aligned}
\]
we thus have $L_s(f\mu) = L_sfL_s\mu$.

Since
\[
\begin{aligned}
R_s(f\mu)(E) & = \int_{Es} f\dd{\mu}                 \\
             & = \int \chi_{Es}(x)f(x)\dd\mu(x)      \\
             & = \int \chi_{Es}(xs)f(xs)\dd{\mu(xs)} \\
             & =  \int \chi_E(x)f(xs)\dd{\mu(xs)}    \\
             & = \int_E f(xs)\dd{\mu(xs)}
\end{aligned}
\]
we thus have $R_s(f\mu) = R_sfRs_\mu$.
\end{exposition}

\begin{exposition}
A measure $\mu$ is said to be a left (resp. right) \addindex{invariant measure} if $L_s\mu = \mu$ (resp. $R_s\mu = \mu$) for every $s \in G$.
\end{exposition}

\begin{exposition}
A nonzero left (resp. right) invariant positive Radon measure $\mu$ on a Hausdorff topological group $G$ is called a left (resp. right) \addindex{Haar measure}.
\end{exposition}

\begin{lemma}
Let $X$ be a Hausdorff topological group.
Let $\mu$ be a Haar measure on $X$.
The following hold:
\begin{enumerate}
\item there is a compact subset $K$ of $X$ such that $0 \lt \mu(K) \lt \infty$
\item $\mu(U) \neq 0$ for every nonempty open subset $U$ of $X$
\item $\int f\dd{\mu} \gt 0$ for every $f \in C_c(X)_{\gt 0}$.
\item if $f$ and $f'$ are continuous functions $X \to \C$ and $f = f'$ $\mu$-a.e., then $f = f'$.
\end{enumerate}
\end{lemma}
\begin{proof}
\proofitem{1}.
Since $\mu$ is nonzero, $\mu(X) \neq 0$.
Hence the result follows from inner regularity of $\mu$ and the fact that $\mu$ is finite on compact sets.

\proofitem{2}.
Let $U$ be a nonempty subset of $X$.
By \proofitem{1} there is $K^\compact \subset X$ such that $0 \lt \mu(K) \lt \infty$.
Because $U$ is nonempty, the family $(xU)_{x \in X}$ is a open cover of $X$ and hence of $K$.
By compactness of $K$ and the fact that $\mu$ is a Haar measure, it follows $0 \lt \mu(K) \leq \sum_{i=0}^n U = n\mu(U)$ for some $n \in \N$.
Then $n \gt 0$ and thus $\mu(U) \neq 0$.

\proofitem{3}.
If $f \in C_c(X)$ is positive and nonzero, then $U \eqdef \{x:f(x) \gt 2^{-1}\norm{f}_u\}$ is open and nonempty.
By \proofitem{2}, it follows $\int f \dd{\mu} \geq 2^{-1}\norm{f}_u \mu(U) \gt 0$.

\proofitem{4}.
By assumption, $\{f \neq f'\}$ has measure zero.
Since $f$ and $f'$ are continuous, this set is open, and hence is empty by \proofitem{2}.
Hence $f = f'$.
\end{proof}

\begin{exposition}
Let $G$ be a topological group.
Let $f$ be a measurable function on $G$ (with values in $\C$ or $\interval{0}{\infty}$).
If $s \in G$, we define the measurable function $\check{f}$ by
\[
\check{f}(x) \eqdef f(x^{-1})\nsp{.}
\]
This is well-defined on $L^0(\mu)$ for any measure $\mu$ on $G$.
\end{exposition}

\begin{example}
We have $\check{\chi_E} = \chi_{E^{-1}}$ since $x \in E^{-1} \iff x^{-1} \in E$.
\end{example}

\begin{exposition}
For every measure $\mu$ on a Hausdorff topological group $G$, we get a measure $\check{\mu}$ defined by
\[
\check{\mu}(E) \eqdef \mu(E^{-1})\nsp{.}
\]
If $\mu$ is a Radon measure, then so is $\check{\mu}$.

Then we have
\[
\int \check{f}\dd{\mu}= \int f(x^{-1})\dd{\mu(x)} = \int f(x)\dd{\check{\mu}}
\]
whenever the leftmost integral makes sense (consider first characteristic functions).

We write $\dd{\mu(x^{-1})}$ instead of $\dd{\check{\mu}(x)}$, giving
\[
\int f\dd{\mu(x^{-1})} = \int f(x^{-1})\dd{\mu(x)}\nsp{.}
\]
\end{exposition}

\begin{exposition}
Since
\[
\begin{aligned}
(\widecheck{f\mu})(E) & = \int_{E^{-1}}f\dd{\mu}                              \\
                      & = \int \chi_{E^{-1}}(x)f(x)\dd{\mu(x)}                \\
                      & = \int \chi_{E^{-1}}(x^{-1})f(x^{-1})\dd{\mu(x^{-1})} \\
                      & = \int \chi_{E}(x)f(x^{-1})\dd{\mu(x^{-1})}           \\
                      & = \int_E f(x^{-1})\dd{\mu(x^{-1})}                    \\
                      & = \int_E f(x^{-1})\dd{\mu(x^{-1})}                    \\
                      & = (\check{f}\check{\mu})(E)
\end{aligned}
\]
we have $\widecheck{f\mu} = \check{f}\check{\mu}$.
\end{exposition}

\begin{lemma}
A measure $\mu$ is a left Haar measure if and only if $\check{\mu}$ is a right Haar measure.
\end{lemma}
\begin{proof}
Omitted.
\end{proof}

\begin{lemma}
Let $f,g \in C_c(G)_{\geq 0}$.
Assume $g \neq 0$.
There exists finite families $(c_i)$ and $(s_i)$ such that $f \leq \sum c_iL_{s_i}g$.
\end{lemma}
\begin{proof}
There exists an open set $U$ such that $\inf_{s \in U} g(s) \gt 0$ (by continuity of $g$), and we can cover $\supp(f)$ with finitely many translates of $U$.
\end{proof}

\begin{lemma}
For all $f,g \in C_c(G)_{\geq 0}$ such that $g \neq 0$, define
\[
\haarnumber{f}{g} \eqdef \inf \left\{\sum c_i:f \leq c_iL_{s_i}g\right\}\nsp{.}
\]
The following hold:
\begin{enumerate}
\item $\haarnumber{L_sf}{g} = \haarnumber{f}{g}$
\item $\haarnumber{cf}{g} = c\haarnumber{f}{g}$ for all $c \geq 0$
\item $\haarnumber{f+f'}{g} \leq \haarnumber{f}{g} + \haarnumber{f'}{g}$.
\end{enumerate}
\end{lemma}
\begin{proof}
Omitted.
\end{proof}

\begin{lemma}
The following hold:
\begin{enumerate}
\item $\haarnumber{f}{g} \geq \norm{f}_\infty/\norm{g}_\infty$
\item $\haarnumber{f}{h} \leq \haarnumber{f}{g}\haarnumber{g}{h}$
\item \[
      0 \lt \frac{1}{\haarnumber{f_0}{f}} \leq \frac{\haarnumber{f}{g}}{\haarnumber{f_0}{g}} \leq \haarnumber{f}{f_0}
      \]
\end{enumerate}
\end{lemma}
\begin{proof}
\proofitem{1}.
Choose $c_i$ and $s_i$ such that $f \leq \sum c_iL_{s_i}g$.
By \cref{thm:extreme-value-theorem} we can choose $x$ such that $f(x) = \norm{f}_\infty$.
Then $\norm{f}_\infty \leq \sum c_ig(s_i^{-1}x) \leq (\sum c_i)\norm{g}_\infty$.

\proofitem{2}.
If $f \leq \sum c_iL_{s_i}g$ and $g \leq \sum d_jL_{t_j}h_j$, then $f \leq \sum_{i,j}L_{s_it_j}h$.

\proofitem{3}.
Follows from \proofitem{2}.
\end{proof}

\begin{lemma}
Let $f_1,f_2,h$ be elements of $C_c(G)_{\geq 0}$.
Suppose $h \geq 1$ on the support of $f+f'$.
Let $\epsilon \gt 0$.
There exists a compact neighborhood $V$ of $1$ such that for every $g \in C_c(G)_{\gt 0}$ with $\supp(g) \subset V$,
\[
\haarnumber{f_1}{g} + \haarnumber{f_2}{g} \leq \haarnumber{f_1+f_2}{g} + \epsilon \haarnumber{h}{g}\nsp{.}
\]
\end{lemma}
\begin{proof}
Let $f \eqdef f_1 + f_2 + \frac{\epsilon}{2}h$.
There are elements $\phi_k \in C_c(G)_{\geq 0}$ such that $\phi_k = f_k/f$ on the support of $f+f'$ and are $0$ outside this support.
Since $\phi_k$ are continuous and have compact support, they are uniformly continuous.

Fix $\eta \gt 0$ to be determined later.
Since $G$ is locally compact, there is a compact neighborhood $V$ such that $\abs{\phi_k(s)-\phi_k(t)} \lt \eta$ for every $k$, and $s$ and $t$ such that $s^{-1}t \in V$.

Let $g \in C_c(G)_{\gt 0}$ and suppose $\supp(g) \subset V$.
Then for all $s$,
\[
\phi_kL_sg \leq (\phi_k(s) + \eta)L_s(g)\nsp{.}
\]
Choose $c_i$ and $s_i$ such that $f \leq \sum c_iL_{s_i}g$.
Then
\[
f_k = \phi_k f \leq \sum c_i\phi_kL_{s_i}g \leq \sum c_i(\phi_k(s_i) + \eta)L_{s_i}(g)
\]
so
\[
\haarnumber{f_1}{g} + \haarnumber{f_2}{g} \leq \sum c_i(\phi_1(s_i) + \phi_2(s_i) + 2\eta) \leq (1+2\eta)\sum c_i \leq (1+2\eta)\haarnumber{f}{g}\nsp{.}
\]
Then we get
\[
\begin{aligned}
\haarnumber{f_1}{g} + \haarnumber{f_2}{g} & \leq (1+2\eta)\haarnumber{f}{g}                                                                                                                                        \\
                                          & \leq (1+2\eta)\left(\haarnumber{f_1+f_2}{g} + \frac{\epsilon}{2}\haarnumber{h}{g}\right)                                                                               \\
                                          & \leq \haarnumber{f_1+f_2}{g} + \frac{\epsilon}{2}\haarnumber{h}{g} + 2\eta\left(\haarnumber{f_1+f_2}{h}\haarnumber{h}{g} + \epsilon\eta\haarnumber{h}{g}\right)\nsp{.}
\end{aligned}
\]
If we choose $\eta$ such that $\eta\left(2\haarnumber{f_1+f_2}{h}+\epsilon\right) \leq \frac{\epsilon}{2}$, then the result follows.
\end{proof}

\begin{lemma}
\label{lem:haar-uniqueness-auxillary-function}
Let $G$ be a locally compact Hausdorff topological group.
Let $f \in C_c(G)$.
Let $\nu$ be a Radon measure on $G$.
The map
\[
s \mapsto \int f(t^{-1}s)\dd{\nu(t)}
\]
is well-defined and continuous.
\end{lemma}
\begin{proof}
Let $u_s(t) = t^{-1}s$.
Let $S$ be the (compact) support of $f$.
By \cref{lem:slice-radon-integral-continuous} we need to show that for every $s_0 \in G$, there is a neighborhood $V$ of $s_0$ such that $\bigcup_{s \in V}u_s^\pre(S)$ is relatively compact in $G$.
We have $u_s^\pre(S) = \{t \in G:t^{-1}s \in S\} = sS^{-1}$.
Since $s_0S^{-1}$ is compact and $G$ is locally compact, there is a relatively compact open $W \supset s_0S^{-1}$ (\cref{prop:locally-compact-hausdorff-neighborhood-base}).
We can choose a neighborhood $U$ of $1$ such that $Us_0S^{-1} \subset W$ (see \cref{prop:topological-group-compact-properties}).
Then for every $s \in V \eqdef Us_0$, $u_s^\pre(S) = sS^{-1} \subset US^{-1} \subset W$, so $\bigcup_{s \in V} u_s^\pre(S) \subset W$ is relatively compact.
\end{proof}

\begin{theorem}
Let $G$ be a locally compact Hausdoff topological group.
There exists a Haar measure on $G$, and any two Haar measures on $G$ are unique up to multiplication by a (strictly positive) real number.
\end{theorem}
\begin{proof}
The sets $\{g \in C_c(G)_{\geq 0}:\supp(g) \subset V\}$, as $V$ ranges over all compact neighborhoods of $1$, form a filter base.
Let $F$ be an ultrafilter finer than this filter base.
Fix a $f_0 \in C_c(G)_{\gt 0}$.
For $f \in C_c(G)_{\geq 0}$ and $g \in C_c(G)_{\gt 0}$, define
\[
T_g(f) \eqdef \frac{\haarnumber{f}{g}}{\haarnumber{f_0}{g}}\nsp{.}
\]
For each $f$, the pushforward of $F$ by the map $g \mapsto T_g(f)$ is an ultrafilter on the compact Hausdorff space $\interval{1/\haarnumber{f_0}{f}}{\haarnumber{f}{f_0}}$; let $T(f)$ be its limit.
Then $T(f+f') \leq T(f) + T(f')$ (consider associated nets).
Similarly, for all $\epsilon \gt 0$ and $h \geq 1$ on the support $f+f'$, $T(f) + T(f') \leq T(f+f') + \epsilon T(h)$.
It follows (by fixing $h$ and letting $\epsilon$ vary) that $T(f+f') = T(f)+T(f')$.
Since $\haarnumber{cf}{g} = c\haarnumber{f}{g}$, we see that $T$ is ``$\R_+$-linear''.
Thus by \cref{thm:riesz-markov-kakutani-theorem} we get an associated Radon measure $\mu$ on $G$.
Since $\haarnumber{f}{g} \geq \norm{f}_\infty/\norm{g}_\infty$, $\mu$ is nonzero, and since $\haarnumber{L_sf}{g} = \haarnumber{f}{g}$, we see that $\mu$ is a Haar measure.

We show the uniqueness.
Let $\nu$ be a right Haar measure.
It suffices to show that the left Haar measure $\check{\nu}$ is a multiple of $\mu$.
Let $f \in C_c(G)_{\gt 0}$.
Then $\int f\dd{\mu} \gt 0$.
By \cref{lem:haar-uniqueness-auxillary-function}, there is a well-defined continuous function $D_f$ defined by
\[
D_f(s) = \frac{1}{\int f\dd{\mu}} \int f(t^{-1}s)\dd{\nu(t)}\nsp{.}
\]
Let $g \in C_c(G)$.
The map $(s,t) \mapsto f(s)g(ts)$ is continuous with compact support on $G \times G$.
By \cref{thm:fubini-theorem},
\[
\begin{aligned}
\left(\int f\dd{\mu}\right)\left(\int g\dd{\nu}\right) & = \int\int f(s)g(ts)\dd{\nu(t)}\dd{\mu(s)}                                                                              \\
                                                       & = \int\int f(s)g(ts)\dd{\mu(s)}\dd{\nu(t)}                                                                              \\
                                                       & = \int\int f(t^{-1}s)g(s)\dd{\mu(s)}\dd{\nu(t)}                             &  & \text{because $\mu$ is a Haar measure} \\
                                                       & = \int\left(\int f(t^{-1}s)\dd{\nu(t)}\right)g(s)\dd{\mu(s)}                                                            \\
                                                       & = \int \left(\int f\dd{\mu}\right)D_f(s)g(s)\dd{\mu(s)}                                                                 \\
                                                       & = \left(\int f\dd{\mu}\right)\left(\int D_f(s)g(s)\dd{\mu(s)}\right)\nsp{,}
\end{aligned}
\]
so $\int g\dd{\nu} = \int D_f(s)g(s)\dd{\mu(s)}$.
Taking real and imaginary parts, and then positive and negative parts, and using \cref{thm:riesz-markov-kakutani-theorem}, we conclude that $\nu = D_f\mu$.
In particular, the $D_f = D_{f'}$ $\mu$-a.e. for all appropriate $f$ and $f'$.
But then $D_f = D_{f'}$ since both are continuous (since $\mu$ is a Haar measure), so $D_f$ is independent of $f$.
Write $D$ for the common value.
Then by definition for $f \in C_c(G)_{\gt 0}$,
\[
D(1) =  \frac{1}{\int f\dd{\mu}}\int f(t^{-1})\dd{\nu(t)}
\]
and thus for all such $f$,
\[
D(1)\int f\dd{\mu} = \int f\dd{\check{\nu}}\nsp{.}
\]
The equation above still holds if $f = 0$.
We conclude by \cref{thm:riesz-markov-kakutani-theorem}.
\end{proof}

\begin{exposition}
Let $G$ be a locally compact Hausdorff topological group.
We will fix a Haar measure $\mu$ on $G$.
We define $L^p(G) \eqdef L^p(\mu)$.
This is independent of the choice of Haar measure, since they are all proportional.
\end{exposition}

\begin{example}
Let $G$ be a discrete group.
The counting measure on $G$ is Haar measure on $G$ (the compact subsets of $G$ are the finite sets).
\end{example}

\section{Haar Modulus}

\begin{lemma}
Let $X$ and $Y$ be Hausdorff topological spaces.
Let $\phi\colon X \to Y$ be a homeomorphism.
Let $\mu$ be a Radon measure on $Y$.
Then the map $E \mapsto \mu(\phi(E))$ is a Radon measure on $X$.
\end{lemma}
\begin{proof}
Omitted (note that the map is the pushforward of $\mu$ by $\phi^\inv$).
\end{proof}

\begin{exposition}
Let $G$ be a locally compact Hausdorff topological group.

Let $\phi\colon G\to G$ an automorphism of topological groups.
Then for every measurable $E \subset G$, $\phi(E)$ is measurable.
Furthermore the map $E \mapsto \mu(\phi(E))$ is a (left) Haar measure $\mu_\phi$ (as $\mu(\phi(gE)) = \mu(\phi(g)\phi(E)) = \mu(\phi(E))$ for any measurable $E \subset G$).
Thus there is a $\modulus(\phi) \gt 0$ such that for all measurable $E$,
\[
\mu(\phi(E)) = \modulus(\phi)\mu(E)\nsp{.}
\]
Since Haar measures are unique up to a positive real number, $\modulus(\phi)$ is independent of the original choice of $\mu$.
This defines a map
\[
\modulus\colon\Aut_{\TopGrp}(G) \to \ointerval{0}{\infty}\nsp{,}
\]
which is straightforwardly verified to be a group homomorphism.
Since $\chi_E \circ \phi^\inv = \chi_{\phi(E)}$, we get
\[
\int f(\phi^\inv(x))\dd{\mu(x)} = \modulus(\phi) \int f\dd{\mu}
\]
for all integrable $f$ (by the usual arguments).
\end{exposition}

\begin{proposition}
\label{prop:automorphism-modulus-continuity}
Let $G$ be a locally compact Hausdoff topological group.
Let $\Gamma$ be a topological group.
Let $\gamma \mapsto u_\gamma$ be a homomorphism $\Gamma \to \Aut_{\TopGrp}(G)$ such that $(\gamma,x) \mapsto u_\gamma(x)$ is a continuous map $\Gamma \times G \to G$.
Then the map $\Gamma \to \ointerval{0}{\infty}$ given by
\[
\gamma \mapsto \modulus(u_\gamma)
\]
is a continuous group homomorphism.
\end{proposition}
\begin{proof}
It is clear that the map is a group homomorphism; we prove continuity.

Fix $f \in C_c(G)_{\gt 0}$ and let $S$ be its support.

Let $\gamma_0 \in \Gamma$.
Let $U$ be a relatively compact neighborhood of $u_{\gamma_0}^\inv(S)$ (since $u_{\gamma_0}$ is a homeomorphism, $u_{\gamma_0}^\inv(S)$ is compact).
By \cref{lem:slice-continuous-compact-open}, the map $\gamma \to u_\gamma$ is continuous if we give $\Aut_{\TopGrp}$ the compact-open topology, so there is a neighborhood $V \subset \Gamma$ of $\gamma_0$ such that $u_\gamma^\inv(S) \subset U$ for all $\gamma \in V$.
Then $\bigcup_{\gamma \in V} u_{\gamma}^\inv(S) \subset U$, verifing the conditions of \cref{lem:slice-radon-integral-continuous}; thus
\[
\gamma \mapsto \int f(u_\gamma(x))\dd{\mu(x)}
\]
is continuous from $\Gamma \to \C$.
Since inversion on $\Gamma$ is continuous, $u_\gamma^\inv = u_{\gamma^{-1}}$ for all $\gamma \in \Gamma$, and
\[
\modulus(u_\gamma) = \frac{\int f(u_{\gamma^{-1}}(x))\dd{\mu(x)}}{\int f\dd{\mu}}\nsp{,}
\]
the result then follows.
\end{proof}

\begin{exposition}
Let $G$ be a locally compact Hausdorff topological group.

For each $s \in G$, let $u_s$ be the conjugation $x \mapsto s^{-1}xs$.
We get a homomorphism $s \mapsto u_s$ from $G$ to $\Aut_{\TopGrp}$ such that $(s,x) \mapsto u_s(x) = s^{-1}xs$ is continuous as a map $G \times G \to G$.
Write $\Delta(s) \eqdef \modulus(u_s)$.
Then
\[
s \mapsto \Delta(s)
\]
defines a continuous homomorphism $G \to \ointerval{0}{\infty}$ (\cref{prop:automorphism-modulus-continuity}).
We call this function the \addindex{Haar modulus} function.

Since $\mu(s^{-1}Es) = \mu(Es)$, the defining relation becomes
\[
\mu(Es) = \Delta(s)\mu(E)\nsp{,}
\]
i.e.
\[
R_s\mu = \Delta(s)\mu\nsp{.}
\]
\end{exposition}

\begin{exposition}
If $\phi\colon G \to H$ is an isomorphism of topological groups, then $\Delta_H \circ \phi = \Delta_G$.
Indeed, let $\mu$ be a Haar measure on $H$.
Then $E \mapsto \mu(\phi(E))$ is Haar measure on $G$ and
\[
(\mu \circ \phi)(Es) = \mu(\phi(E)\phi(s)) = \Delta_H(\phi(s))(\mu \circ \phi)(E)\nsp{.}
\]
\end{exposition}

\begin{exposition}
Let $\mu$ be a Haar measure on a locally compact Hausdorff topological group.
Since $L_s\mu = \mu$ and $R_s\mu$ is proportional to $\mu$, we see that $L_s$ and $R_s$ define automorphisms of $L^1(\mu)$.
\end{exposition}

\begin{exposition}
We have
\[
R_s\left(\frac{1}{\Delta} \mu\right) = R_s\left(\frac{1}{\Delta}\right)R_s\mu = \frac{1}{\Delta(s)\Delta}(\Delta(s)\mu) = \frac{1}{\Delta}\mu
\]
so $1/\Delta \mu$ is a right Haar measure.
Since $\check{\mu}$ is a right Haar measure, we have $\check{\mu} = (a/\Delta)\mu$ for some $a \gt 0$.
Because $\Delta$ is a homomorphism,
\[
\mu = a\left(\widecheck{\frac{1}{\Delta}\mu}\right)= a\check{\frac{1}{\Delta}}\check{\mu} =  a\Delta\check{\mu} = a^2\frac{\Delta}{\Delta}\mu = a^2\mu
\]
and hence $a = 1$.
Thus
\[
\check{\mu} = \frac{1}{\Delta}\mu\nsp{.}
\]
\end{exposition}

\begin{exposition}
We have
\[
\int \check{f}\frac{1}{\Delta}\dd{\mu} = \int \check{f}\dd{\check{\mu}} = \int f\dd{\mu}\nsp{.}
\]
In particular, if $f$ is $\mu$-integrable, then $\check{f}/\Delta$ is $\mu$-integrable.
\end{exposition}

\begin{exposition}
We say a locally compact Hausdorff topological group is a \addindex{unimodular group} if the Haar modulus $\Delta$ is equal to the constant function $1$.
\end{exposition}

\begin{lemma}
Let $G$ be a locally compact Hausdorff topological group.
If there is a compact neighborhood $V$ of $1$ invariant under conjugation, then $G$ is unimodular.
\end{lemma}
\begin{proof}
For every $s \in G$, $\mu(V) = \mu(s^{-1}Vs) = \Delta(s)\mu(V)$.
Since $0 \lt \mu(V) \lt \infty$, we can divide and conclude $\Delta(s) = 1$ for all $s$.
\end{proof}

\begin{proposition}
Let $G$ be a locally compact Hausdorff topological group.
If $G$ is commutative, compact, or discrete, then $G$ is unimodular.
\end{proposition}
\begin{proof}
If $G$ is commutative or compact, then it is clear that $1$ has a compact neighborhood invariant under conjugation.
If $G$ is discrete, then the Haar measure is a multiple of the counting measure.
It is clear that $\{1\}$ is then a neighborhood of $1$ which is compact and invariant under conjugation.
\end{proof}

\begin{exposition}
Let $G$ be a unimodular locally compact Hausdorff topological group.
Then for every Haar measure on $G$,
\[
\mu = R_s\mu = L_s\mu = \check{\mu}\nsp{.}
\]
It follows that if $f$ is integrable, so are $L_sf$, $R_sf$, and $\check{f}$, and
\[
\int f(sx)\dd{\mu(x)} = \int f(xs)\dd{\mu(x)} = \int f(x^{-1})\dd{\mu(x)} = \int f\dd{\mu}
\]
for all $s \in G$.
In particular, $\mu(sE) = \mu(Es) = \mu(E^{-1}) = \mu(E)$.
\end{exposition}

\section{Convolution}

\begin{exposition}
Let $G$ be a Hausdorff topological group.
We write $M(G)$ for the Banach space of complex Radon measures on $G$.
\end{exposition}

\begin{exposition}
Let $G$ be a Hausdorff topological group.
Let $\mu$ and $\nu$ be complex Radon measures on $G$.
Then the \addindex{convolution} of $\mu$ and $\nu$, written $\mu * \nu$, is complex Radon measure defined to be the pushforward of the complex Radon measure $\mu \times \nu$ by the group operation $G \times G \to G$.
\end{exposition}

\begin{exposition}
Let $G$ be Hausdorff topological group.
Let $\mu$ and $\nu$ be complex measures on $G$.
Let $f\colon X \to \C$ be bounded.
Then
\[
\int f\dd{(\mu * \nu)} = \int f(xy)\dd{(\mu \times \nu)(x,y)}\nsp{.}
\]
\end{exposition}

\begin{proposition}
Let $G$ be a Hausdorff topological group, written additively.
Then convolution makes $M(G)$ into a Banach algebra.
\end{proposition}
\begin{proof}
Let us write the group operation on $G$ additively.

Complex bilinearity follows from e.g. the fact that $(a\mu + b\nu)\times \lambda = a(\mu \times \lambda) + b(\nu \times \lambda)$.

For all compact $K,L \subset G$,
\[
((\lambda \times \mu) \times \nu)(\{((x,y),z):(x+y,z) \in K \times L\}) = (\lambda\times\mu)(\{(x,y):x+y \in K\})\nu(L) = (\lambda * \mu)(K)\nu(L) = ((\lambda * \mu) \times \nu)(K \times L)
\]
so we see that $(\lambda * \mu) \times \nu$ is the pushforward of $(\lambda \times \mu)\times \nu$ by $+ \times 1\colon (G \times G) \times G \to G \times G$.
Similarly $\lambda \times (\mu * \nu)$ is the pushforward of $\lambda \times (\mu \times \nu)$ by $1 \times +$.
Pushing forward $(\lambda * \mu) \times \nu$ by the addition on $G$ gives $(\lambda * \mu) * \nu$.
We can pushforward $(\lambda \times \mu) \times \nu$ by the associativity isomorphism $(G \times G) \times G \to G \times (G \times G)$, then by $1 \times +$, and the finally by $+$ to get $\lambda * (\mu * \nu)$.
Since addition on $G$ is associative, these two ways of pushing forward $(\lambda \times \mu) \times \nu$ are the same, i.e. $(\lambda \times \mu) \times \nu = \lambda * (\mu * \nu)$.
Thus convolution is associative.

Since
\[
\begin{aligned}
\norm{\mu * \nu} & = \sup\left\{\sum \abs{(\mu \times \nu)(\{(x,y):x+y \in E_n\})}:\bigsqcup E_n = G\right\} \\
                 & \leq \abs{\mu \times \nu}(G \times G)                                                     \\
                 & = (\abs{\mu} \times \abs{\nu})(G \times G)                                                \\
                 & = \abs{\mu}(G)\abs{\nu}(G)                                                                \\
                 & = \norm{\mu}\norm{\nu}
\end{aligned}
\]
we see that convolution makes $M(G)$ a Banach algebra.
\end{proof}

\begin{proposition}
Let $G$ be a Hausdorff topological group, written additively.
The following hold:
\begin{enumerate}
\item for every $s \in G$, and $\mu \in M(G)$, $\delta_s * \mu = L_s\mu$ and $\mu * \delta_s = R_{-s}\mu$
\item $\delta_1$ is the multiplicative identity of $M(G)$
\item $\norm{\delta_s} = 1$.
\end{enumerate}
\end{proposition}
\begin{proof}
By bilinearity of convolution, we can assume $\mu$ is positive and finite.
Then by \cref{thm:tonelli-theorem}, we have
\[
\begin{aligned}
(\mu * \delta_s)(E) & = \int \chi_E(x+y)\dd{(\mu \times \delta_s)(x,y)}  \\
                    & = \int \int \chi_E(x+y)\dd{\delta_s(y)}\dd{\mu(x)} \\
                    & = \int \chi_E(x+s)\dd{\mu(x)}                      \\
                    & = \int \chi_E(x)\dd{R_{-s}\mu}                     \\
                    & = R_{-s}\mu(E)\nsp{,}
\end{aligned}
\]
so $\mu * \delta_s = R_{-s}\mu$.
Similarly, $\delta_s * \mu = L_s\mu$.

\proofitem{2}.
Follows from \proofitem{1}.

\proofitem{3}.
Omitted.
\end{proof}

\begin{lemma}
Let $G$ be a Hausdorff topological group, written additively.
Then $\delta_x * \delta_y = \delta_{x+y}$.
Thus the map $G \mapsto M(G)$ given by $x \mapsto \delta_x$ is an injective monoid homomorphism.
\end{lemma}
\begin{proof}
We have
\[
(\delta_x * \delta_y)(E) = (\delta_x \times \delta_y)(\{(g,h):g+h \in E\}) = \delta_{(x,y)}(\{(g,h):g+h \in E\})
\]
so $(\delta_x * \delta_y)(E) =1$ if and only if $x+y \in E$.
\end{proof}

\begin{proposition}
Let $G$ be a Hausdorff topological group, written additively.
Then $G$ is abelian if and only if convolution is commutative.
\end{proposition}
\begin{proof}
Since $G$ can be identified with a submonoid of $M(G)$, we see that if $M(G)$ is commutative, then $G$ is abelian.

Suppose $G$ is abelian.
The pushforward of $\mu \times \nu$ by the braiding $G \times G \to G\times G$ is $\nu \times \mu$.
Then pushing forward by the (addition) operation on $G$ gives $\nu * \mu$.
Since $G$ is abelian, this also equals $\mu * \nu$.
\end{proof}

\begin{exposition}
Let $G$ be a Hausdorff topological group, written additively.

We define an involution on $M(G)$ as follows.
For $\mu$ a complex Radon measure on $G$, define a complex Radon measure $\mu^*$ on $G$ by
\[
\mu^*(E) \eqdef \overline{\mu(-E)} = \overline{\check{\mu}} = \check{\overline{\mu}}\nsp{.}
\]
\end{exposition}

\begin{proposition}
Let $G$ be a Hausdorff topological group, written additively.
Then the involution on $M(G)$ is an isometry making $M(G)$ a $*$-algebra with respect to convolution.
\end{proposition}
\begin{proof}
It is straightforward to verify that the involution is an isometry.

It is clear that $(a\mu)^* = a^*\mu^*$.
Let $\beta\colon\powset{G \times G} \to \powset{G \times G}$ be given by
\[
\beta(E) \eqdef \{(-y,-x):(x,y) \in E\} = \{(x,y):(-y,-x) \in E\}\nsp{.}
\]
Since for all compact $K$ and $L$,
\[
(\nu^* \times \mu^*)(K \times L) = \nu(-K)^*\mu(-L)^* = (\mu \times \nu)(-L \times -K)^*
\]
we have $\nu^* \times \mu^* = \overline{(\mu \times \nu)\circ \beta}$.
The pushforward of $\overline{(\mu \times \nu)\circ \beta}$ by the group operation is given by
\[
E \mapsto \overline{(\mu \times \nu)(\beta(\{(x,y):x+y \in E\}))}= \overline{(\mu \times \nu)(\{(-y,-x):x+y \in E\})} = \overline{(\mu \times \nu)(\{(x,y):x+y \in -E\})} =(\mu * \nu)^*(E)\nsp{.}
\]
\end{proof}

\begin{exposition}
Fix a Haar measure $\mu$ on a Hausdorff topological group $G$.
Let $f$ and $g$ be integrable functions $G \to \C$.
Then by \cref{thm:tonelli-theorem},
\[
\begin{aligned}
\int \int \abs{f(y)g(y^{-1}x)}\dd{\mu(y)}\dd{\mu(y)} & = \int \int \abs{f(y)g(y^{-1}x)}\dd{\mu(x)}\dd{\mu(x)} \\
                                                     & = \int \int \abs{f(y)g(y^{-1}x)}\dd{\mu(x)}\dd{\mu(y)} \\
                                                     & =  \int \int \abs{f(y)g(x)}\dd{\mu(x)}\dd{\mu(y)}      \\
                                                     & = \left(\int \abs{f}\right)\left(\int \abs{g}\right)   \\
                                                     & = \norm{f}_1 \norm{g}_1                                \\
                                                     & \lt \infty\nsp{.}
\end{aligned}
\]
so $(x,y) \mapsto f(y)g(y^{-1}x)$ is $\mu \times \mu$-integrable.

By \cref{thm:fubini-theorem}, the map $x \mapsto \int f(y)g(y^{-1}x)\dd{\mu(y)}$ is defined for almost every $x \in G$, and is in $L^1(G)$.

We call this function the \addindex{convolution} of $f$ and $g$.
We get a well-defined map $* \colon L^1(G) \times L^1(G) \to L^1(G)$, which again we call convolution.
\end{exposition}

\begin{exposition}
Let $G$ be a Hausdorff topological group with Haar measure $\mu$.
Let $f,g \in L^1(G)$.
Let $\lambda$ and $\nu$ be complex measures given by $\dd{\lambda} = f\dd{\mu}$ and $\dd{\nu} = g\dd{\mu}$.
Let $E \subset X$ be measurable.
Then
\[
\begin{aligned}
(\lambda * \nu)(E) & = \int \chi_E \dd{(\lambda * \nu)}                         \\
                   & = \int \int \chi_E(yx)\dd{\lambda(y)}\dd{\nu(x)}           \\
                   & = \int \int \chi_E(yx)f(y)g(x) \dd{\mu(y)}\dd{\mu(x)}      \\
                   & = \int \int \chi_E(yx)f(y)g(x) \dd{\mu(x)}\dd{\mu(y)}      \\
                   & = \int \int \chi_E(x)f(y)g(y^{-1}x)\dd{\mu(x)}\dd{\mu(y)}  \\
                   & = \int \chi_E(x) \int f(y)g(y^{-1}x)\dd{\mu(y)}\dd{\mu(x)} \\
                   & = \int \chi_E(x) (f*g)(x)\dd{\mu(x)}                       \\
                   & = \int_E f*g \dd{\mu}\nsp{.}
\end{aligned}
\]
Thus we see that the isometry $L^1(G) \to M(G)$ preserves convolution.
In particular, convolution is associative and $\C$-bilinear on $L^1(G)$.
\end{exposition}

\begin{exposition}
Let $G$ be a locally compact Hausdorff topological group.
Let $f \in L^1(G)$.
We define
\[
f^*(x) \eqdef \frac{1}{\Delta(x)}\overline{f(x^{-1})} = \widecheck{\overline{\Delta f}} = \overline{\widecheck{\Delta f}}\nsp{,}
\]
which then gives a well-defined element $f^* \in L^1(G)$.
We have
\[
\int_E f^*\dd{\mu} = \int_E \overline{\widecheck{\Delta f}}\dd{\mu} = \overline{\int_E \widecheck{f}\frac{1}{\Delta}\dd{\mu}} = \overline{\int \chi_E \widecheck{f}\dd{\widecheck{\mu}}} = \overline{\int \chi_{E^{-1}} f\dd{\mu}} = \overline{\int_{E^{-1}} f\dd{\mu}}\nsp{,}
\]
so we see that $L^1(G) \to M(G)$ preserves $*$.

In particular, we have defined an involution on $L^1$ making it a $*$-prealgebra, and the isometry $L^1(G) \to M(G)$ is a $*$-homomorphism.
\end{exposition}

% every L1 has a continuous represntative.

