tex_files = $(wildcard **/*.tex)
.PHONY: all format
all: format

format: $(tex_files)
	latexindent -m -wd -s $(tex_files) math.tex preamble.tex preamble2.tex
